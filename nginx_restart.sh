sudo kill -9 $(cat aux_module/aux_module.pid)
sudo nohup ruby aux_module/aux_module.rb -o 0.0.0.0 > aux_module/nohup.out 2>&1 &
sudo /opt/nginx/sbin/nginx -s stop
bundle exec rake jobs:clear
sudo /usr/local/bin/ruby script/delayed_job restart
sudo /opt/nginx/sbin/nginx -c /opt/nginx/conf/nginx.conf