class CouponCodeController < ApplicationController

	 include CouponCodeHelper
	 include ApplicationHelper
	 
	 require 'csv'
  	 require 'json'


	 def create_coupon_code
	 	@tenant_id = params[:tenant_id]
	 	@regions = Region.all
	 end

	 def generate_coupon_codes
	    @tenant_id = params[:tenant_id]
	    @errors = params[:errors].present? ? params[:errors] : ""
	    @regions = Region.all
	 end

	 def create_coupon
	 	status = true
		error = ""
		@bg = current_bg
		if status
			coupon_code = coupon_code(@bg,params)
		    logger.debug "create_coupon metadata is " + coupon_code.to_s
			
			status = coupon_code["status"]
			error = coupon_code["error"]
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	 end

	 def generate_coupon
	    @bg = current_bg
	    @tenant_id = params[:tenant_id]
	    appids = App.where(:tenant_id => params[:tenant_id]).map(&:ott_id)
	    tenants = Tenant.where(:id => params[:tenant_id]).map(&:ott_id)
	    regions = params[:regions].present? ? params[:regions].split(",").map(&:strip) : ["IN"]
	    currency_code = params[:currency] || get_currency_code(regions.first)
	    params_to_scrub = [ :action, :controller, :upload_coupon ]
	    params_to_scrub1 = [ :action, :controller ]
	    @errors = nil
	    metadata = nil
	    upload_file = params[:upload_coupon]
	    if params[:mode] == 'upload' 	
	      byebug	
	      if (params[:upload_coupon].content_type.present?) && (params[:upload_coupon].content_type.include?('.sheet'))
	        un_coupons = []
	        name =  upload_file.original_filename
	        directory = Rails.root.join("public/excel_files/")
	        path = File.join(directory, name)
	        File.open(path, 'wb') { |f| f.write(upload_file.read) }
	        
	        excel = Roo::Excelx.new(path)
	        excel.default_sheet = excel.sheets.first
	        if excel.last_row.present? && (excel.last_row > 1)
	          (2..excel.last_row).each do |i|
	            un_coupons << { :unique_code => excel.row(i)[0], :app_ids => JSON.parse(excel.row(i)[1])}  
	          end
	          metadata = params.except!( *params_to_scrub ).merge({:app_ids => appids, :business_group_id => @bg.ott_id, :tenants => tenants, :regions => regions, :currency => currency_code, :un_coupons => un_coupons, :coupon_count => 0 })
	        else
	          @errors = "Empty file not allowed"    
	        end  
	      else
	        @errors = "Please provide valid file format(xlsx)"  
	      end  
	    else
	      metadata = params.except!( *params_to_scrub1 ).merge({:app_ids => appids, :business_group_id => @bg.ott_id, :tenants => tenants, :regions => regions, :currency => currency_code })  
	    end  

	    # if params[:mode] == 'upload'
	    #   csv_file = params[:upload_coupon]
	    #   if csv_file.present?
	    #     un_coupons = []
	    #     csv_text = File.read(csv_file.path)
	    #     csv = CSV.parse(csv_text, :headers => true)
	    #     if csv.count > 0
	    #       csv.each_with_index do |row, i|
	    #         un_coupons << { :unique_code => row[0], :app_ids => JSON.parse(row[1])}
	    #       end
	    #       metadata = params.except!( *params_to_scrub ).merge({:app_ids => appids, :business_group_id => @bg.ott_id, :tenants => tenants, :regions => regions, :currency => currency_code, :un_coupons => un_coupons, :coupon_count => 0 })
	    #     else
	    #       @errors = "Empty CSV not allowed"
	    #     end
	    #   else
	    #     @errors = "Please Upload CSV file"
	    #   end             
	    # else
	    #   metadata = params.except!( *params_to_scrub1 ).merge({:app_ids => appids, :business_group_id => @bg.ott_id, :tenants => tenants, :regions => regions, :currency => currency_code })                  
	    # end
	    
	    if @errors.blank?
	      status = true
	      error = ""
	      maxtime = 60
	      body = {}
	      body["auth_token"] = @bg.ott_auth_token
	      body["content"] = metadata
	      catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "coupon", :tenant_id => params[:tenant_id])
	      url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+@bg.ott_auth_token
	      response = http_request("post",body,url,maxtime)
	      # byebug
	      if response["status"]
			catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "coupon")
			url = "http://"+get_ott_base_url+"/get_generated_coupons?auth_token="+@bg.ott_auth_token
			response = http_request("get",{},url,60)
			@couponitems = response["body"]
			redirect_to :coupons_generated_coupons
		  else
		  	@regions = Region.all
		  	session[:errors] = response["body"]["data"][0]
		  	redirect_to :back
	      end
	    else
	      @regions = Region.all
	      session[:errors] = @errors
	      redirect_to :back
	    end
	  end

	 def get_all
		status = true
		error = ""
        tenantid = params[:tenant_id]
		@bg = current_bg
		tenants = Tenant.find_by(:_id => tenantid)
		catalogs = Catalog.find_by(:bg_id => current_bg.id, :theme => "coupon", :tenant_id => tenantid)
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?region=IN&auth_token="+tenants.ott_auth_token
		response = http_request("get",{},url,60)
		couponitems = response["body"]["data"]
		status = response["status"]
		error  = response["error"]

		if status
		data_list = Kaminari.paginate_array(couponitems["items"]).page(params[:page])
        render json: {:items => couponitems, :total_count => data_list.total_count, :total_pages => data_list.total_pages}
        else
        render :json => {:error => error}.to_json, :status => 500
        end
	end

	def generated_coupons
	    bg = current_bg
	    if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
		page_size = params[:page_size] ? params[:page_size] : 10
		params[:page_size] = page_size
	    @tid = Tenant.find_by(:bg_id => bg.id).id
	    url = "http://"+get_ott_base_url+"/get_generated_coupons?auth_token="+bg.ott_auth_token+"&bg_id="+bg.ott_id
	    response = http_request("get",{},url,60)
	    couponitems = response["body"]
	    @couponitems = Kaminari.paginate_array(couponitems).page(page).per(page_size)
	    if params[:title].present?
	        @couponitems =  couponitems.select {|k| k if k["title"].to_s.match(/#{params[:title]}/i)}
	    end
	    @next_items = Kaminari.paginate_array(couponitems).page(next_page).per(page_size)
	end

	def delete_generated_coupons
		coupon_id = params[:coupon_id]
		bg = current_bg
		url = "http://"+get_ott_base_url+"/coupons/"+coupon_id+"/delete_generated_coupons?auth_token="+bg.ott_auth_token+"&bg_id="+bg.ott_id
		@response = http_request("delete",{},url,60)
		@couponitems = @response['body']['data'] if @response["status"]
	end	

	def generate_coupon_csv
	    coupon_id = params[:coupon_id]
	    bg = current_bg
	    url = "http://"+get_ott_base_url+"/generate_coupon_csv?auth_token="+bg.ott_auth_token+"&coupon_id="+coupon_id
	    @coupons = http_request("get",{},url,60)
	    if response['status'] != false
	      respond_to do |format|
	        format.html
	        format.csv do
	          headers['Content-Disposition'] = "attachment; filename=\"#{@coupons["body"]["title"]}_#{Time.now.in_time_zone.strftime("%a, %d %b %Y %H:%M:%S %p")}.csv\""
	          headers['Content-Type'] ||= 'text/csv'
	        end
	      end
	    end  
	 end

	def generate_status_csv
	    bg = current_bg
	    coupon_id = params[:coupon_id]
	    email = current_user.email
	    url = "http://"+get_ott_base_url+"/generate_coupon_status_csv?auth_token="+bg.ott_auth_token+"&coupon_id="+coupon_id+"&email="+email
	    @response = http_request("get",{},url,60) 
	end
end