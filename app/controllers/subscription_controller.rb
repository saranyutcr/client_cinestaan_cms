class SubscriptionController < ApplicationController

    include SubscriptionHelper
    include ApplicationHelper

	def new
		@bg = current_bg
		@all_tenants = Tenant.where(:bg_id => @bg.id).order_by(:created_at => 'desc')
	end

	def get_apps
		status = true
		error = "No apps"

		tenant_id = params[:tenant_id]
		apps = App.where(:tenant_id=>tenant_id)

		if status
        render json: {:apps => apps}
        else
        render :json => {:error => error}.to_json, :status => 500
        end
	end

	def create
		status = true
		error = ""
		@bg = current_bg
		if status
			metadata = subscription_items(@bg,params)
		    logger.debug "Subscription metadata is " + metadata.to_s
			
			status = metadata["status"]
			error = metadata["error"]
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end

	def view_all		
		@tenant_id = params[:tenant_id]
		@content_id = params[:content_id]
		@app_id = params[:app_id]
		@bg = current_bg
		if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
		page_size = params[:page_size] ? params[:page_size] : 10
		params[:page_size] = page_size
		catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "subscription", :tenant_id => params[:tenant_id])
		if @app_id == "0"
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+@content_id+"?auth_token="+@bg.ott_auth_token+"&region=IN&status=any"
		else
		apps = App.find_by(:ott_id => @app_id)
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+@content_id+"?auth_token="+apps.ott_auth_token+"&region=IN&status=any"
		end
		response = http_request("get",{},url,60)
		@subscription = response["body"]["data"]
		# if params[:method] == "viewall"
		@subtitle = @subscription["title"]
		@data_list = Kaminari.paginate_array(@subscription["plans"]).page(page).per(page_size)
    	if params[:title].present?
      	@data_list= @subscription["plans"].select {|k| k if k["title"].to_s.match(/#{params[:title]}/i)}
    	end
    	@next_items = Kaminari.paginate_array(@subscription["plans"]).page(next_page).per(page_size)
    	# end
	end

	def get_all
		status = true
		error = ""

		@bg = current_bg
		catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "subscription", :tenant_id => params[:tenant_id])
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?region=any&auth_token="+@bg.ott_auth_token
		response = http_request("get",{},url,60)
		@subscription = response["body"]["data"]
		status = response["status"]
		error  = response["error"]

		if status
		data_list = Kaminari.paginate_array(@subscription["items"]).page(params[:page])
        render json: {:items => @subscription, :total_count => data_list.total_count, :total_pages => data_list.total_pages}
        else
        render :json => {:error => error}.to_json, :status => 500
        end
	end

	def add_plan
	    bg = current_bg
	    status = true
	    error = ""
	    @app_id = params[:app_id]
	    @tenant_id = params[:tenant_id]
	    bg = current_bg
		@apps =  App.all_of(:bg_id => bg.id, :tenant_id => @tenant_id)
	    @content_id = params[:content_id] if params[:content_id].present?
	    catalog = Catalog.find_by(:bg_id => bg.id, :theme => "subscription", :tenant_id => params[:tenant_id])
	    @buckets = ""#Bucket.where(:bg_id => bg.id)
        render  "subscription/add_plan"
	end

	def add_plans
		status = true
		error = ""
		app_id = params[:app_id]
		@items_id = params[:content_id]
		@bg = current_bg
		@tenant_id = params[:tenant_id]
		catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "subscription", :tenant_id => params[:tenant_id])
		if app_id == "0"
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+@items_id+"?auth_token="+@bg.ott_auth_token+"&region=any&status=any"
		else
		apps = App.find_by(:ott_id => app_id)
		# url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+@items_id+"?auth_token="+apps.ott_auth_token+"&region=any&status=any"
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+@items_id+"?auth_token="+@bg.ott_auth_token+"&region=any&status=any"
		end
		response = http_request("get",{},url,60)
		@subscription = response["body"]["data"]

		if params[:method] == "viewall"
		#render "subscription/view_all"
		#render json: {:items => @subscription}
		subtitle = @subscription["title"]
		data_list = Kaminari.paginate_array(@subscription["plans"]).page(params[:page])
        render json: {:items => data_list, :subtitle=> subtitle, :total_count => data_list.total_count, :total_pages => data_list.total_pages}
         
		else
		if params[:method] == "view"
		if status
        render partial: "view_info"
        else
        render :json => {:error => error}.to_json, :status => 500
        end
    	else
    	render "subscription/add_plans"
    	end
    	end
	end

	def create_plan
	   status = true
	    error = ""
		items_id = params[:items_id]
	    @tenant_id = params[:tenant_id]
	    @app_id = params["app_id"]

	    @bg = current_bg
	    if status
	      metadata = create_sub_plan(@bg,params,@tenant_id)
	      logger.debug "Plans metadata is " + metadata.to_s

	      status = metadata["status"]
	    end
	    if status
	      render json: {} , status: :ok
	    else
	      error = metadata['body']['error']['message']
	      render :json => {:error => error}.to_json, :status => 500
	    end
	end

	def add_more_plans
		status = true
		error = ""
		items_id = params[:items_id]
		@tenant_id = params[:tenant_id]
		@bg = current_bg
		if status
			metadata = subscription_plans(@bg,params,items_id,@tenant_id)
		    logger.debug "Plans metadata is " + metadata.to_s
			
			status = metadata["status"]
			error = metadata["error"]
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end

	def get_plan
		status = true
		error = ""
		@app_id = params[:app_id]
		@plan_id = params[:plan_id]
		@tenant_id = params[:tenant_id]
		@bg = current_bg
		apps = App.find_by(:ott_id => @app_id)
		url = "http://"+get_ott_base_url+"/subscriptions/plans/"+@plan_id+"?auth_token="+apps.ott_auth_token+"&region=IN&status=any"
		response = http_request("get",{},url,60)
		@plans = response["body"]["data"]

		if params[:method] == "edit"
			render "subscription/edit_plans"
		else   
		if status
			render json: {:items => @plans}
		else
			render :json => {:error => error}.to_json, :status => 500
		end
		end
	end

	def get_user_pack
		status = true
		error = ""
		@bg = current_bg
		@user_id = params[:user_id].strip
		@tenant_id = params[:tenant_id]
		apps = App.find_by(:tenant_id => @tenant_id)
		url = "http://"+get_ott_base_url+"/admin/plans?auth_token="+apps.ott_auth_token+"&region=IN&user_id="+@user_id
		response = http_request("get",{},url,60)
		@packs = response["body"]["data"]
		if params[:method] == "view"
		if status
			@spacks = []

			@packs.each do |pa|
			pack = {}
			pack["user_id"] = pa["user_id"]
			pack["plan_id"] = pa["plan_id"]
			pack["region"] = pa["region"]
			pack["category"] = pa["category"]
			pack["valid_till"] = DateTime.parse(pa["valid_till"]).strftime("%B %d %y %H:%M:%S")
			pack["start_date"] = DateTime.parse(pa["start_date"]).strftime("%B %d %y %H:%M:%S")
			@spacks << pack
			end

			@spacks = Kaminari.paginate_array(@spacks).page(params[:page])
        	render json: {:packs => @spacks, :total_count => @spacks.total_count, :total_pages => @spacks.total_pages}
			#render json: {:packs => @packs}
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	   else
	   	if @packs.nil?
	   		error = response["body"]["error"]["message"]
	   		puts "error======"
	   		p error
	   		render :json => {:error => error}, :status => 500
	   	elsif @packs.empty?
	   		render partial: "user_data"
	   	else
		lastpack = @packs.last["valid_till"]
		puts "@lastpackvalid======"
		cdt = DateTime.parse(lastpack)
		@lastpackvalid = cdt.strftime("%B %d %y %H:%M:%S")
		p @lastpackvalid
		render partial: "user_data"
		end
	   	
	   end
	end

  def edit_plan
    status = true
    error = ""
    @bg = current_bg
    @tenant_id = params[:tenant_id]
    @plan_id = params[:plan_id]
    status = true
    error = ""
    maxtime = 60
    catalog = Catalog.find_by(:bg_id => @bg.id, :theme => "subscription", :tenant_id => params[:tenant_id])
    url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/items/"+@plan_id+"?region=any&auth_token="+@bg.ott_auth_token
    response = http_request("get",{},url,60)
    @plan = response["body"]["data"]
    body = {}
    body["auth_token"] = @bg.ott_auth_token
    metadata ={}
    metadata["business_group_id"] = @bg.ott_id

    metadata["app_ids"] = [@plan["app_ids"]].flatten
    #metadata["tenant_ids"] = []
    metadata["title"] = @plan["title"]
    metadata["plan_type"] = (@plan["category"].nil?) ||( @plan["category"] == "") ? "all_access_plans" : @plan["category"] #@plan["category"].nil? ? "" : @plan["category"]
    metadata["plans"] = []
    metadata["status"] = params["status"]
    body["content"] = metadata
    catalog = Catalog.find_by(:bg_id => @bg.id, :theme => "subscription", :tenant_id => params[:tenant_id])
    url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/items/"+@plan_id+"?region=IN&auth_token="+@bg.ott_auth_token+"&status=edit"
    response = http_request("put",body,url,maxtime)
    if response["code"] == 200
      render json: {} , status: :ok
    else
      render :json => {:error => response["body"]["error"]["message"]}.to_json, :status => 500
    end
  end
	def tenant_plans
		@bg = current_bg
		@tid = params[:tenant_id]
		env = Rails.env
		case env
		when "development"
			@webview = @bg.staging_webview
		when "preproduction"
			@webview = @bg.preprod_webview
		when "production"
			@webview = @bg.prod_webview
		end
		if params[:pack] == "pack"
		render partial: "get_user"
		elsif params[:coupon] == "coupon"
		render partial: "coupons_data"
	    elsif params[:access] == "access"
		render partial: "/access/view_all"
		elsif params[:appsdata] == "appsdata"
		render partial: "/apps/view_all"
		elsif params[:appsmessages] == "appsmessages"
		render partial: "/message/view_all"
		else			
		render partial: "plans_data"
		end

	end 
end
