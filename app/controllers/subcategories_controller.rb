class SubcategoriesController < ApplicationController
	before_filter :set_object
	include ApplicationHelper

	def set_object
	  @subcategory = Subcategory.new
	  @show = ShowTheme.new
	  @media = Media.new
	end

	def new
		@bg = current_bg

		
		if params[:show_id] ##Create Subcategory under Show
			#Check added for ott id 
			@show_id = params[:show_id]
			@cms_show_object = (@show.get_show_by_id(params[:show_id])).as_json(:except => ["updated_at","created_at"]) || \
			                   (@show.get_show_by_ott_id(params[:show_id])).as_json(:except => ["updated_at","created_at"])
			url = "http://" + get_ott_base_url + \
			      "/catalogs/" + @cms_show_object["ott_catalog_id"] + \
			      "/items/" + @cms_show_object["ott_id"] + \
			      "?auth_token=" + @bg.ott_auth_token + "&region=any&status=any"
			response = http_request("get",{},url,60)
			@ott_show = response["body"]["data"]

			# Upload image to s3 server
		  	s3uploadremovspace = @bg.name+"/"+@ott_show["catalog_name"]+"/shows/"+@ott_show["title"]+"/subcategories"
		  	s3upload = s3uploadremovspace.gsub(" ", "_");
		  	@signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)


		else ##Create Subcategory under Subcategory
			@subcategory_id = params[:subcategory_id]
			@cms_subcategory_object = (@subcategory.get_subcategory_by_id(params[:subcategory_id])).as_json(:except => ["updated_at","created_at"])
			url = "http://" + get_ott_base_url + \
			      "/catalogs/shows/subcategories/" + @cms_subcategory_object["ott_id"] + \
			      "?auth_token=" + @bg.ott_auth_token + "&region=any&status=any"
			response = http_request("get",{},url,60)
			@ott_subcategory = response["body"]["data"]

			if @ott_subcategory["nested_subcategory_name"].nil? || @ott_subcategory["nested_subcategory_name"].empty?
			# Upload image to s3 server
		  	s3uploadremovspace = @bg.name+"/"+@ott_subcategory["catalog_name"]+"/shows/"+@ott_subcategory["show_name"]+"/subcategories/"+@ott_subcategory["title"]+"/subcategories"
		  	s3upload = s3uploadremovspace.gsub(" ", "_");
		  	@signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)
		    else
		    # Upload image to s3 server
		  	s3uploadremovspace = @bg.name+"/"+@ott_subcategory["catalog_name"]+"/shows/"+@ott_subcategory["show_name"]+"/subcategories/"+@ott_subcategory["nested_subcategory_name"]+"/subcategories/"+@ott_subcategory["title"]+"/subcategories"
		  	s3upload = s3uploadremovspace.gsub(" ", "_");
		  	@signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)
		    end

		end	
	end	

	def create
		if params[:cms_show_object]
			params[:cms_show_object] = eval(params[:cms_show_object])
			params[:ott_show_object] = eval(params[:ott_show_object])
			params[:type] = "show"
		else
			params[:cms_subcategory_object] = eval(params[:cms_subcategory_object])
			params[:ott_subcategory_object] = eval(params[:ott_subcategory_object])
			params[:type] = "nested"
		end	

		if params[:child_flag] == "episodes"
	      params[:subcategories_flag] = "no"
	      params[:episodes_flag] = "yes"
	    else
	      params[:subcategories_flag] = "yes"
	      params[:episodes_flag] = "no"
	    end 
    
		params[:bg_object] = current_bg.as_json
	    status,error = @subcategory.create_at_ott(params)
	    if status
	    	render json: {} , status: :ok
	    else
	    	render :json => {:error => error}.to_json, :status => 500
	    end
	end	

	def view_all
	end
	
	def get_all
		if params[:page].nil?
				page = 0
				next_page = page + 1
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
			page_size = 25

		@subcategories = @subcategory.get_limited_subcategories_by_bg(current_bg,page,page_size)  
		@next_subcategories = @subcategory.get_limited_subcategories_by_bg(current_bg,next_page,page_size) 
=begin
    	data = subcategories.as_json.map do |subcategory|
	    	tenant_names = []
	        tenants = subcategory["tenant_ids"]
	        tenants.each do |t|
	          name = Tenant.find(t).name
	          tenant_names << name if name
	        end 
        	catalog_name = Catalog.find_by(:id => subcategory["catalog_id"]).name
        	show_name = (@show.get_show_by_id(subcategory["show_id"])).title
        	subcategory.merge!({"tenant_names" => tenant_names,
        	                  "catalog_name" => catalog_name,
        	                  "show_name" => show_name })	
    	end
=end
=begin
    	next_data = next_subcategories.as_json.map do |subcategory|
	    	tenant_names = []
	        tenants = subcategory["tenant_ids"]
	        tenants.each do |t|
	          name = Tenant.find(t).name
	          tenant_names << name if name
	        end 
        	catalog_name = Catalog.find_by(:id => subcategory["catalog_id"]).name
        	show_name = (@show.get_show_by_id(subcategory["show_id"])).title
        	subcategory.merge!({"tenant_names" => tenant_names,
        	                  "catalog_name" => catalog_name,
        	                  "show_name" => show_name })	
    	end
=end
    #render json: {:list => data,:next_list => next_data}
	end	

	def view_subcategories
		@bg = current_bg
		@show_id = params[:show_id]
		@show_data = @show.get_show_by_id(params[:show_id]) || @show.get_show_by_ott_id(params[:show_id])
		if @show_data["subcategories_flag"] == "yes" 
			if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
			page_size = params[:page_size] ? params[:page_size] : 25
			params[:page_size] = page_size

			subcategories = Subcategory.where(:ott_show_id => params["show_id"],:parent_subcategory_id => "show").page(page).per(page_size)
			@subcategories = []
			subcategories.each do |sub|
				catalog_name = Catalog.find(sub["catalog_id"]).name
				show_name = @show.get_show_by_id(sub["show_id"]).title
				sub["catalog_name"] = catalog_name
				sub["show_name"] = show_name
			    @subcategories << sub
			end
			@next_items = Subcategory.where(:ott_show_id => params["show_id"],:parent_subcategory_id => "show").page(next_page).per(page_size)
			if params[:title].present?
		        @subcategories = Subcategory.where(:ott_show_id => params["show_id"],:parent_subcategory_id => "show").where(:title => /#{params[:title]}/i)
      		end
		end
		
	    if @show_data["episodes_flag"] == "yes"
	    	if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
			page_size = params[:page_size] ? params[:page_size] : 25
			params[:page_size] = page_size

			url = "http://"+get_ott_base_url+"/catalogs/shows/"+@show_data.ott_id + "/episodes?region=any&status=any&auth_token="+current_bg.ott_auth_token
			response = http_request("get",{},url,60)
			if response["code"] == 200
				catalog = response["body"]["data"]
				episodes = response["body"]["data"]["items"]
				@items = []
				episodes.each do |episode|
					mid = episode["content_id"]
					media = Media.find_by(:ott_id => mid)
					episode["cms_id"] = media.id
					episode["cms_status"] = media.status
					episode["priority"] = media.priority
					msid = media.show_id
					if msid.nil?
						episode["show_id"] = params[:show_id]
					else
						episode["show_id"] = msid
					end
				 	@items << episode 
				end
				if params[:title].present?
		        @items = @items.select {|k| k if k["title"].to_s.match(/#{params[:title]}/i)}
      			end
				@items = Kaminari.paginate_array(@items).page(page).per(page_size)
			end
			next_url = "http://"+get_ott_base_url+"/catalogs/shows/"+@show_data.ott_id + "/episodes?region=any&episode_type=any&status=any&auth_token="+current_bg.ott_auth_token
			response = http_request("get",{},next_url,60)
			@next_items = Kaminari.paginate_array(response["body"]["data"]["items"]).page(next_page).per(page_size)

	    end   


	end	

	def show_subcategories
		cms_subcategories = (@subcategory.get_subcategories_by_show_id(params[:show_id])).as_json(:except => ["updated_at","created_at"])
		cms_subcategories.map do |sub|
			catalog_name = Catalog.find(sub["catalog_id"]).name
			show_name = @show.get_show_by_id(sub["show_id"]).title
			sub["catalog_name"] = catalog_name
			sub["show_name"] = show_name
		end	
		data_list = Kaminari.paginate_array(cms_subcategories).page(params[:page])
        render json: {:list => cms_subcategories, :total_count => data_list.total_count, :total_pages => data_list.total_pages}
		#render json: {:list => cms_subcategories}
	end	

	def view_nested_subcategories
	  @bg = current_bg
	  @subcategory_id = params[:subcategory_id]
	  @subcategory_data = @subcategory.get_subcategory_by_id(params[:subcategory_id])
	  if @subcategory_data["subcategories_flag"] == "yes"
	  		if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
			page_size = params[:page_size] ? params[:page_size] : 25
			params[:page_size] = page_size

	  	 	cms_nested_subcategories = Subcategory.where(:parent_subcategory_id => sub_id).page(page).per(page_size)
	  	 	@subcategories = []
			cms_nested_subcategories.each do |sub|
				catalog_name = Catalog.find(sub["catalog_id"]).name
				show_name = @show.get_show_by_id(sub["show_id"]).title
				sub["catalog_name"] = catalog_name
				sub["show_name"] = show_name
				@subcategories << sub
			end	
			@next_items = Subcategory.where(:parent_subcategory_id => sub_id).page(next_page).per(page_size)		
	  		if params[:title].present?
		        @subcategories = Subcategory.where(:parent_subcategory_id => sub_id).where(:title => /#{params[:title]}/i)
      		end
	  end	

	  if @subcategory_data["episodes_flag"] == "yes"
	    	if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
			page_size = params[:page_size] ? params[:page_size] : 25
			params[:page_size] = page_size

			url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+\
						@subcategory_data["ott_id"]+"/episodes?region=any&status=any&auth_token="+\
						current_bg.ott_auth_token
			response = http_request("get",{},url,60)
			if response["code"] == 200
				catalog = response["body"]["data"]
				episodes = response["body"]["data"]["items"]
				@items = []
				episodes.each do |episode|
					mid = episode["content_id"]
					media = Media.find_by(:ott_id => mid)
					episode["cms_id"] = media.id
					episode["cms_status"] = media.status
					episode["priority"] = media.priority
					msid = media.show_id
					if msid.nil?
						episode["show_id"] = params[:show_id]
					else
						episode["show_id"] = msid
					end
				 	@items << episode 
				end
				  if params[:title].present?
		        @items = @items.select {|k| k if k["title"].to_s.match(/#{params[:title]}/i)}
      			end
				@items = Kaminari.paginate_array(@items).page(page).per(page_size)
			end
			
			next_url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+\
						@subcategory_data["ott_id"]+"/episodes?region=any&status=any&auth_token="+\
						current_bg.ott_auth_token
			response = http_request("get",{},next_url,60)
			@next_items = Kaminari.paginate_array(response["body"]["data"]["items"]).page(next_page).per(page_size)

	    end   
	end
	
	def get_nested_subcategories	
		cms_subcategories = (@subcategory.get_subcategories_by_id(params[:subcategory_id])).as_json(:except => ["updated_at","created_at"])
			cms_subcategories.map do |sub|
				catalog_name = Catalog.find(sub["catalog_id"]).name
				show_name = @show.get_show_by_id(sub["show_id"]).title
				sub["catalog_name"] = catalog_name
				sub["show_name"] = show_name
			end	
		  render json: {:list => cms_subcategories}
	end
	
	#Business group can edit title,description,status,featured image,landscape image and portrait image
  	def edit
  		status = true
	    @bg = current_bg
	    
	    if params[:show_id] ##Create Subcategory under Show
			#Check added for ott id 
			@cms_show_object = (@show.get_show_by_id(params[:show_id])).as_json(:except => ["updated_at","created_at"]) || \
			                   (@show.get_show_by_ott_id(params[:show_id])).as_json(:except => ["updated_at","created_at"])
			url = "http://" + get_ott_base_url + \
			      "/catalogs/" + @cms_show_object["ott_catalog_id"] + \
			      "/items/" + @cms_show_object["ott_id"] + \
			      "?auth_token=" + @bg.ott_auth_token + "&region=any&status=any"
			response = http_request("get",{},url,60)
			@ott_show = response["body"]["data"]
			@show_id = params[:show_id]

			@cms_subcategory_object = (@subcategory.get_subcategory_by_id(params[:id])).as_json(:except => ["updated_at","created_at"])
			url = "http://" + get_ott_base_url + \
			      "/catalogs/shows/subcategories/" + @cms_subcategory_object["ott_id"] + \
			      "?auth_token=" + @bg.ott_auth_token + "&region=any&status=any"
			response = http_request("get",{},url,60)
			@ott_subcategory = response["body"]["data"]
			@subcategory_id = params[:id]
			# Upload image to s3 server
		  	s3uploadremovspace = @bg.name+"/"+@ott_show["catalog_name"]+"/shows/"+@ott_show["title"]+"/subcategories"
		  	s3upload = s3uploadremovspace.gsub(" ", "_");
		  	@signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)

		else ##Create Subcategory under Subcategory
			@cms_subcategory_object = (@subcategory.get_subcategory_by_id(params[:subcategory_id])).as_json(:except => ["updated_at","created_at"])
			url = "http://" + get_ott_base_url + \
			      "/catalogs/shows/subcategories/" + @cms_subcategory_object["ott_id"] + \
			      "?auth_token=" + @bg.ott_auth_token + "&region=any&status=any"
			response = http_request("get",{},url,60)
			@ott_subcategory = response["body"]["data"]
			@subcategory_id = params[:subcategory_id]
            
            if params[:subcategory_id]
				@cms_nested_subcategory_object = (@subcategory.get_subcategory_by_id(params[:id])).as_json(:except => ["updated_at","created_at"])
				url = "http://" + get_ott_base_url + \
			      "/catalogs/shows/subcategories/" + @cms_nested_subcategory_object["ott_id"] +  "?auth_token=" + @bg.ott_auth_token + "&region=any&status=any"
			    response = http_request("get",{},url,60)
			    @ott_nested_subcategory = response["body"]["data"]
				
            end

			if @ott_subcategory["nested_subcategory_name"].nil? || @ott_subcategory["nested_subcategory_name"].empty?
			# Upload image to s3 server
		  	s3uploadremovspace = @bg.name+"/"+@ott_subcategory["catalog_name"]+"/shows/"+@ott_subcategory["show_name"]+"/subcategories/"+@ott_subcategory["title"]+"/subcategories"
		  	s3upload = s3uploadremovspace.gsub(" ", "_");
		  	@signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)
		    else
		    # Upload image to s3 server
		  	s3uploadremovspace = @bg.name+"/"+@ott_subcategory["catalog_name"]+"/shows/"+@ott_subcategory["show_name"]+"/subcategories/"+@ott_subcategory["nested_subcategory_name"]+"/subcategories/"+@ott_subcategory["title"]+"/subcategories"
		  	s3upload = s3uploadremovspace.gsub(" ", "_");
		  	@signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)
		    end
		end	

	    sub = @subcategory.get_subcategory_by_id(params[:id])
	    if sub
	      url = "http://" + get_ott_base_url + "/catalogs/shows/subcategories/" + sub.ott_id + "?region=IN&auth_token=" + @bg.ott_auth_token
	      response = http_request("get",{},url,60)
		  	     
	      @sub_resp = response["body"]["data"]

	      if params[:show_id]
	      	caturl = "http://" + get_ott_base_url + "/catalogs/" + @cms_show_object["ott_catalog_id"] + "?auth_token=" + @bg.ott_auth_token
	      else
	      	caturl = "http://" + get_ott_base_url + "/catalogs/" + @cms_subcategory_object["ott_catalog_id"] + "?auth_token=" + @bg.ott_auth_token
	      end
			response = http_request("get",{},caturl,60)
			if status
				@catalog = response["body"]["data"]
				@t_ott_id = response["body"]["data"]["tenant_ids"]
				@tenant_list = []

				@t_ott_id.each do |obj|
				  Tenant.where(:ott_id=>obj).each do |tenan|
				    h = {}
				    h["id"] = tenan.id
				    h["name"] = tenan.name
				    h["ott_id"] = tenan.ott_id

				    h ["genres"] =[]
				      response["body"]["data"]["genres"].keys.each do |g|
				      h["genres"] = (response["body"]["data"]["genres"][tenan.ott_id]).map{|gen| gen["name"] } if g == h["ott_id"]
				    end

				    h ["languages"] =[]
				    response["body"]["data"]["languages"].keys.each do |l|
				      h ["languages"] = (response["body"]["data"]["languages"][tenan.ott_id]).map{|lan| lan["name"] } if l == h["ott_id"]
				    end 

				    # h["applications"] = []
				    # App.where(:tenant_id => tenan.id).each do |tapp|
				    #   if @catalog["application_ids"].include? tapp.ott_id 
				    #     application = {}
				    #     application["a_id"] = tapp.id
				    #     application["a_name"] = tapp.name
				    #     application["a_tid"] = tapp.tenant_id
				    #     h['applications'] << application
				    #   end
				    # end
				    @tenant_list << h
				  end
				end
			end
	      render "edit"
	    else
	        
	    end
  	end 

  	def update
  		if params[:method] == "update"
		    if params[:cms_show_object]
				params[:cms_show_object] = eval(params[:cms_show_object])
				params[:ott_show_object] = eval(params[:ott_show_object])
				params[:type] = "show"
			else
				params[:cms_subcategory_object] = eval(params[:cms_subcategory_object])
				params[:ott_subcategory_object] = eval(params[:ott_subcategory_object])
				params[:type] = "nested"
			end	
			params[:bg_object] = current_bg.as_json
			params[:sub_object] = eval(params[:sub_object])
			params[:subcategories_flag] = params[:sub_object][:subcategory_flag]
      		params[:episodes_flag] = params[:sub_object][:episode_flag]
		    status,error = @subcategory.update_at_ott(params)
		    if status
		    	render json: {} , status: :ok
		    else
		    	render :json => {:error => error}.to_json, :status => 500
		    end 
    	end

    	if params[:method] == "delete"
    		subcategory = @subcategory.get_subcategory_by_id(params[:id])
    		if subcategory.episodes_flag == "yes"
    			episodes = @media.get_media_by_subcategory(params[:id])
    			if episodes.count > 0
	    			render :json => {:error => "Please delete epsiodes under this Subcategory!"}.to_json, :status => 500
	    			return
	    		else
	    			##Delete Subcategory from OTT
	    			status,msg = subcategory.delete_subcategory_from_ott

	    			##Delete Subcategory from CMS
	    			if status
	    				status,msg = subcategory.delete_subcategory_from_cms
	    			end
	    		end	
    		else
    			subcategories = @subcategory.get_subcategories_by_parent_id(params[:id])
    			if subcategories.count > 0	
    				render :json => {:error => "Please delete Subcategories under this Subcategory!"}.to_json, :status => 500
    				return
    			else
    				##Delete Subcategory from OTT
	    			status,msg = subcategory.delete_subcategory_from_ott

	    			##Delete Subcategory from CMS
	    			if status
	    				status,msg = subcategory.delete_subcategory_from_cms
	    			end
    			end
    		end	
    		
    		if status
				render json: {} , status: :ok
			else
			   	render :json => {:error => msg}.to_json, :status => 500
			end	

    	end	

  	end	

end
