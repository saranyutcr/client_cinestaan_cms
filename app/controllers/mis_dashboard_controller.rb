class MisDashboardController < ApplicationController
	include ApplicationHelper
	include GaCommonCode
	  @@region = "IN"
	  @@credentials_file = File.read("#{Rails.root}/config/cinestaan-staging-c70fe0aee983.json")
	def dashboard
	end

	def get_top_items
		start_date,end_date = static_date
		@top_content = get_top_25_content(start_date,end_date)
		person_info = get_person_info
		person_data = person_info.flatten.collect{|i| {:cname=>i["title"],:content_id=>i["content_id"]}}
		@top_content.each do |i|
		person_data.each{|j| i["content_owner_name"] = j[:cname] if i["content_owner_id"] == j[:content_id]}
		end
	end

	def get_top_items_on_daterange
		start_date,end_date = dynamic_date params[:from_date],params[:to_date]
		@top_content = get_top_25_content(start_date,end_date)
		person_info = get_person_info
		person_data = person_info.flatten.collect{|i| {:cname=>i["title"],:content_id=>i["content_id"]}}
		@top_content.each do |i|
		person_data.each{|j| i["content_owner_name"] = j[:cname] if i["content_owner_id"] == j[:content_id]}
		end
		render partial: 'get_top_items_table'
	end

	def get_all_items
		start_date,end_date = static_date
		@all_content = get_all_content_descending_order(start_date,end_date)
		person_info = get_person_info
		person_data = person_info.flatten.collect{|i| {:cname=>i["title"],:content_id=>i["content_id"]}}
		@all_content.each do |i|
		person_data.each{|j| i["content_owner_name"] = j[:cname] if i["content_owner_id"] == j[:content_id]}
		end
		@all_content_pagination = Kaminari.paginate_array(@all_content).page(params[:page]).per(25)
	end

	def get_all_items_on_daterange
		start_date,end_date = dynamic_date params[:from_date],params[:to_date]
		@all_content = get_all_content_descending_order(start_date,end_date)
		person_info = get_person_info
		person_data = person_info.flatten.collect{|i| {:cname=>i["title"],:content_id=>i["content_id"]}}
		@all_content.each do |i|
		person_data.each{|j| i["content_owner_name"] = j[:cname] if i["content_owner_id"] == j[:content_id]}
		end
		@all_content_pagination = Kaminari.paginate_array(@all_content).page(params[:page]).per(25)
		render partial: 'all_items_table'
	end

	def content_performance_by_owner
		cname = []
		@co_hash = {}
		content_owner_data = []
		start_date,end_date = static_date
		person_info = get_person_info
		person_info.flatten.each do |i|
		 	cname << i["title"]
		   	content_owner = get_content_owner_data(i["content_id"], start_date, end_date)
		 	content_owner_data << content_owner['total_views']
		end
		cname.zip(content_owner_data) { |a,b| @co_hash[a] = b } 	
	end

	def content_performance_by_owner_on_daterange
		cname = []
		@co_hash = {}
		content_owner_data = []
		start_date,end_date = dynamic_date params[:from_date],params[:to_date]
		person_info = get_person_info
		person_info.flatten.each do |i|
		 	cname << i["title"]
		   content_owner = get_content_owner_data(i["content_id"], start_date, end_date)
		 	content_owner_data << content_owner['total_views']
		end
		cname.zip(content_owner_data) { |a,b| @co_hash[a] = b } 
		render partial: 'content_performance_by_owner_table'
	end

	def language_data
		start_date,end_date = static_date
		ga_data = get_ga_language_data start_date,end_date
		@language_ga_data = process_ga_language_data ga_data
	end

	def language_data_on_daterange
		start_date,end_date = dynamic_date params[:from_date],params[:to_date]
		ga_data = get_ga_language_data start_date,end_date
		@language_ga_data = process_ga_language_data ga_data
		render partial: 'language_data_graph'
	end

	def consumption_by_category
		start_date,end_date = static_date
		ga_data = get_ga_category_data start_date,end_date
		@consumption_by_category_ga_data = process_ga_category_data ga_data
		
	end

	def consumption_by_category_on_daterange
		start_date,end_date = dynamic_date params[:from_date],params[:to_date]
		ga_data = get_ga_category_data start_date,end_date
		@consumption_by_category_ga_data = process_ga_category_data ga_data
		render partial: 'consumption_by_category_graph'
	end

	def content_by_geography_country
		start_date,end_date = static_date
		ga_data = get_ga_country_data start_date,end_date
		@country_ga_data = process_ga_country_data ga_data
		
	end

	def content_by_geography_country_on_daterange
		start_date,end_date = dynamic_date params[:from_date],params[:to_date]
		ga_data = get_ga_country_data start_date,end_date
		@country_ga_data = process_ga_country_data ga_data
		render partial: 'content_by_country'
	end
	
	def content_by_geography_city
		start_date,end_date = static_date
		ga_data = get_ga_city_data start_date,end_date
		@city_ga_data = process_ga_city_data ga_data,params[:country]
		
	end

	def content_by_geography_city_on_daterange
		start_date,end_date = dynamic_date params[:from_date],params[:to_date]
		ga_data = get_ga_city_data start_date,end_date
		@city_ga_data = process_ga_city_data ga_data,params[:country]
		render partial: 'content_by_geography_city_graph'
	end

	def content_by_geography_zipcode
		start_date,end_date = static_date
		ga_data = get_ga_zipcode_data start_date,end_date
		@zipcode_ga_data = process_ga_zipcode_data ga_data,params[:country],params[:city]
	end

	def content_by_geography_zipcode_on_daterange
		start_date,end_date = dynamic_date params[:from_date],params[:to_date]
		ga_data = get_ga_zipcode_data start_date,end_date
		@zipcode_ga_data = process_ga_zipcode_data ga_data,params[:country],params[:city]
		render partial: 'content_by_geography_zipcode_graph'
	end

	def users_by_device
		start_date,end_date = static_date
		ga_data = get_ga_device_data start_date,end_date
		@users_by_device_ga_data = process_ga_device_data ga_data
	end

	def users_by_device_on_daterange
		start_date,end_date = dynamic_date params[:from_date],params[:to_date]
		ga_data = get_ga_device_data start_date,end_date
		@users_by_device_ga_data = process_ga_device_data ga_data
		render partial: 'user_by_device_graph'
	end

	private

	def static_date
		tdate = DateTime.now - 30
		start_date = tdate.strftime("%Y-%m-%d")
		datetime_now = DateTime.now
		end_date = datetime_now.strftime("%Y-%m-%d")
		return [start_date,end_date]
	end

	def dynamic_date sdate,edate
		from_date = DateTime.parse(sdate)
		start_date = from_date.strftime("%Y-%m-%d")
		to_date = DateTime.parse(edate)
		end_date = to_date.strftime("%Y-%m-%d")
		return [start_date,end_date]
	end

	def get_person_catalogs
		catalogs_api = "http://"+get_ott_base_url+ "/catalogs?auth_token=" + current_bg.ott_auth_token + "&region=" + @@region + "&catalog_type=non_media"
		response = http_request("get",{},catalogs_api,60)
		catalogs = response["body"]["data"]["items"]
		return catalogs
	end

	def get_content_owner_data(content_owner_id, start_date, end_date)
		# Get all catalog items of a given content owner
		catalog_items = get_all_catalog_items(content_owner_id)
		# Get data from GA in the given date range (including start and end date)
		ga_data = get_ga_data(start_date, end_date)
		aggregate_ga_data = process_ga_data(ga_data)
		# Merge catalog with ga data.
		merged_data = merge_catalog_with_ga_data(catalog_items, aggregate_ga_data)
		return merged_data
	end
	
	def get_person_info
		items = []
		person_catalog = get_person_catalogs
			 unless person_catalog.blank?
			   person_data = person_catalog.select {|i| i["theme"] == "person" && i["layout_type"] == "content_owner"}
			 end 
			 person_data.each do |i|
			 	catalog_items = get_catalog_items i["catalog_id"]
			 	items << catalog_items 
			 end
		return items
	end

	def sort_descending(merged_data)
		sorted = merged_data.sort_by { |k| k["views"].to_i }
		return sorted.reverse
	end 

	def get_all_content_descending_order(start_date, end_date)
		merged_data = get_content_owner_data(nil, start_date, end_date)
		# Sort in descending order of views
		sorted_data = sort_descending(merged_data["merged_data"])
		return sorted_data
	end

	def get_top_25_content(start_date, end_date)
		sorted_data = get_all_content_descending_order(start_date, end_date)
		return sorted_data[0..24]
	end

	def get_ga_country_data(startdate, enddate)
		parameters = {"reportRequests" => [{"viewId" => "141655433","dateRanges" => [{"startDate" => startdate,"endDate" => enddate}], "metrics" => [{"expression" => "ga:totalEvents"},{"expression" => "ga:metric15"},{"expression" => "ga:metric16"},{"expression" => "ga:metric17"},{"expression" => "ga:metric3"},{"expression" => "ga:metric4"},{"expression" => "ga:metric5"},{"expression" => "ga:metric6"},{"expression" => "ga:metric7"}],"dimensions" => [{"name" =>"ga:eventCategory"},{"name" => "ga:eventAction"},{"name" => "ga:country"}]}]}
		response = get_analytics(parameters, @@credentials_file)
		if response.code == 200
			ga_data = JSON.parse(response.body)
			return ga_data['reports'][0]
		end
		return nil
	end 

	def process_ga_country_data(ga_data)
		aggregate_ga_data = []
		unless ga_data.nil?
		if !ga_data["data"]["rows"].nil?
			ga_data["data"]["rows"].each do |row|
				# aggegate content
				content_index = ga_data["data"]["rows"].each_index.select{|i| ga_data["data"]["rows"][i]['dimensions'][2] == row['dimensions'][2]}
				h = {}
				h["country"] = row['dimensions'][2]
				content_index.each do |i|	
					if(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginAttempt")
						h["LoginAttempt"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][1]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginSuccess")
						h["LoginSuccess"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][2]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginFailure")
						h["LoginFailure"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][3]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "playStart")
						h["playStart"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][4]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "playComplete")
						h["playComplete"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][5]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "PlaybackTime")
						h["PlaybackTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][6]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LivePlaybackTime")
						h["LivePlaybackTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][7]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "PlaybackBufferingTime")
						h["PlaybackBufferingTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][8]
					end	
				end
				aggregate_ga_data.push(h)
				ga_data["data"]["rows"].delete_if { |x| x['dimensions'][2] == row['dimensions'][2] }
			end
		end
		end
		return aggregate_ga_data
	end

	def get_ga_city_data(startdate, enddate)
		parameters = {"reportRequests" => [{"viewId" => "141655433","dateRanges" => [{"startDate" => startdate,"endDate" => enddate}], "metrics" => [{"expression" => "ga:totalEvents"},{"expression" => "ga:metric15"},{"expression" => "ga:metric16"}, {"expression" => "ga:metric17"},{"expression" => "ga:metric3"},{"expression" => "ga:metric4"},{"expression" => "ga:metric5"},{"expression" => "ga:metric6"},{"expression" => "ga:metric7"}],"dimensions" => [{"name" =>"ga:eventCategory"},{"name" => "ga:eventAction"},{"name" => "ga:country"},{"name" => "ga:city"}]}]}
		response = get_analytics(parameters, $credentials_file)
		if response.code == 200
			ga_data = JSON.parse(response.body)
			return ga_data['reports'][0]
		end
		return nil
	end 

	def process_ga_city_data(ga_data, country)
		aggregate_ga_data = []
		unless ga_data.nil?
		if !ga_data["data"]["rows"].nil?
			# Delete unnecessary data
			ga_data["data"]["rows"].delete_if { |row| row['dimensions'][2] != country }
			ga_data["data"]["rows"].each do |row|
				# aggegate content
				content_index = ga_data["data"]["rows"].each_index.select{|i| ga_data["data"]["rows"][i]['dimensions'][3] == row['dimensions'][3]}
				h = {}
				h["country"] = country
				h["city"] = row['dimensions'][3]
				content_index.each do |i|	
					if(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginAttempt")
						h["LoginAttempt"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][1]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginSuccess")
						h["LoginSuccess"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][2]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginFailure")
						h["LoginFailure"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][3]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "playStart")
						h["playStart"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][4]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "playComplete")
						h["playComplete"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][5]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "PlaybackTime")
						h["PlaybackTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][6]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LivePlaybackTime")
						h["LivePlaybackTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][7]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "PlaybackBufferingTime")
						h["PlaybackBufferingTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][8]
					end	
				end
				aggregate_ga_data.push(h)
				ga_data["data"]["rows"].delete_if { |x| x['dimensions'][2] == row['dimensions'][2] }
			end
		end
		end
		return aggregate_ga_data
	end
	
	def get_ga_zipcode_data(startdate, enddate)
		parameters = {"reportRequests" => [{"viewId" => "141655433","dateRanges" => [{"startDate" => startdate,"endDate" => enddate}], "metrics" => [{"expression" => "ga:totalEvents"},{"expression" => "ga:metric15"},{"expression" => "ga:metric16"},{"expression" => "ga:metric17"},{"expression" => "ga:metric3"},{"expression" => "ga:metric4"},{"expression" => "ga:metric5"},{"expression" => "ga:metric6"},{"expression" => "ga:metric7"}],"dimensions" => [{"name" =>"ga:eventCategory"},{"name" => "ga:eventAction"},{"name" => "ga:country"},{"name" => "ga:city"},{"name" => "ga:dimension17"}]}]}
		response = get_analytics(parameters, $credentials_file)
		if response.code == 200
			ga_data = JSON.parse(response.body)
			return ga_data['reports'][0]
		end
		return nil
	end 
	def process_ga_zipcode_data(ga_data, country, city)
		aggregate_ga_data = []
		if !ga_data["data"]["rows"].nil?
			# Delete unnecessary data
			ga_data["data"]["rows"].delete_if { |row| row['dimensions'][2] != country }
			ga_data["data"]["rows"].delete_if { |row| row['dimensions'][3] != city }
			ga_data["data"]["rows"].each do |row|
				# aggegate content
				content_index = ga_data["data"]["rows"].each_index.select{|i| ga_data["data"]["rows"][i]['dimensions'][4] == row['dimensions'][4]}
				h = {}
				h["country"] = country
				h["city"] = city
				h["zipcode"] = row['dimensions'][4]
				content_index.each do |i|	
					if(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginAttempt")
						h["LoginAttempt"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][1]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginSuccess")
						h["LoginSuccess"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][2]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginFailure")
						h["LoginFailure"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][3]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "playStart")
						h["playStart"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][4]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "playComplete")
						h["playComplete"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][5]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "PlaybackTime")
						h["PlaybackTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][6]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LivePlaybackTime")
						h["LivePlaybackTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][7]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "PlaybackBufferingTime")
						h["PlaybackBufferingTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][8]
					end	
				end
				aggregate_ga_data.push(h)
				ga_data["data"]["rows"].delete_if { |x| x['dimensions'][2] == row['dimensions'][2] }
			end
		end
		return aggregate_ga_data
	end

	def get_ga_language_data(startdate, enddate)
		parameters = {"reportRequests" => [{"viewId" => "141655433","dateRanges" => [{"startDate" => startdate,"endDate" => enddate}], "metrics" => [{"expression" => "ga:totalEvents"},{"expression" => "ga:metric15"},{"expression" => "ga:metric16"},{"expression" => "ga:metric17"},{"expression" => "ga:metric3"},{"expression" => "ga:metric4"},{"expression" => "ga:metric5"},{"expression" => "ga:metric6"},{"expression" => "ga:metric7"}],"dimensions" => [{"name" =>"ga:eventCategory"},{"name" => "ga:eventAction"},{"name" => "ga:dimension12"}]}]}
		response = get_analytics(parameters, $credentials_file)
		if response.code == 200
			ga_data = JSON.parse(response.body)
			return ga_data['reports'][0]
		end
		return nil
	end 

	def process_ga_language_data(ga_data)
		aggregate_ga_data = []
		if !ga_data["data"]["rows"].nil?
			ga_data["data"]["rows"].each do |row|
				# aggegate content
				content_index = ga_data["data"]["rows"].each_index.select{|i| ga_data["data"]["rows"][i]['dimensions'][2] == row['dimensions'][2]}
				h = {}
				h["language"] = row['dimensions'][2]
				content_index.each do |i|	
					if(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginAttempt")
						h["LoginAttempt"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][1]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginSuccess")
						h["LoginSuccess"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][2]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginFailure")
						h["LoginFailure"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][3]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "playStart")
						h["playStart"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][4]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "playComplete")
						h["playComplete"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][5]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "PlaybackTime")
						h["PlaybackTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][6]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LivePlaybackTime")
						h["LivePlaybackTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][7]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "PlaybackBufferingTime")
						h["PlaybackBufferingTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][8]
					end	
				end
				aggregate_ga_data.push(h)
				ga_data["data"]["rows"].delete_if { |x| x['dimensions'][2] == row['dimensions'][2] }
			end
		end
		return aggregate_ga_data
	end
	def get_ga_category_data(startdate, enddate)
		parameters = {"reportRequests" => [{"viewId" => "141655433","dateRanges" => [{"startDate" => startdate,"endDate" => enddate}], "metrics" => [{"expression" => "ga:totalEvents"},{"expression" => "ga:metric15"},{"expression" => "ga:metric16"},{"expression" => "ga:metric17"},{"expression" => "ga:metric3"},{"expression" => "ga:metric4"},{"expression" => "ga:metric5"},{"expression" => "ga:metric6"},{"expression" => "ga:metric7"}],"dimensions" => [{"name" =>"ga:eventCategory"},{"name" => "ga:eventAction"},{"name" => "ga:dimension6"}]}]}
		response = get_analytics(parameters, $credentials_file)
		if response.code == 200
			ga_data = JSON.parse(response.body)
			return ga_data['reports'][0]
		end
		return nil
	end 

	def process_ga_category_data(ga_data)
		aggregate_ga_data = []
		if !ga_data["data"]["rows"].nil?
			ga_data["data"]["rows"].each do |row|
				# aggegate content
				content_index = ga_data["data"]["rows"].each_index.select{|i| ga_data["data"]["rows"][i]['dimensions'][2] == row['dimensions'][2]}
				h = {}
				h["category"] = row['dimensions'][2]
				content_index.each do |i|	
					if(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginAttempt")
						h["LoginAttempt"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][1]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginSuccess")
						h["LoginSuccess"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][2]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginFailure")
						h["LoginFailure"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][3]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "playStart")
						h["playStart"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][4]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "playComplete")
						h["playComplete"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][5]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "PlaybackTime")
						h["PlaybackTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][6]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LivePlaybackTime")
						h["LivePlaybackTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][7]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "PlaybackBufferingTime")
						h["PlaybackBufferingTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][8]
					end	
				end
				aggregate_ga_data.push(h)
				ga_data["data"]["rows"].delete_if { |x| x['dimensions'][2] == row['dimensions'][2] }
			end
		end
		return aggregate_ga_data
	end

	def get_ga_device_data(startdate, enddate)
		parameters = {"reportRequests" => [{"viewId" => "141655433","dateRanges" => [{"startDate" => startdate,"endDate" => enddate}], "metrics" => [{"expression" => "ga:totalEvents"},{"expression" => "ga:metric15"},{"expression" => "ga:metric16"},{"expression" => "ga:metric17"},{"expression" => "ga:metric3"},{"expression" => "ga:metric4"},{"expression" => "ga:metric5"},{"expression" => "ga:metric6"},{"expression" => "ga:metric7"}],"dimensions" => [{"name" =>"ga:eventCategory"},{"name" => "ga:eventAction"},{"name" => "ga:operatingSystem"},{"name" => "ga:deviceCategory"}]}]}
		response = get_analytics(parameters, $credentials_file)
		if response.code == 200
			ga_data = JSON.parse(response.body)
			return ga_data['reports'][0]
		end
		return nil
	end 

	def process_ga_device_data(ga_data)
		aggregate_ga_data = []
		if !ga_data["data"]["rows"].nil?
			ga_data["data"]["rows"].each do |row|
				# aggegate content
				content_index = ga_data["data"]["rows"].each_index.select{|i| ga_data["data"]["rows"][i]['dimensions'][2] == row['dimensions'][2] && ga_data["data"]["rows"][i]['dimensions'][3] == row['dimensions'][3]}
				h = {}
				h["os"] = row['dimensions'][2]
				h["device"] = row['dimensions'][3]
				content_index.each do |i|	
					if(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginAttempt")
						h["LoginAttempt"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][1]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginSuccess")
						h["LoginSuccess"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][2]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LoginFailure")
						h["LoginFailure"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][3]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "playStart")
						h["playStart"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][4]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "playComplete")
						h["playComplete"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][5]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "PlaybackTime")
						h["PlaybackTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][6]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "LivePlaybackTime")
						h["LivePlaybackTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][7]
					elsif(ga_data["data"]["rows"][i]['dimensions'][1] == "PlaybackBufferingTime")
						h["PlaybackBufferingTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][8]
					end	
				end
				aggregate_ga_data.push(h)
				ga_data["data"]["rows"].delete_if { |x| x['dimensions'][2] == row['dimensions'][2] && x['dimensions'][3] == row['dimensions'][3]  }
			end
		end
		return aggregate_ga_data
	end
end
