class AccessController < ApplicationController
	
	include AccessControllerHelper
    include ApplicationHelper


    def new
    	@bg = current_bg
    	@tenant_id =  params[:tenant_id]
    	#@catalogs = Catalog.find_by(:bg_id => current_bg.id, :theme => "access_control")
    	@tenants = Tenant.where(:bg_id => @bg.id).order_by(:created_at => 'desc')
    	#@tenants = Tenant.find_by(:id => @tenant_id)
	end


	def create
		status = true
		error = ""
		@bg = current_bg

		if !params.has_key? "tenants"
			status = false
			error = "Please select Tenants!"
        end

        if params[:start_date] == ""
			status = false
			error = "Please Select Start Date!"
        end

        if params[:end_date] == ""
			status = false
			error = "Please Select End Date!"
        end

        tenant_id = params[:tenants]

    	catalogs = Catalog.find_by(:bg_id => current_bg.id, :theme => "access_control", :tenant_id => tenant_id )
    	@Catalog_id = catalogs.ott_id
    	if status && (Time.parse(params[:start_date]) > Time.parse(params[:end_date]))
			status = false
			error = "Start Date cannot be greater than End Date"
		end

		if status
			metadata = create_access_controller(@bg,@Catalog_id,params)
		    logger.debug "AccessController metadata is " + metadata.to_s
			
			status = metadata["status"]
			error = metadata["error"]
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end

	def view_all
		@bg = current_bg
		@all_tenants = Tenant.where(:bg_id => @bg.id).order_by(:created_at => 'desc')
		cusers_list = current_user.roles
		@cusers_check = cusers_list.include? 'media_viewer'
		@cusers_length = cusers_list.length
		if @all_tenants.count == 1
			@all_tenants.each do |tent|
				@tid = tent.id
			end
			status = true
			error = ""
			tenants = Tenant.find_by(:_id => @tid)
			catalogs = Catalog.find_by(:bg_id => current_bg.id.to_s, :theme => "access_control", :tenant_id => tenants.id.to_s)
			url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?region=IN&auth_token="+tenants.ott_auth_token+"&status=edit"
			response = http_request("get",{},url,60)
			accesscontrolitems = response["body"]["data"]
			status = response["status"]
			error  = response["error"]
			if status
				if params[:page].nil?
					page = 1
					next_page = page + 1
					params[:page] = page
				else
					page = params[:page]
					next_page = page.to_i + 1
				end
			page_size = params[:page_size] ? params[:page_size] : 10
			params[:page_size] = page_size
	         if params[:title].present?
	         	@data_list =  accesscontrolitems["items"].select {|k| k if k["title"].to_s.match(/#{params[:title]}/i)}
	  		else
	  			@data_list = Kaminari.paginate_array(accesscontrolitems["items"]).page(page).per(page_size)
	    	 end
	    	 @next_items = Kaminari.paginate_array(accesscontrolitems["items"]).page(next_page).per(page_size)
	        end	
		else
			@all_tenants
		end
	end

	def get_all
		status = true
		error = ""
        tenantid = params[:tenant_id]
		@bg = current_bg
		tenants = Tenant.find_by(:_id => tenantid)
		catalogs = Catalog.find_by(:bg_id => current_bg.id, :theme => "access_control", :tenant_id => tenantid)
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?region=IN&auth_token="+tenants.ott_auth_token+"&status=edit"
		response = http_request("get",{},url,60)
		accesscontrolitems = response["body"]["data"]
		status = response["status"]
		error  = response["error"]

		if status
			if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
		page_size = params[:page_size] ? params[:page_size] : 10
		params[:page_size] = page_size
		if params[:title].present?
      		data_list =  accesscontrolitems["items"].select {|k| k if k["title"].to_s.match(/#{params[:title]}/i)}
      		items_cnt = data_list.count
    		render json: {:items => data_list, :next_cnt => {},:psize => params[:page_size],:pge => page,:nxt => 0,:icount => items_cnt}
    	 else
		data_list = Kaminari.paginate_array(accesscontrolitems["items"]).page(page).per(page_size)
        next_items = Kaminari.paginate_array(accesscontrolitems["items"]).page(next_page).per(page_size)
        # render json: {:items => accesscontrolitems, :total_count => data_list.total_count, :total_pages => data_list.total_pages}
       	nxt_ims = next_items.count 
        items_cnt = data_list.count
        render json: {:items => data_list, :next_cnt => next_items,:psize => params[:page_size],:pge => page,:nxt => nxt_ims,:icount => items_cnt}
        end
        else
        render :json => {:error => error}.to_json, :status => 500
        end
	end


	def details
		@content_id =  params[:content_id]
		status = true
		error = ""
		@tenantid = params[:tenant_id]
		@bg = current_bg
		tenants = Tenant.find_by(:_id => @tenantid)
		catalogs = Catalog.find_by(:bg_id => current_bg.id, :theme => "access_control", :tenant_id => @tenantid)

		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+@content_id+"?region=IN&auth_token="+tenants.ott_auth_token+"&status=any"
		response = http_request("get",{},url,60)
		@accesscontrolitems = response["body"]["data"]
		status = response["status"]
		error  = response["error"]

		if params[:method] == "edit"
		if status
        render "access/edit"
        else
        render :json => {:error => error}.to_json, :status => 500
        end

        else
		if status
        render partial: "view_info"
        else
        render :json => {:error => error}.to_json, :status => 500
        end
        end
	end

	def acdelete

		@content_id =  params[:content_id]
		status = true
		error = ""

		tenantid = params[:tenant_id]
		@bg = current_bg
		tenants = Tenant.find_by(:_id => tenantid)
		catalogs = Catalog.find_by(:bg_id => current_bg.id, :theme => "access_control", :tenant_id => tenantid)

		if params[:method] == "delete"
			body = {}
		    body["auth_token"] = @bg.ott_auth_token
		    url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+@content_id+"?auth_token="+@bg.ott_auth_token+"&status=edit&region=IN"
		    response = http_request("delete",{},url,60)

		    if response["code"] == 200
		    	#catalog local delete
		    	#catalogs.destroy
		    	render json: {} , status: :ok
		    else
		    	status = response["status"]
		    	error = response["error"]
		    	render :json => {:error => error}.to_json, :status => 500
		    end
			end
	end
end
