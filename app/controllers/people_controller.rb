class PeopleController < ApplicationController
	include PeopleHelper
	include ApplicationHelper
	include HttpRequest

	before_filter :set_object

	def set_object
		@app_obj = App.new
	end	

	def new
		@bg = current_bg
		@catalog_id = params[:catalog_id]
		@catalogs = []
		#Catalog.where(:bg_id => @bg.id).and(:theme => "person").each do |cata|
		Catalog.where(:id => @catalog_id).and(:theme => "person").each do |cata|
			h = {}
			h["cn"] = cata.name
		 	h["name"] = cata.name+"["+cata.theme+"]"
		 	h["id"] = cata.id
		 	h["tenant_id"] = cata.tenant_id
		 	h["bg_id"] = cata.bg_id
		 	h["tenants"] = []
		 	cata.tenant_id.each do |t|
		 		t_info = {}
		 		data = Tenant.find(t)
		 		t_info["tenant_name"] = data.name
		 		t_info["tenant_id"] = data.id
		 		t_info["apps"] = []
		 		App.where(:tenant_id => data.id).each do |app|
		 			catalog_apps = cata.app_id
		 			if catalog_apps.include? ("#{app.id}")
		 				a_info = {}
		 				a_info["app_name"] = app.name
		 				a_info["app_id"] = app.id
		 				t_info["apps"] << a_info
		 			end	
		 		end	
		 		h["tenants"] << t_info
		 	end 
			@catalogs << h
		end
		catalog = Catalog.find(@catalog_id)
		url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"?auth_token="+@bg.ott_auth_token
			
			response = http_request("get",{},url,60)
			@catalog = response["body"]["data"]

		# Upload image to s3 server
	    s3uploadremovspace = @bg.name+"/"+@catalogs[0]["cn"]+"/person/images"
	    s3upload = s3uploadremovspace.gsub(" ", "_");
	    @signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)
	end
	
	def create
		params.merge!({"bg_obj" => current_bg})
		params[:title] = params[:title].strip
		status,msg = create_at_ott(params)
		if status
			render json: {}, status: :ok 
		else
			render :json => {:error => msg}.to_json, :status => 500
		end	
	end	

	def view_all
		# url = "http://" + get_ott_base_url + "/people?auth_token=" + current_bg.ott_auth_token
		# response = http_request("get",{},url,60)
		# if response["code"] == 200
		# 	@people = response["body"]["data"]["items"]
		# 	@total_people_count = response["body"]["data"]["total_people_count"]
		# end
	end
		
	def get_all
		url = "http://" + get_ott_base_url + "/people?auth_token=" + current_bg.ott_auth_token
		response = http_request("get",{},url,60)
		if response["code"] == 200
			items = response["body"]["data"]["items"]
			catalog_hashes = items.map {|h| h["catalog_id"]}.uniq.inject({}) { |hash,id| hash.merge( id => Catalog.find_by(:ott_id => id).name) }
			items.each do |item|
				item["catalog_name"] = catalog_hashes[item["catalog_id"]]
			end
			items_list = Kaminari.paginate_array(items).page(params[:page])
			render json: {:list => items_list, :total_count => items_list.total_count, :total_pages => items_list.total_pages} , status: :ok	
			#render json: { :list => items }, status: :ok
		else
			render :json => {:error => "OTT Error"}.to_json, :status => 500
		end
	end	

	def get_people
	    url = "http://" + get_ott_base_url + "/people?auth_token=" + current_bg.ott_auth_token
		response = http_request("get",{},url,60)
		if response["code"] == 200
			@people = response["body"]["data"]["items"]
			@total_people_count = response["body"]["data"]["total_people_count"]
			render partial: "people/view_people"
		else
		    render json: {} , status: :ok	
		end
	end	

	def edit
		@bg = current_bg
		person_id = params[:person_id]
		catalog_id = params[:catalog_id]
		catalog = Catalog.find_by(:ott_id => catalog_id)
		@catalog_name = catalog.name
		@catalog_data = []
		catalog.tenant_id.each do |tenant|
			t = {}
			data = Tenant.find(tenant)
			t["tenant_name"] = data.name
			t["tenant_id"] = data.id
			t["apps"] = []
			@app_obj.get_apps_by_id(catalog.app_id).each do |app|
				
				if app.tenant_id == tenant
					h = {}
					h["app_name"] = app.name
					h["app_id"] = app.id
					t["apps"] << h
				end	
			end	
			@catalog_data << t
		end	
		url = "http://"+get_ott_base_url+"/catalogs/"+catalog_id+"?auth_token="+current_bg.ott_auth_token
      response = http_request("get",{},url,60)
      if response["code"] == 200
      	@catalog = response["body"]["data"]
	  end
		url = "http://" + get_ott_base_url + "/catalogs/"+ catalog_id +"/items/"+ person_id + "?auth_token=" + current_bg.ott_auth_token + "&region=IN"
	    resp = http_request("get",{},url,60)
	    if resp["code"] == 200
	    	@person_info = resp["body"]["data"]
	    	@person_info["cms_apps"] = @app_obj.get_apps_by_ott_id(@person_info["app_ids"]).as_json.map { |h| h["_id"] }
	    end	
	   s3uploadremovspace = @bg.name+"/"+@catalog_name+"/person/images"
	    s3upload = s3uploadremovspace.gsub(" ", "_");
	    @signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)
	end

	def update
		method = params[:method]
		person_id = params[:id]
		catalog_id = params[:catalog_id]
		case method
		when "update"
			params.merge!({"bg_obj" => current_bg})
			status,msg = update_people_at_ott(params)
		when "delete"
			params.merge!({"bg_obj" => current_bg})

			#Delete at ott
			status,msg = delete_at_ott(params)
		end
		if status
			render json: {}, status: :ok 
		else
			render :json => {:error => msg}.to_json, :status => 500
		end
	end

	def search_people
		@bg = current_bg
		if params[:search].nil?
			search = ""
		else
			search = params[:search]
		end
		url = "http://" + get_ott_base_url + "/people?search="+search+"&auth_token=" + @bg.ott_auth_token + "&region=IN"
		response = http_request("get",{},url,60)
		if response["code"] == 200
			people = response["body"]["data"]["items"]
			render :json => {:data => people}.to_json, :status => 200
		end
	end	
end
