class SessionsController < ApplicationController
	def login_form
		render partial: "login_modal"
	end

	def signup_form
		render partial: "signup_modal"
	end

	def forgot_form
		render partial: "forgot_modal"
	end

	def create
		user = User.authenticate(params[:email], params[:password])
		logger.debug "==>124 user is #{user.inspect}=="
		if user == "Incorrect Password"
			#render :json => {:error => user}.to_json, :status => 500
			flash[:danger] = "Email/Password Incorrect."
			redirect_to "/"
		elsif user == "Invalid Email"
			#render :json => {:error => "#{params[:email]} is not registered with us. Please register with Instaott"}.to_json, :status => 500
			#render :json => {:error => "You are not registered with us. Sign Up then try to Sign In"}.to_json, :status => 500
			flash[:danger] = "You are not registered with us. Sign Up then try to Sign In"
			redirect_to "/"
		elsif user
			email_verify = user.isemail_verify
	  		if email_verify == "NO" || email_verify.nil?
	  			#render :json => {:error => "Email Verification Pending. Please check your Email and Confirm"}.to_json, :status => 500
				flash[:danger] = "Email Verification Pending. Please check your Email and Confirm"
				redirect_to "/"
	  		else
			    	session[:user_id] = user.id
	    			#render json: {} , status: :ok
				redirect_to "/"
		    	end
		else
	    		#render :json => {:error => "Invalid Email or Password"}.to_json, :status => 500
			flash[:danger] = "Invalid Email or Password"
			redirect_to "/"
	  	end
	  	# if user
	  	# 	email_verify = user.isemail_verify
	  	# 	if email_verify == "NO"
	  	# 		render :json => {:error => "Your Email is not Verify"}.to_json, :status => 500
	  	# 	else
	   #  	session[:user_id] = user.id
	   #  	render json: {} , status: :ok
	   #  	end
	  	# else
	   #  	render :json => {:error => "Invalid Email or Password"}.to_json, :status => 500
	  	# end
	end
	def destroy
  		session[:user_id] = nil
  		session[:bg_id] = nil
  		render "home/index", :layout => false
  	end
	def set_bg
		session[:bg_id] = params[:bg_id]
		render json: {} , status: :ok
	end
	def unset_bg
		session[:bg_id] = nil
		render json: {} , status: :ok
	end
end
