class TvchannelsController < ApplicationController

	include ApplicationHelper
	include TvchannelsHelper

	 def new
	 	@bg = current_bg
	 	@catalog_id = params[:catalog_id]
	 	@tenant_id  = params[:tenant_id]
	 	@catalog = Catalog.find(@catalog_id);
	 	url = "http://"+get_ott_base_url+"/catalogs/"+@catalog.ott_id+"?auth_token="+@bg.ott_auth_token
		response = http_request("get",{},url,60)
		@livechannel = response["body"]["data"]
	 end


	 def create
	 	@bg = current_bg
	 	catalogid = params[:catalog_id]

	 	catalog = Catalog.find_by(:id => catalogid, :theme => "livetv");
	 	catalogottid = catalog.ott_id
		status = true
		error = ""
		if status
			response = livechannels(@bg,params,catalogottid)
			
			status = response["status"]
			error = response["error"]
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	 end
end