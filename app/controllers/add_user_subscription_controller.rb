class AddUserSubscriptionController < ApplicationController

	include AddUserSubscriptionHelper
	include ApplicationHelper

	def new
		@bg = current_bg
		if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
		page_size = params[:page_size] ? params[:page_size] : 10
		params[:page_size] = page_size
		@all_tenants = Tenant.where(:bg_id => @bg.id).order_by(:created_at => 'desc')
		if @all_tenants.count == 1
			@all_tenants.each do |tent|
				@tid = tent.id
			end
			if params[:coupon] == "coupon"
				status = true
				error = ""
				tenants = Tenant.find_by(:_id => @tid)
				catalogs = Catalog.find_by(:bg_id => current_bg.id, :theme => "coupon", :tenant_id => tenants._id.to_s)
				url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?region=IN&auth_token="+tenants.ott_auth_token
				response = http_request("get",{},url,60)
				couponitems = response["body"]["data"]
				status = response["status"]
				error  = response["error"]
				if status
					@couponitems = Kaminari.paginate_array(couponitems["items"]).page(page).per(page_size)
	    			if params[:title].present?
	       				@couponitems =  couponitems["items"].select {|k| k if k["title"].to_s.match(/#{params[:title]}/i)}
	    			end
	    			@next_items = Kaminari.paginate_array(couponitems["items"]).page(next_page).per(page_size)
	    		end
	    	else
	    		cusers_list = current_user.roles
				@cusers_check = cusers_list.include? 'media_viewer'
				@cusers_length = cusers_list.length
	    		status = true
				error = ""
				catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "subscription", :tenant_id => @tid.to_s)
				url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?region=any&auth_token="+@bg.ott_auth_token
				response = http_request("get",{},url,60)
				subscription = response["body"]["data"]
				status = response["status"]
				error  = response["error"]
				if status
				@subscription = Kaminari.paginate_array(subscription["items"].reverse).page(page).per(page_size)
		        	if params[:title].present?
	       				@subscription =  subscription["items"].reverse.select {|k| k if k["title"].to_s.match(/#{params[:title]}/i)}
	    			end
	    		@next_items = Kaminari.paginate_array(subscription["items"].reverse).page(next_page).per(page_size)
		        end
	    	end
		else
			@all_tenants
		end
	end

	def create
		status = true
		error = ""
		if status
			metadata = usersubscriptions(params)
		    logger.debug "User Subscriptions is " + metadata.to_s
			
			status = metadata["status"]
			error = metadata["error"]
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
end