class StaticController < ApplicationController

	include ApplicationHelper

	before_filter :login_check, except: [:contact, :aboutus]

	def contact
		render "static_pages/contact_us"
	end

	def aboutus
		render "static_pages/about_us"
	end


end
