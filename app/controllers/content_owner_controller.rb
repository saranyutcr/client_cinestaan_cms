class ContentOwnerController < ApplicationController
	include ApplicationHelper
	include GaCommonCode
	@@region = "IN"
	def content_owner_dashboard
		if params[:content_owner_name].present?
	 		@@current_user_content_owner_id = content_owner_data content_owner_details,params[:content_owner_name]
	 	else
	 		@@current_user_content_owner_id = current_user.content_owner_id
	 	end
		content_id = @@current_user_content_owner_id.nil? ? "" : @@current_user_content_owner_id.split("-")[0]
		catalog_id = @@current_user_content_owner_id.nil? ? "" : @@current_user_content_owner_id.split("-")[1] 
	    @advance_paid = content_owner_mg_paid(content_id,catalog_id)	
		@all_media_data = []
		tdate = DateTime.now - 30
		start_date = tdate.strftime("%Y-%m-%d")
		datetime_now = DateTime.now
		end_date = datetime_now.strftime("%Y-%m-%d")
		# Get all catalog items of a given content owner
		@all_catalog_items = get_all_catalog_items @@current_user_content_owner_id.split("-")[0] unless @@current_user_content_owner_id.nil?
		# Get data from GA in the given date range (including start and end date)
		ga_data = get_ga_data(start_date, end_date)
		aggregate_ga_data = process_ga_data(ga_data)
		# Merge catalog with ga data.
		@all_media_data = merge_catalog_with_ga_data(@all_catalog_items, aggregate_ga_data)

	end

	def content_owner_asset_info
		# @show_data = Media.where(:content_owner_id => current_user.content_owner_id).where(:show_id=>params[:show_id]).to_a
	end

	def get_co_ga_details
		@all_media_data = []
		from_date = DateTime.parse(params[:from_date])
		start_date = from_date.strftime("%Y-%m-%d")
		to_date = DateTime.parse(params[:to_date])
		end_date = to_date.strftime("%Y-%m-%d")
		catalog_items_params = params[:cont_owner_data]
		catalog_items = []
		catalog_items_arr = []
		catalog_items_params.each do |k,v| 
			catalog_items_arr << v
		end
		catalog_items << catalog_items_arr
		# Get data from GA in the given date range (including start and end date)
		ga_data = get_ga_data(start_date, end_date)
		aggregate_ga_data = process_ga_data(ga_data)
		# Merge catalog with ga data.
		@all_media_data = merge_catalog_with_ga_data(catalog_items, aggregate_ga_data)
		render partial: 'content_owner_table'
	end

	private

	def content_owner_mg_paid(content_id,catalog_id)
		advance_paid = 0.0
		currency = "INR"
		url = "http://"+get_ott_base_url+"/catalogs/"+catalog_id+"/items/"+content_id+"?auth_token="+current_bg.ott_auth_token+"&status=any&region="+@@region
    	response = http_request("get",{},url,60)
    	if response["code"] == 200
    		unless response["body"]["data"]["mg_paid"].blank?
    		advance_paid = response["body"]["data"]["mg_paid"]
    		end
    		unless response["body"]["data"]["custom_fields"].blank?
    		currency = response["body"]["data"]["custom_fields"]["currency"]
	    	end
    	end
    	return currency,advance_paid
	end  

	def content_owner_data co_array,co_name
		co_details = ""
		co_array.each do |i|
	    	if i.include?(co_name)
		     	co_details = i
		     	break
	     	end
		end
		co_val = co_details.split("-")[1] + "-" +  co_details.split("-")[2]
		return co_val
	end
end
