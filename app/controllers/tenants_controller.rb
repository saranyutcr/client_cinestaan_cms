class TenantsController < ApplicationController
	include TenantsHelper
	include ApplicationHelper
	include AppsHelper
	include AccessControllerHelper
	include SubscriptionHelper
	include MessagesHelper
	include CouponCodeHelper
	
	def create
		status = true
		error = ""
		#validate parameters received
		#check for uniqueness of tenant name inside business group
		#remote post tenant
		bg = current_bg

		tenant = create_tenant(bg,params[:name].strip)
		if !tenant
			status = false
			error = "Tenant Already Exist"
		end
		if status
		catalog = Catalog.find_by(:bg_id => bg.id, :theme => "access_control", :tenant_id => tenant.id)
		if catalog.nil?
			platforms = ["web","android","ios"]
			@apps = create_apps(tenant,platforms,platforms)
			accesscontrol = create_access_control(bg,tenant)

			logger.debug "=======>accesscontrol<=======#{accesscontrol}"

			if !accesscontrol
				status = false
				error = "accesscontrol could not be created at OTT server"
			end

			accesscontrolitems = default_create_access_control_items(bg,accesscontrol.ott_id,tenant)
			if !accesscontrolitems
				status = false
				error = "accesscontrolitems could not be created at OTT server"
			end
		end
		subcatalog = Catalog.find_by(:bg_id => bg.id, :theme => "subscription", :tenant_id => tenant.id)
		if subcatalog.nil?
			subscription = create_subscription(bg,tenant)
			if !subscription
				status = false
				error = "subscription could not be created at OTT server"
			end

			@apps[2].each do |apid|
				appd = App.find_by(:ott_id => apid)
				appid = appd.id
				tenantid = appd.tenant_id
		  		planitems = subscription_items(bg,tenantid,appid)
				if !planitems
					status = false
					error = "Plans could not be created at OTT server"
				end
				end

		    subscription_free_pack =  subscription_free_pack(bg,tenant)
		    if !subscription_free_pack
		    	status = false
		    	error = "App_Plans could not be created at OTT"
		    end
		end

	    if status
			#Create messages
			message = create_catalog_message_ott(bg,tenant)
			if !message
				status = false
				error = "message could not be created at OTT server"
			end
		end

		if status 

			message_types = ConfigData.find_by(:access_key => "messages001")
			message_types.data["messages"].each do |type|
            message_items = message_items(bg,tenant,type)
			if !message_items
				status = false
				error = "Message could not be created at OTT server"
			end
			end
		end

		if status
			coupon_code = create_catalog_coupon(bg,tenant)
			if !coupon_code
				status = false
				error = "coupon code could not be created at OTT server"
			end
		end

		
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end

	def fetch
	end

	def fetch_all
		@bg = current_bg
		tenant_list = Tenant.where(:bg_id => @bg.id).order_by(:created_at => 'desc').page(params[:page])
		render json: {:list => tenant_list, :total_count => tenant_list.total_count, :total_pages => tenant_list.total_pages} , status: :ok
	end
	def update
		status = true
		error = ""
		case params[:method]
		when "delete"
			tenant = Tenant.find(params[:tenant_id])
			#delete self from OTT server
		    bg = BusinessGroup.find(current_bg.id)
			body = {}
			body["auth_token"] = bg.ott_auth_token
			url = "http://"+get_ott_base_url+"/tenant/"+tenant.ott_id
			response = http_request("delete",body,url,60)
			if !response["status"]
		      status = response["status"]
		      error = response["error"]
		    end
		    if status
				tenant.destroy
			end
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
	def view_all
		@bg = current_bg
		if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
		page_size = params[:page_size] ? params[:page_size] : 10
		params[:page_size] = page_size
		@tenant_list = Tenant.where(:bg_id => @bg.id).order_by(:created_at => 'desc').page(page).per(page_size)
    	if params[:title].present?
      	@tenant_list= Tenant.where(:bg_id => @bg.id).where(:name => /#{params[:title]}/i)
    	end
    	@next_items = Tenant.where(:bg_id => @bg.id).order_by(:created_at => 'desc').page(next_page).per(page_size)
	end

	def view_tenant
		# @portal_base_address = "http://128.199.119.23:4001"
		@bg = current_bg
		@tenant = Tenant.find(params[:tenant_id])
		apps = App.where(:tenant_id => @tenant.id)
		@tenant_apps = apps.as_json.collect{ |h| h["name"] }

		env = Rails.env
		case env
		when "development"
			@webview = @bg.staging_webview
		when "preproduction"
			@webview = @bg.preprod_webview
		when "production"
			@webview = @bg.prod_webview
		end

		catalog = Catalog.find_by(:bg_id => @bg.id, :theme => "access_control", :tenant_id => @tenant.id.to_s)
		if catalog.nil?
			accesscontrol = create_access_control(@bg,@tenant)

			logger.debug "=======>accesscontrol<=======#{accesscontrol}"

			if !accesscontrol
				status = false
				error = "accesscontrol could not be created at OTT server"
			end

			accesscontrolitems = default_create_access_control_items(@bg,accesscontrol.ott_id,@tenant)
			if !accesscontrolitems
				status = false
				error = "accesscontrolitems could not be created at OTT server"
			end
		end
		subcatalog = Catalog.find_by(:bg_id => @bg.id, :theme => "subscription", :tenant_id => @tenant.id.to_s)
		if subcatalog.nil?
			subscription = create_subscription(@bg,@tenant)
			if !subscription
				status = false
				error = "subscription could not be created at OTT server"
			end

			apps.each do |apid|
				appd = App.find_by(:ott_id => apid.ott_id)
				appid = appd.id
				tenantid = appd.tenant_id
		  		planitems = subscription_items(@bg,tenantid,appid)
				if !planitems
					status = false
					error = "Plans could not be created at OTT server"
				end
				end
		end

		couponcatalog = Catalog.find_by(:bg_id => @bg.id, :theme => "coupon", :tenant_id => @tenant.id.to_s)
		if couponcatalog.nil?
			coupon_code = create_catalog_coupon(@bg,@tenant)
			if !coupon_code
				status = false
				error = "coupon code could not be created at OTT server"
			end
		end

	end
end
