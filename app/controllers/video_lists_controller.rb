class VideoListsController < ApplicationController
	include ApplicationHelper
	include MediaHelper
	include UsersHelper
	include HomeHelper
	include PaginateMe
	include MediaLib
	include CatalogsHelper
	include TrackerLib


	before_filter :login_check, :set_objects
	before_filter :get_bg_plan, only: [:create,:bulk_upload]

	def set_objects
	    @catalog_object = Catalog.new
	end
		
	def get_bg_plan
    	@bg_plan = BusinessGroupPlan.find_by(:status => "active",:business_group_id => current_bg.id)
    end	

	def new_video_list
		status = true
		@bg = current_bg
        @catalog_id = params[:catalog_id]
        @parent_media_id = params[:media_id]
        @show_id = params[:show_id]
        @subcategory_id = params[:subcategory_id]
		@ps_list_single = []
		PublishSettings.where(:bg_id => @bg.id).order_by(:created_at => 'desc').each do |ps|
			disqualify = false
			if ps.naming_tag
				if has_custom_keys(ps.naming_tag)
					disqualify = true
				end
			end
			# if extra_keys_required_for_storage(ps.cdn.first).count > 0
			# 	disqualify = true
			# end
			# if !disqualify
			# 	@ps_list_single << ps
			# end
		end

		if @bg.plan != "transcoding"
			if params[:show_id]
			@show_data = ShowTheme.find_by(:id => params[:show_id]) || ShowTheme.find_by(:ott_id => params[:show_id])
			@catalog_id = @show_data.catalog_id
			end

			if params[:subcategory_id]
			@subcategory_data = Subcategory.find_by(:id => params[:subcategory_id])
			@catalog_id = @subcategory_data.catalog_id
			end

			@catalog = Catalog.find_by(:id => @catalog_id)
			@catalogtheme = @catalog.theme
			

			url = "http://"+get_ott_base_url+"/catalogs/"+@catalog.ott_id+"?auth_token="+@bg.ott_auth_token
			response = http_request("get",{},url,60)
			if !response["status"]
				status = response["status"]
				error = response["error"]
			end


			if status
			    @catalog = response["body"]["data"]	

			    @t_ott_id = response["body"]["data"]["tenant_ids"]
			    @videolist_tags = response["body"]["data"]["videolist_tags"]

			    @tenant_list = []
			    @accesscontrol = []
			    @valvideolist_tags = []

			    @videolist_tags.each do |vlt|
			    	@valvideolist_tags << vlt
			    end

			    @t_ott_id.each do |obj|
			  	  Tenant.where(:ott_id=>obj).each do |tenan|
				  	
				  	h = {}
				 	h["id"] = tenan.id
				 	h["name"] = tenan.name
				 	h["ott_id"] = tenan.ott_id

					h ["genres"] =[]
					response["body"]["data"]["genres"].keys.each do |g|
			          h["genres"] = (response["body"]["data"]["genres"][tenan.ott_id]).map{|gen| gen["name"] } if g == h["ott_id"]
					end

					h ["languages"] =[]
					response["body"]["data"]["languages"].keys.each do |l|
					  h ["languages"] = (response["body"]["data"]["languages"][tenan.ott_id]).map{|lan| lan["name"] } if l == h["ott_id"]
					end 
			    	
				    h["applications"] = []
				    App.where(:tenant_id => tenan.id).each do |tapp|
				        if @catalog["application_ids"].include? tapp.ott_id	
				            application = {}
				            application["a_id"] = tapp.id
						    application["a_name"] = tapp.name
						    application["a_tid"] = tapp.tenant_id
						    h['applications'] << application
					    end
				    end

			 		@tenant_list << h

			 		catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "access_control", :tenant_id => tenan.id.to_s)
			 		acurl = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
					acresponse = http_request("get",{},acurl,60)
					if acresponse["code"] == 200
					accesscontrols = acresponse["body"]["data"]["items"]

					accesscontrols.each do |acc|
						ac = {}
						ac["name"] = tenan.name
						ac["title"] = acc["title"]
						ac["content_id"] = acc["content_id"]
						@accesscontrol << ac
					end
					end  
			      end
		        end
             end	

			url = "http://" + get_ott_base_url + "/people?auth_token=" + current_bg.ott_auth_token
			response = http_request("get",{},url,60)
			if response["code"] == 200
				@people = response["body"]["data"]["items"]
				@total_people_count = response["body"]["data"]["total_people_count"]
			end
		end	
	end


	def create
		@videoflag = params[:videoflag]
		@Eshow_id = params[:Eshow_id]
		@Esubcategory_id = params[:Esubcategory_id]
		status = true
		error = ""
		bg = current_bg
		
		if bg.plan != "transcoding"
		  params[:catalog_id] = params[:catalog_id].split("-")[0]	
		end	

		if status
			if !params[:title].strip
				status = false
				error = "Title cannot be empty!"
			end
		end
		if status
			if params[:title].strip == ""
				status = false
				error = "Title cannot be empty!"
			end
		end

		if status && (bg.plan=="transcoding")
			if status
				if !params[:source_url]
					status = false
					error = "Video URL cannot be empty!"
				end
			end
			if status
				if (!params[:source_url].start_with?('ftp://')) && (!params[:source_url].start_with?('http://'))
					status = false
					error = "Error in Video URL Format"
				end
			end
			if status
				if !params[:ps_id]
					status = false
					error = "Publish Settings ID cannot be empty!"
				end
			end
			if status
				if params[:ps_id].strip == ""
					status = false
					error = "Publish Settings ID cannot be empty!"
				end
			end

			if status
                if params[:source_url].empty?
                	status = false
                	error = "Please provide source url!"
                end	
			end	
		else

			if params[:utc_published_date] 
				if params[:utc_published_date] == "undefined"
					params[:utc_published_date] = ""
				else
					t = params[:utc_published_date].to_i / 1000
					params[:utc_published_date] =  Time.at(t).utc
				end	
			end	

			if params[:utc_unpublished_date] 
				if params[:utc_unpublished_date] == "undefined"
					params[:utc_unpublished_date] = ""
				else	
					t = params[:utc_unpublished_date].to_i / 1000
					params[:utc_unpublished_date] =  Time.at(t).utc
				end	
			end	

            if status
            	if @videoflag == "yes"
            	else
	            	if !params.has_key? "apps"
	            		status = false
	            		error = "Please Select Applications!"
	            	end	
                end
            end	

            if status
            	if @videoflag == "yes"
            	else
            	if params["language"].empty?
            		status = false
            		error = "Please Select Language!"
            	end	
            	end
            end	

            if status
            	if @videoflag == "yes"
            	else
            	if !params.has_key? "genres"
            		status = false
            		error = "Please Select Genres!"
            	end	
            	end
            end

            if status
				status,error = validate_ott_media_params(params)
			end

		end
		media_params = {}
		if status
			media_params["cut_intervals"] = params[:cut_intervals]
			media_params["banner"] = params[:banner]
			media_params["title"] = params[:title].strip
			source_type = params[:source_type]
			if source_type == "uploadvideo"
				source_type = "HTTP"
			else
				source_type
			end
			media_params["source_type"] = source_type
			media_params["source_url"] = fix_multiple_slashes(params[:source_url])
			#media_params["source_url"] = params[:source_url]

			media_params["notification_url"] = params[:notification_url]
			media_params["user_id"] = current_user.id
			media_params["user_role"] = current_user.roles.first
			if current_bg.plan == "ott_saas" 
				media_params["publish_settings_id"] = PublishSettings.find_by(:bg_id => current_bg.id).id
			else	
			    media_params["publish_settings_id"] = params[:ps_id]
		    end
			media_params["bg_id"] = current_bg.id
			if (!params[:priority]) || (params[:priority] == "")
				media_params["priority"] = "low"
			else
				media_params["priority"] = params[:priority]
			end
			media_params["status"] = "Created"
			media_params["ott_status"] = "edit"
			media_params["is_removed"] = false
			media_params["bulk_upload_id"] = nil
			media_params["custom_keys"] = nil
			media_params["smart_url"] = params[:smart_url]
			media_params["videoflag"] = params[:videoflag]
			media_params["show_id"] = params[:Eshow_id]
			media_params["subcategory_id"] = params[:Esubcategory_id]
			media_params["utc_published_date"] = params[:utc_published_date]
			media_params["utc_unpublished_date"] = params[:utc_unpublished_date]
			media_params["parent_media_id"] = params[:parent_media_id]
			if params.has_key? "imgoptradio"
				media_params["imgoptradio"] = params["imgoptradio"]
			else
			  media_params["imgoptradio"] = "no"	
			end	
			status = validate_media_params(media_params)
		end
		if status

			params[:bg_object] = bg
			params[:entity] = "media"
			params[:bg_plan] = @bg_plan
			status,message = track_entity_count(params)

			if status
			media = create_media(media_params)
			#if BusinessGroup.find(media.bg_id).plan=="transcoding" #auto-transcode if non-ott bg
			if bg.plan == "transcoding"	
				status,error = media.transcode_request
			else
				if params[:catalog_id]
					# if !media_params["smart_url"].empty?
					if params[:source_url].empty?
                        media.status = "Created Externally" 
					end	
					catalog = Catalog.find(params[:catalog_id])
					theme = catalog.theme
					data = params
					# status,params = check_images(catalog,data,bg)
					# if status 
						media.ott_catalog_id = catalog.ott_id
						media.save
							videoflag = params[:videoflag]
							if videoflag == "yes"
							metadata = extract_metadata_from_videolistparams(params)
							logger.debug "Extracted videolist metadata is " + metadata.to_s
							end
							if media.ott_id
								status,error = media.update_at_ott(metadata)
							else
								videoflag = params[:videoflag]
								if videoflag == "yes"
									status,error = media.create_videotags_at_ott(metadata,catalog)
								else
									# if theme == "show"
									# 	if @Eshow_id.nil? || @Eshow_id.empty?
									# 	status,error = media.create_at_ott(metadata,catalog,@Esubcategory_id)
									# 	else
									# 	status,error = media.create_at_ott(metadata,catalog,@Eshow_id)
									# 	end
									# 	else
									# status,error = media.create_at_ott(metadata,catalog,@Eshow_id)
									# end
							    end

								if !status
									media.destroy  #Check for integrity on delete
									message = error
								else
								#if status is true then check publish and unpublish date and run delayed	
								   	if !metadata["published_date"].blank? && metadata["status"] != "published"
								   		media.add_media_in_publish_queue(metadata)
								   	end
								   	if !metadata["unpublished_date"].blank?
								   		media.add_media_in_unpublish_queue(metadata)
								   	end
								   	media.save
								end	
							end
						# else
						# 	message = "Please provide images"
						#     media.destroy	
						# end	
					end
				end
			end	
		else
			message = error 
			error = "Validation Error in Input Media Parameters"
		end

		if status
			render json: {:media_id => media.id} , status: :ok
		else
			#render :json => {:error => error}.to_json, :status => 500
			render :json => {:error => message}.to_json, :status => 500
		end
	end

    def get_videotags
     status  = true
     error = ""
     catalog = Catalog.find(params[:catalog_id])
     @catalog_id = params[:catalog_id]
     @contentid = params[:contentid]
     @bg = current_bg
     url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/videolist_tags"+"?auth_token="+@bg.ott_auth_token+"&region=IN&status=any"
     response = http_request("get",{},url,60)
     @video_list = response["body"]["data"]["items"]
     if @video_list.nil?
     	status = false
     	error = "No VideoList Items for this content!"
     end
     if status
      #render json: {} , status: :ok
     else
      render :json => {:error => error}.to_json, :status => 500
      end	
	end

	def videotags_items
		@bg = current_bg
		@category = params[:category]
		catalog = Catalog.find(params[:catalog_id])
		catalogott_id = catalog.ott_id
		media = Media.find_by(:ott_catalog_id => catalogott_id)
		url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/videolists?auth_token="+@bg.ott_auth_token+"&region=IN&status=any&category="+@category
		response = http_request("get",{},url,60)
		@videolists = response["body"]["video_list"]
		render json: {:videolist => @videolists }
	end


	def videolist_items
		@bg = current_bg
	    @itemid	 = params[:videotag].split("-")[0]
		@category = params[:videotag].split("-")[1]
		contentid = params[:videotag].split("-")[2]
		videolist_id = params[:videolist].split("-")[0]
		catalog_id = params[:videolist].split("-")[1]
		metadata = {}
		catalog = Catalog.find(@itemid)
		catalogott_id = catalog.ott_id
		media = Media.find_by(:ott_catalog_id => catalogott_id)
		item_id = media.ott_id
		url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/items/"+contentid+"?auth_token="+@bg.ott_auth_token+"&region=IN&status=any"
		response = http_request("get",{},url,60)
		@item = response["body"]["data"]
			metadata["title"] = @item["title"]
			metadata["featured_image"] = @item["thumbnails"]["l_large"]["url"]
			metadata["l_listing_image"] = @item["thumbnails"]["l_medium"]["url"]
			metadata["p_listing_image"] = @item["thumbnails"]["p_small"]["url"]
			metadata["category"]= @category
			metadata["regions"] = ["IN"]
			metadata["business_group_id"] = BusinessGroup.find(catalog.bg_id).ott_id
			metadata["description"] = @item["description"]
			metadata["duration"] = 60
			metadata["rating"] = @item["rating"]
			metadata["smart_url"] = "smart_url"
		    metadata["status"] = @item["status"]
		    
		    status,error = media.update_video_list(metadata,catalog_id,contentid,videolist_id)
			if status
            	render json: {} , status: :ok
            else
            	render :json => {:error => error}.to_json, :status => 500
            end	
	end

	def edit
		media_id = params[:video_id]
		@bg = current_bg
		@video_list = Media.find_by(:_id => media_id)
		videoid = @video_list.ott_id
		@mtitle = @video_list.title
		catalog =  Catalog.find_by(:ott_id => @video_list.ott_catalog_id)
		@catalog_name = catalog.name+"["+catalog.theme+"]"
		@catalog_id = catalog.id

		url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"?auth_token="+@bg.ott_auth_token
		response = http_request("get",{},url,60)
		@catalog = response["body"]["data"]
			# if !response["status"]
			# 	status = response["status"]
			# 	error = response["error"]
			# end

		url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/videolists?auth_token="+@bg.ott_auth_token+"&status=any&region=IN"
		response = http_request("get",{},url,60)
		#logger.debug "=====================> #{response} <==================="
		#@videodata = response["body"]["video_list"]
		@videodata = response["body"]["data"]["items"]


		@videodata.each do |vli|
			if videoid == vli["videolist_id"]
				@vtitle = vli["title"]
				@category = vli["category"]
				@desc = vli["description"]
				@videolist_id = vli["videolist_id"]
				@play_url = vli["play_url"]
				@smart_url = vli["smart_url"]
				@status = vli["status"]
				@video_people = vli["people"]
				@video_asset_id = vli["asset_id"]
				@video_duration = vli["duration"]
				@l_large = vli["thumbnails"]["l_large"]["url"]
				@l_medium = vli["thumbnails"]["l_medium"]["url"]
				@p_small = vli["thumbnails"]["p_small"]["url"]
				s3uploadremovspace = @bg.name+"/"+catalog.name+"/video_list/"+@vtitle+"/images"
	        	@s3upload = s3uploadremovspace.gsub(" ", "_");
        		puts "======s3upload videolist======"
        		p @s3upload
			end
			end

		@signature = get_s3_upload_key(@s3upload,@bg.bucket_name,@bg.domain_name)

		url = "http://" + get_ott_base_url + "/people?auth_token=" + @bg.ott_auth_token
			response = http_request("get",{},url,60)
			if response["code"] == 200
				@people = response["body"]["data"]["items"]
			end
	end

	def update
		status = true
		error = ""
		videolist_id = params[:videolist_id]
		video = Media.find(params[:video_id])
		catalog = Catalog.find(params[:catalog_id])
		catalogott_id = catalog.ott_id
		media = Media.find_by(:ott_catalog_id => catalogott_id)
		metadata = extract_metadata_from_videolistparams(params)
		status,error = media.edit_videolist(metadata,catalog,videolist_id)

		message = error

		if status
			video.title = params[:title]
			video.asset_id = params[:asset_id]
			logger.debug "========================> #{params[:status]} <=============================="
			video.status = params[:status]
			video.save
			render json: {} , status: :ok
		else
			#render :json => {:error => error}.to_json, :status => 500
			render :json => {:error => message}.to_json, :status => 500
		end
	end

end
