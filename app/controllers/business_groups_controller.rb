class BusinessGroupsController < ApplicationController
	include UsersHelper
	include HomeHelper
	include MediaHelper
	include TenantsHelper
	include AppsHelper
	include ApplicationHelper
	include CatalogsHelper
	include AccessControllerHelper
	include SubscriptionHelper
	include MessagesHelper
	include CloudFrontConfig
	include CouponCodeHelper
	
	before_filter :login_check, except: [:register_bg, :verifing_bg, :update_bg, :forgot_password, :verify_forgot_password_link, :verify_forgot_password, :check_brand_name]
	def new
		@heading = "Add a New Business Group"
		render "new"
	end
	def create
		status = true
		error = ""
		#check for uniqueness of email for bg and user
		response = nil
		duplicate_Names = BusinessGroup.where(:name => /#{params[:name].strip}/i)
	   	if duplicate_Names.count !=0
	   	status = false
	   	error = "BG_CMS"
	   	end
		if status && !(params[:plan]=="transcoding")
			body = {}
			body["name"] = params[:name].strip
			body["email"] = params[:email]
			body["user_name"] = params[:name].downcase.gsub(" ","_")
			body["password"] = "welcome123"
			body["auth_token"] = get_ott_auth_token
			url = "http://"+get_ott_base_url+"/business_group"

			response = http_request("post",body,url,60)
			if !response["status"]
				status = false
				error = response["body"]["error"]["message"]
			else
			   ott_bg_resp = response["body"]["data"]
			   friendly_id = ott_bg_resp["friendly_id"]
			end
		end
		if status
			#business group creation
			bg = BusinessGroup.new
			bg.name = params[:name].strip
			bg.email = params[:email]
			bg.tx_base_url = params[:tx_base_url]
			bg.tx_auth_token = params[:tx_auth_token]
			if params[:plan]=="ott" || params[:plan]=="ott_saas"
				bg.ott_id = response["body"]["data"]["business_group_id"]
				bg.ott_auth_token = response["body"]["data"]["authentication_token"]
			end
			bg.admin_base_url = params[:admin_base_url]
			bg.plan = params[:plan]
			bg.friendly_id = friendly_id
			env = Rails.env
			case env
			when "development"
				bg.staging_webview = "http://"+get_web_view_base_url+"/WEBVIEW/"+bg.friendly_id+"/"
			else
				bg.preprod_webview = "http://"+get_web_view_base_url+"/WEBVIEW/"+bg.friendly_id+"/"
				bg.prod_webview = "http://"+get_web_view_base_url+"/WEBVIEW/"+bg.friendly_id+"/"
			end

			#check configuration for wowza or aws
			# streaming = ConfigData.find_by(:access_key => "streaming_server")
			streaming = params["streaming_server"]
			if !streaming.blank?
				server = streaming
				if server == "aws" 
					if bg.save
						status,msg = bg.delay(:queue => "#{Rails.env}_cms").create_distribution
						cdn = ["saranyu_cloudfront"]
					end
				else
				 	bg.save
				 	cdn = ["saranyu_test_cdn"]
				end	
			else
				bg.save
				cdn = ["saranyu_test_cdn"]
			# 	if bg.save
			# 		status,msg = bg.delay(:queue => "#{Rails.env}_cms").create_distribution
			# 	end	
			end	

			#create cloud front account for bg
			# if bg.save
			# 	status,msg = bg.delay(:queue => "#{Rails.env}_cms").create_distribution
				# status,msg = bg.create_distribution
				
				# logger.debug "check cloudfront settings==============>"
				# logger.debug status
				# logger.debug msg
				# if status
				# 	bg.domain_name = msg
				# 	bg.save
				# end	
			# end	
			#admin user creation
			admin = User.new
			admin.bg_id = bg.id
			admin.email = params[:email]
			admin.password = generate_password
			admin.isemail_verify = "Yes"
			admin.save
			admin.roles = ["admin"]
			admin.save
			Delayed::Job.enqueue(SendSignupMail.new(admin,admin.password), :run_at => Time.now, :queue => "#{Rails.env}_cms")
		end

		if status && !(params[:plan]=="transcoding")

			ps = PublishSettings.new
			ps.name = "Insta OTT Default Publish Settings"
			ps.profiles = ["video_profile_144p_64Kbps", "video_profile_240p_256Kbps", "video_profile_320p_512Kbps"]
			ps.has_video_tile = true
			ps.splice = nil
			ps.logo = nil
			ps.naming_tag = nil
			ps.protocols = ["hls", "http_pd"]
			#ps.cdn = ["saranyu_test_cdn"]
			# ps.cdn = ["saranyu_cloudfront"]
			ps.cdn = cdn
			ps.bg_id = bg.id
			ps.save

			#default tenant creation
			tenant = create_tenant(bg,bg.name)
			if !tenant
				status = false
				error = "Tenant could not be created at OTT server"
			end
		
			if status 
				#default android app creation&& !(params[:plan]=="transcoding")
				body = {}
				body["name"] = params[:name].strip
				body["email"] = params[:email]
				body["user_name"] = params[:name].strip
				platforms = ["web","android","ios"]
				apps = create_apps(tenant,platforms,platforms)
				if apps.nil?
					status = false
					error = "Apps could not be created at OTT server"
				end
				if apps.empty?
					status = false
					error = "Apps could not be created at OTT server"
				end
			end

			if status 
				#default access_control creation
				accesscontrol = create_access_control(bg,tenant)
				if !accesscontrol
					status = false
					error = "accesscontrol could not be created at OTT server"
				end
			end

			if status
				#default access_control_item creation
				accesscontrolitems = default_create_access_control_items(bg,accesscontrol.ott_id,tenant)
				if !accesscontrolitems
					status = false
					error = "accesscontrolitems could not be created at OTT server"
				end
			end

			if status 
				#Create Subscription Catalog
				subscription = create_subscription(bg,tenant)
				if !subscription
					status = false
					error = "subscription could not be created at OTT server"
				end
			end

			if status 
				#Create Plans for apps
				apps[2].each do |apid|
				appd = App.find_by(:ott_id => apid)
				appid = appd.id
				tenantid = appd.tenant_id
		  		planitems = subscription_items(bg,tenantid,appid)
				if !planitems
					status = false
					error = "Plans could not be created at OTT server"
				end
				end

				subscription_free_pack =  subscription_free_pack(bg,tenant)
			    if !subscription_free_pack
			    	status = false
			    	error = "App_Plans could not be created at OTT"
			    end
			end

			if status
				#Create messages
				message = create_catalog_message_ott(bg,tenant)
				if !message
					status = false
					error = "message could not be created at OTT server"
				end
			end

			if status
				coupon_code = create_catalog_coupon(bg,tenant)
				if !coupon_code
					status = false
					error = "coupon code could not be created at OTT server"
				end
			end

			if status 

				message_types = ConfigData.find_by(:access_key => "messages001")
				message_types.data["messages"].each do |type|
	            message_items = message_items(bg,tenant,type)
				if !message_items
					status = false
					error = "Message could not be created at OTT server"
				end
				end
			end

		end	

		if status
			BusinessGroupPlan.create(:plan_id => "5742dca4a1af603d6f000002",\
			:business_group_id => bg.id,:status => "active",:start_date => Time.now,\
			:days => 30,:plan_details => {"cost"=>0,"currency"=>"INR", "days"=>30,\
			"display_title"=>"Trial Pack", "status"=>"active", "title"=>"trial",\
			"trackers"=>[{"id"=>"5742d473a1af605fe7000001", "title"=>"catalog_limit",\
			"display_title"=>"Number of Catalogs", "count"=>5, "tracker_type"=>"count"},\
			{"id"=>"5742d494a1af605fe7000002", "title"=>"media_limit",\
			"display_title"=>"Number of Videos", "count"=>10, "tracker_type"=>"count"}]})
			render json: {} , status: :ok and return
		else
			if !(params[:plan]=="transcoding") && bg
				#delete self from OTT server
			    body = {}
			    body["auth_token"] = get_ott_auth_token
			    url = "http://"+get_ott_base_url+"/business_group/"+bg.ott_id
			    response = http_request("delete",body,url,60)
			    if !response["status"]
			      status = response["status"]
			      error = response["error"]
			    end
				bg.destroy
			end
			render :json => {:error => error}.to_json, :status => 500
		end

	end
	def fetch
	end

	
	def update
		status = true
		error = ""
		case params[:method]
		when "delete"
			bg = BusinessGroup.find(params[:bg_id])
			#delete self from OTT server
			if bg.plan != "transcoding"
			    body = {}
			    body["auth_token"] = get_ott_auth_token
			    url = "http://"+get_ott_base_url+"/business_group/"+bg.ott_id
			    response = http_request("delete",body,url,60)
			    if !response["status"]
			      status = response["status"]
			      error = response["error"]
			    end
			end
		    if status
				bg.destroy
			end
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
	def fetch_all
		status = true
		error = ""
		if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end

		page_size = params[:page_size] ? params[:page_size] : 5
		params[:page_size] = page_size
		if current_user.roles.include?"cms_manager"
			business_groups = []
			BusinessGroup.all.each do |bg|
				if bg.ott_id.blank?
				else
				h = {}
				h["id"] = bg.id
				h["name"] = bg.name
				h["email"] = bg.email
				h["tx_base_url"] = bg.tx_base_url
				h["tx_auth_token"] = bg.tx_auth_token
				h["ott_id"] = bg.ott_id
				h["ott_auth_token"] = bg.ott_auth_token
				h["admin_base_url"] = bg.admin_base_url
				h["plan"] = bg.plan
				total_media_count = Media.where(:is_removed=>false).where(:bg_id=>bg.id).count
				published_media_count = Media.where(:is_removed=>false).where(:bg_id=>bg.id).where(:status=>"Published").count
				waiting_media_count = Media.where(:is_removed=>false).where(:bg_id=>bg.id).where(:status=>"Created").count
				error_media_count = Media.where(:is_removed=>false).where(:bg_id=>bg.id).any_of({:status=>"Transcoding Error"},{:status=>"Request Error"},{:status=>"Ingestion Error"}).count
				processing_media_count = total_media_count - published_media_count - waiting_media_count - error_media_count
				h["total_media_count"] = total_media_count
				h["published_media_count"] = published_media_count
				h["processing_media_count"] = processing_media_count
				h["error_media_count"] = error_media_count
				business_groups << h
			    end
			end
		else
			status = false
			error = "Unauthorized Access"
		end
		if status
		if params[:title].present?
      		bs_groups =  business_groups.select {|k| k if k["name"].to_s.match(/#{params[:title]}/i)}
    		render json: {:list => bs_groups, :next_cnt => {},:psize => params[:page_size],:pge => page,:nxt => 0,:icount => 0}
    	 else
			bs_groups = Kaminari.paginate_array(business_groups).page(page).per(page_size)
			next_items = Kaminari.paginate_array(business_groups).page(next_page).per(page_size)
			nxt_ims = next_items.count 
        	items_cnt = bs_groups.count
			render json: {:list => bs_groups, :next_cnt => next_items,:psize => params[:page_size],:pge => page,:nxt => nxt_ims,:icount => items_cnt}
		end
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end

	def view_all
		@heading = "View All Business Groups"
		@business_groups = []
		BusinessGroup.all.each do |bg|
			h = {}
			h["id"] = bg.id
			h["name"] = bg.name
			h["email"] = bg.email
			h["tx_base_url"] = bg.tx_base_url
			h["tx_auth_token"] = bg.tx_auth_token
			h["ott_auth_token"] = bg.ott_auth_token
			h["admin_base_url"] = bg.admin_base_url
			h["plan"] = bg.plan
			total_media_count = Media.where(:is_removed=>false).where(:bg_id=>bg.id).count
			published_media_count = Media.where(:is_removed=>false).where(:bg_id=>bg.id).where(:status=>"Published").count
			waiting_media_count = Media.where(:is_removed=>false).where(:bg_id=>bg.id).where(:status=>"Created").count
			error_media_count = Media.where(:is_removed=>false).where(:bg_id=>bg.id).any_of({:status=>"Transcoding Error"},{:status=>"Request Error"},{:status=>"Ingestion Error"}).count
			processing_media_count = total_media_count - published_media_count - waiting_media_count - error_media_count
			h["total_media_count"] = total_media_count
			h["published_media_count"] = published_media_count
			h["processing_media_count"] = processing_media_count
			h["error_media_count"] = error_media_count
			@business_groups << h
		end
		render "view_all"
	end
	def report
		status = true
		error = ""
		if status && (!params[:start_date] || params[:start_date] == "")
			status = false
			error = "Start Date is not available!"
		end
		if status && (!params[:end_date] || params[:end_date] == "")
			status = false
			error = "End Date is not available!"
		end
		if status && (Time.parse(params[:start_date]) > Time.parse(params[:end_date]))
			status = false
			error = "End Date cannot be older than Start Date"
		end
		if status
			ts = Time.parse(params[:start_date])
			te = Time.parse(params[:end_date])+1.day
			media_count = 0
			transcoding_count = 0
			transcoding_seconds = 0
			Media.where(:bg_id=>current_bg.id).where(:status=>"Published").each do |media|
				if (media.created_at >= ts) && (media.created_at < te)
					if media["input_info"] && media["output_info"]
						media_count = media_count + 1
						transcoding_count += media["output_info"]["profiles"].count
						transcoding_seconds += media["output_info"]["profiles"].count*media["input_info"]["duration"]
					end
				end
			end
			@report = {}
			@report["start_date"] = ts.strftime("%d %B, %Y")
			@report["end_date"] = te.strftime("%d %B, %Y")
			@report["media_count"] = media_count
			@report["transcoding_count"] = transcoding_count
			@report["transcoding_minutes"] = transcoding_seconds/60
			#render json: {:report => report} , status: :ok
			render partial: "view_report"
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
	def fetch_platform_details
		status = true
		error = ""
		body = {}
  		maxtime = 5
  		platform_details = nil
  		server_list = []
  		bg = current_bg
		begin
			Timeout::timeout(maxtime) do
				Fiber.new{
					url = "http://#{bg.admin_base_url}/platform_details"
					ret = Typhoeus::Request.post(url,:body => body.to_json,:headers => {"content-type" => "application/json","accept" =>"application/json"})
					if ret.options[:response_body]
						platform_details = JSON.parse(ret.options[:response_body])
					end
				}.resume
			end
			servers_hash = platform_details["servers"]
			servers_hash.each do |key,value|
				h = {}
				h["server_id"] = key
				value.each do |k,v|
					h[k] = v
				end
				server_list << h
			end
		rescue Timeout::Error
			status = false
			error = "Request Timed Out"
		rescue 	Exception => e
			Rails.logger.debug e.message
			Rails.logger.debug e.backtrace.inspect
			status = false
			error = "Platform details are not available right now!"
		end
		if status
			render json: {:server_list => server_list, :current_user_roles=>current_user.roles} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
	def fetch_instance_details
		status = true
		error = ""
		body = {}
		server = {}
		server["instance_id"] = params[:instance_id]
		body["server"] = server
  		maxtime = 60
  		@instance_details = nil
  		bg = current_bg
		begin
			Timeout::timeout(maxtime) do
				Fiber.new{
					url = "http://#{bg.admin_base_url}/instance_details"
					ret = Typhoeus::Request.post(url,:body => body.to_json,:headers => {"content-type" => "application/json","accept" =>"application/json"})
					if ret.options[:response_body]
						@instance_details = JSON.parse(ret.options[:response_body])["server"]
					end
				}.resume
			end
		rescue Timeout::Error
			status = false
			error = "Request Timed Out"
		rescue 	Exception => e
			Rails.logger.debug e.message
			Rails.logger.debug e.backtrace.inspect
			status = false
			error = "Exception Raised"
		end
		if status
			#render json: {:instance_details => instance_details} , status: :ok
			render partial: "view_instance"
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end


	def register_bg
		# abg = BusinessGroup.find_by(:name => /#{params[:name].strip}/i)
		# email = BusinessGroup.find_by(:email => params[:email])
		# if abg
		# 	render :json => {:error => "Brand Name already Exists. Try Different Brand Name"}.to_json, :status => 500
		# elsif email
		# 	render :json => {:error => "Email already Exists"}.to_json, :status => 500
		# else
		bg = BusinessGroup.new
		bg.name = params[:name].strip
		bg.email = params[:email]
		bg.tx_base_url = "162.243.203.107:9900"
		bg.tx_auth_token = "ywVXaTzycwZ8agEs3ujx"
		bg.admin_base_url = "162.243.203.107:4590"
		bg.plan = "ott_saas"
		bg.save
		#admin user creation
		admin = User.new
		admin.bg_id = bg.id
		admin.email = params[:email]
		admin.first_name = params[:first_name]
		admin.last_name = params[:last_name]
		admin.password = params[:bgpass]
		admin.organization = params[:organization]
		admin.isemail_verify = "NO"
		admin.email_verification_key = get_email_verification_key params[:email]
		admin.save
		admin.roles = ["admin"]
		admin.save
		BusinessGroupPlan.create(:plan_id => "5742dca4a1af603d6f000002",\
		:business_group_id => bg.id,:status => "active",:start_date => Time.now,\
		:days => 30,:plan_details => {"cost"=>0,"currency"=>"INR", "days"=>30,\
		"display_title"=>"Trial Pack", "status"=>"active", "title"=>"trial",\
		"trackers"=>[{"id"=>"5742d473a1af605fe7000001", "title"=>"catalog_limit",\
		"display_title"=>"Number of Catalogs", "count"=>5, "tracker_type"=>"count"},\
		{"id"=>"5742d494a1af605fe7000002", "title"=>"media_limit",\
		"display_title"=>"Number of Videos", "count"=>10, "tracker_type"=>"count"}]})
		Delayed::Job.enqueue(RegisterMail.new(admin), :run_at => Time.now, :queue => "#{Rails.env}_cms")
		render json: {}, status: :ok
		# end
	end

	def update_bg
		status = true
		error = ""
		userid = params[:id]
		admin = User.find_by(:email_verification_key => userid)
		if admin.blank? || admin.isemail_verify == "Yes"
			status = false
		end

		#check for uniqueness of email for bg and user
		response = nil
		if status 
			bg = BusinessGroup.find_by(:id => admin.bg_id)
			body = {}
			body["name"] = bg.name
			body["email"] = bg.email
			body["user_name"] = bg.name.downcase.gsub(" ","_")
			body["password"] = admin.password
			body["auth_token"] = get_ott_auth_token
			url = "http://"+get_ott_base_url+"/business_group"
			response = http_request("post",body,url,60)
			if !response["status"]
				status = false
				error = response["body"]["error"]["message"]
			else
			   ott_bg_resp = response["body"]["data"]
			   friendly_id = ott_bg_resp["friendly_id"]
			end
		end
		if status
			#business group update
			bg.ott_id = response["body"]["data"]["business_group_id"]
			bg.ott_auth_token = response["body"]["data"]["authentication_token"]
			bg.friendly_id = friendly_id
			env = Rails.env
			case env
			when "development"
				bg.staging_webview = "http://"+get_web_view_base_url+"/WEBVIEW/"+bg.friendly_id+"/"
			else
				bg.preprod_webview = "http://"+get_web_view_base_url+"/WEBVIEW/"+bg.friendly_id+"/"
				bg.prod_webview = "http://"+get_web_view_base_url+"/WEBVIEW/"+bg.friendly_id+"/"
			end
 			# bg.save
 			
 			#create cloud front account for bg

			# if bg.save
			# 	# status,msg = create_distribution(bg)
			# 	status,msg = bg.delay(:queue => "#{Rails.env}_cms").create_distribution
			# 	if status
			# 		bg.domain_name = msg
			# 		bg.save
			# 	end	
			# end	

			#check configuration for wowza or aws
			streaming = ConfigData.find_by(:access_key => "streaming_server")
			if streaming
				server = streaming.data["streaming_server"]
				if server == "aws" 
					if bg.save
						status,msg = bg.delay(:queue => "#{Rails.env}_cms").create_distribution
						cdn = ["saranyu_cloudfront"]
					end
				else
				 	bg.save
				 	cdn = ["saranyu_test_cdn"]
				end	
			else
				bg.save
				cdn = ["saranyu_test_cdn"]
				# if bg.save
				# 	status,msg = bg.delay(:queue => "#{Rails.env}_cms").create_distribution
				# end	
			end	

			#admin user update
			admin.bg_id = bg.id
			admin.isemail_verify = "Yes"
			admin.save
		end

		if status 

			ps = PublishSettings.new
			ps.name = "Insta OTT Default Publish Settings"
			ps.profiles = ["video_profile_144p_64Kbps", "video_profile_240p_256Kbps", "video_profile_320p_512Kbps"]
			ps.has_video_tile = true
			ps.splice = nil
			ps.logo = nil
			ps.naming_tag = nil
			ps.protocols = ["hls", "http_pd"]
			# ps.cdn = ["saranyu_test_cdn"]
			# ps.cdn = ["saranyu_cloudfront"]
			ps.cdn = cdn
			ps.bg_id = bg.id
			ps.save

			#default tenant creation
			tenant = create_tenant(bg,bg.name)
			if !tenant
				status = false
				error = "Tenant could not be created at OTT server"
			end
		
			if status 
				#default android app creation&& !(params[:plan]=="transcoding")
				body = {}
				body["name"] = bg.name
				body["email"] = bg.email
				body["user_name"] = bg.name
				platforms = ["web","android","ios"]
				apps = create_apps(tenant,platforms,platforms)
				if apps.nil?
					status = false
					error = "Apps could not be created at OTT server"
				end
				if apps.empty?
					status = false
					error = "Apps could not be created at OTT server"
				end
			end

			if status 
				#default access_control creation
				accesscontrol = create_access_control(bg,tenant)
				if !accesscontrol
					status = false
					error = "accesscontrol could not be created at OTT server"
				end
			end

			if status
				#default access_control_item creation
				accesscontrolitems = default_create_access_control_items(bg,accesscontrol.ott_id,tenant)
				if !accesscontrolitems
					status = false
					error = "accesscontrolitems could not be created at OTT server"
				end
			end

			if status 
				#Create Subscription Catalog
				subscription = create_subscription(bg,tenant)
				if !subscription
					status = false
					error = "subscription could not be created at OTT server"
				end
			end

			if status 
				#Create Plans for apps
				apps[2].each do |apid|
				appd = App.find_by(:ott_id => apid)
				appid = appd.id
				tenantid = appd.tenant_id
		  		planitems = subscription_items(bg,tenantid,appid)
				if !planitems
					status = false
					error = "Plans could not be created at OTT server"
				end
				end

				subscription_free_pack =  subscription_free_pack(bg,tenant)
			    if !subscription_free_pack
			    	status = false
			    	error = "App_Plans could not be created at OTT"
			    end
			end


			if status
				#Create messages
				message = create_catalog_message_ott(bg,tenant)
				if !message
					status = false
					error = "message could not be created at OTT server"
				end
			end

			if status
				coupon_code = create_catalog_coupon(bg,tenant)
				if !coupon_code
					status = false
					error = "coupon code could not be created at OTT server"
				end
			end

			if status 
				message_types = ConfigData.find_by(:access_key => "messages001")
				message_types.data["messages"].each do |type|
	            message_items = message_items(bg,tenant,type)
				if !message_items
					status = false
					error = "Message could not be created at OTT server"
				end
				end
			end
			
		end	



		if status
			#render json: {} , status: :ok and return
			Delayed::Job.enqueue(ConfirmMail.new(admin), :run_at => Time.now, :queue => "#{Rails.env}_cms")
			redirect_to "/?v=pass" 
		else
			if admin.blank?
				redirect_to "/?v=fail"
			elsif admin.isemail_verify == "Yes"
				redirect_to "/?v=alreg"
			else
				bg = BusinessGroup.find_by(:id => admin.bg_id)
				#delete self from OTT server
			    body = {}
			    body["auth_token"] = get_ott_auth_token
			    url = "http://"+get_ott_base_url+"/business_group/"+bg.ott_id
			    response = http_request("delete",body,url,60)
			    if !response["status"]
			      status = response["status"]
			      error = response["error"]
			    end
				bg.destroy
				admin.destroy
				redirect_to "/?v=fail"
			end
			
			#render :json => {:error => error}.to_json, :status => 500
		end

	end

	def forgot_password

		admin = User.find_by(:email => params[:email])
		if !admin 
			render :json => {:error => "You are not registered with us. Sign Up then try to Sign In."}.to_json, :status => 500
		else
		isemail_verify = admin.isemail_verify
		if isemail_verify == "NO" || isemail_verify.nil?
			render :json => {:error => "Email Verification Pending. Please check your Email and Confirm"}.to_json, :status => 500
		else
			admin.password = nil
			admin.forgot_verification_key = get_email_verification_key params[:email]
			admin.save
			Delayed::Job.enqueue(ForgotMail.new(admin), :run_at => Time.now, :queue => "#{Rails.env}_cms")
			render json: {}, status: :ok
		end		
		end

	end

	def verify_forgot_password_link
		@userid = params[:id]
		render "business_groups/forgot_password"
	end

	def verify_forgot_password
		userid = params[:uid]
		admin = User.find_by(:forgot_verification_key => userid)
		if !admin 
			render :json => {:error => "You are not registered with us. Sign Up then try to Sign In"}.to_json, :status => 500
		else
		isemail_verify = admin.isemail_verify
		if isemail_verify == "NO" || isemail_verify.nil?
			render :json => {:error => "Email Verification Pending.Please check your Email and Confirm"}.to_json, :status => 500
		else
			admin.password = params[:bgpass]
			admin.forgot_verification_key = userid
			admin.save
			render json: {}, status: :ok
		end		
		end

	end


	def get_email_verification_key email
       Digest::MD5.hexdigest(email + Time.now.to_s)
    end

    def check_brand_name
      b_name = BusinessGroup.find_by(name: params[:name])
      
      respond_to do |format|
        if b_name
          format.json { render :json => { status: "true" } }
        else
      	  format.json { render :json => { status: "false" } }
        end
      end
    end
end
