class HomeController < ApplicationController
	include UsersHelper
	include HomeHelper
	include MediaHelper
	include ApplicationHelper
	
	before_filter :login_check, :except => ["index"]
	def index
		@bg = current_bg
		if @bg
			@monthly_stats = @bg.monthly_tx_stats
			logger.debug @monthly_stats
		end
		if current_user
			if current_user.roles.include?"cms_manager"
				if current_bg
					render "admin_home"
				else
		if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
		page_size = params[:page_size] ? params[:page_size] : 25
		params[:page_size] = page_size
		business_groups = []
			BusinessGroup.all.each do |bg|
				if bg.ott_id.blank?
				else
				h = {}
				h["id"] = bg.id
				h["name"] = bg.name
				h["email"] = bg.email
				h["tx_base_url"] = bg.tx_base_url
				h["tx_auth_token"] = bg.tx_auth_token
				h["ott_id"] = bg.ott_id
				h["ott_auth_token"] = bg.ott_auth_token
				h["admin_base_url"] = bg.admin_base_url
				h["plan"] = bg.plan
				total_media_count = Media.where(:is_removed=>false).where(:bg_id=>bg.id).count
				published_media_count = Media.where(:is_removed=>false).where(:bg_id=>bg.id).where(:status=>"Published").count
				waiting_media_count = Media.where(:is_removed=>false).where(:bg_id=>bg.id).where(:status=>"Created").count
				error_media_count = Media.where(:is_removed=>false).where(:bg_id=>bg.id).any_of({:status=>"Transcoding Error"},{:status=>"Request Error"},{:status=>"Ingestion Error"}).count
				processing_media_count = total_media_count - published_media_count - waiting_media_count - error_media_count
				h["total_media_count"] = total_media_count
				h["published_media_count"] = published_media_count
				h["processing_media_count"] = processing_media_count
				h["error_media_count"] = error_media_count
				business_groups << h
			    end
			end
				if params[:title].present?
	      		@bs_groups =  business_groups.select {|k| k if k["name"].to_s.match(/#{params[:title]}/i)}
	    		else
				@bs_groups = Kaminari.paginate_array(business_groups).page(page).per(page_size)
				end
				@next_items = Kaminari.paginate_array(business_groups).page(next_page).per(page_size)
					render "cms_manager_home"
				end
			elsif current_user.roles.include?"admin"
				render "admin_home"
			elsif current_user.roles.include?"publisher"
				@monthly_stats = current_user.monthly_tx_stats
				render "publisher_home"
			elsif current_user.roles.include?"content_editor"
				render "content_editor_home"
			elsif current_user.roles.include?"content_owner"
				redirect_to content_owner_dashboard_path,:layout => "content_owner"
			elsif current_user.roles.include?"media_viewer"
				render "media_viewer_home"
			end
		else
			render "home/index", :layout => false
		end
	end

=begin
	def dashboard
		if current_user.roles.include?"cms_manager"
			@bg = BusinessGroup.find_by(:id => params[:bg_id])
		else
			@bg = BusinessGroup.find_by(:id => current_user.bg_id)
		end
		if (current_user.roles.include?"cms_manager") || (current_user.roles.include?"admin")	
			@heading = @bg.name+" Admin Dashboard"
		elsif current_user.roles.include?"publisher"
			@heading = @bg.name+" Publisher Dashboard"
		elsif current_user.roles.include?"content_editor"
			@heading = @bg.name+" Content Editor Dashboard"
		end
		if !params[:tab]
			@tab = "view_media"
		elsif params[:tab]
			@tab = params[:tab]
			case params[:tab]
			when "view_users"
				if !params[:page]
					@user_list = User.where(:bg_id => @bg.id).order_by(:created_at => 'desc').page(1)
				else
					@user_list = User.where(:bg_id => @bg.id).order_by(:created_at => 'desc').page(params[:page])
				end
			when "view_profiles"
				@profile_list = []
				Profile.where(:is_system_profile => true).order_by(:created_at => 'desc').each do |profile|
					@profile_list << profile
				end
				Profile.where(:bg_id => @bg.id).order_by(:created_at => 'desc').each do |profile|
					@profile_list << profile
				end
				if !params[:page]
					@profile_list = Kaminari.paginate_array(@profile_list).page(1)
				else
					@profile_list = Kaminari.paginate_array(@profile_list).page(params[:page])
				end
			when "add_media"
				@ps_list_single = []
				PublishSettings.where(:bg_id => @bg.id).order_by(:created_at => 'desc').each do |ps|
					disqualify = false
					if ps.naming_tag
						if has_custom_keys(ps.naming_tag)
							disqualify = true
						end
					end
					if extra_keys_required_for_storage(ps.cdn.first).count > 0
						disqualify = true
					end
					if !disqualify
						@ps_list_single << ps
					end
				end
			when "view_ps"
				if !params[:page]
					@ps_list_bulk = PublishSettings.where(:bg_id => @bg.id).order_by(:created_at => 'desc').page(1)
				else
					@ps_list_bulk = PublishSettings.where(:bg_id => @bg.id).order_by(:created_at => 'desc').page(params[:page])
				end
			when "edit_media"
				@ps_list_bulk = PublishSettings.where(:bg_id => @bg.id).order_by(:created_at => 'desc')
				@ps_list_single = []
				PublishSettings.where(:bg_id => @bg.id).order_by(:created_at => 'desc').each do |ps|
					disqualify = false
					if ps.naming_tag
						if has_custom_keys(ps.naming_tag)
							disqualify = true
						end
					end
					if extra_keys_required_for_storage(ps.cdn.first).count > 0
						disqualify = true
					end
					if !disqualify
						@ps_list_single << ps
					end
				end
				@edit_media = Media.find(params[:media_id])
				if @edit_media.status!="Created"
					@hide_transcode_data = true
				end
			when "add_app"
				@tenants = Tenant.where(:bg_id => @bg.id).order_by(:created_at => 'desc')
			end
		end
		render "dashboard"
	end
=end
end