class PublishSettingsController < ApplicationController
	include ApplicationHelper
	
	before_filter :login_check, :except => []
	def create
		status = true
		error = nil
		ps = PublishSettings.new
		ps.name = params[:name].strip
		duplicate_Names = PublishSettings.where(:name => /#{params[:name].strip}/i, :bg_id => current_bg.id)
	   	if duplicate_Names.count !=0
	   	status = false
	   	error = "Publish Settings Already Exists!"
	   	end
		#ps.profiles = params[:profiles].split(",").map(&:strip)
		ps.profiles = params[:profiles]
		#check if said profiles are valid for the bg
		valid_profiles = []
		Profile.where(:bg_id=>current_bg.id).each do |profile|
			valid_profiles << profile.name
		end
		Profile.where(:is_system_profile=>true).each do |profile|
			valid_profiles << profile.name
		end
		ps.profiles.each do |profile_name|
			if !(valid_profiles.include? profile_name)
				status = false
				error = profile_name + " is an invalid profile for this Business Group"
			end
		end
		if status && params[:splice]
			if params[:start_at_second].present?
				if !(params[:start_at_second].to_i.is_a? Integer)
					status = false
					error = params[:start_at_second] + " is not a valid entry for splice start at second"
				end
			else
				status = false
				error = params[:start_at_second] + " is not a valid entry for splice start at second"
			end
			if params[:end_at_second].present?
				if !(params[:end_at_second].to_i.is_a? Integer)
					status = false
					error = params[:end_at_second] + " is not a valid entry for splice end at second"
				end
			else
				status = false
				error = params[:end_at_second] + " is not a valid entry for splice end at second"
			end
			if !(params[:end_at_second].to_i > params[:start_at_second].to_i)
				status = false
				error = "splice end time has to be later than splice start time"
			end
			if status 
				splice = {}
				splice["start_at_second"] = params[:start_at_second].to_i
				splice["end_at_second"] = params[:end_at_second].to_i
				ps.splice = splice
			end
		end
		if status && params[:logo]
			if params[:logo_file_url].present?
				if !params[:logo_file_url].is_a? String
					status = false
					error = params[:logo_file_url] + " is not a valid entry for logo file url"
				end
			end
			if params[:logo_width].present?
				if !(params[:logo_width].to_i.is_a? Integer)
					status = false
					error = params[:logo_width] + " is not a valid entry for logo width"
				end
			else
				status = false
				error = params[:logo_width] + " is not a valid entry for logo width"
			end
			if params[:logo_height].present?
				if !(params[:logo_height].to_i.is_a? Integer)
					status = false
					error = params[:logo_height] + " is not a valid entry for logo height"
				end
			else
				status = false
				error = params[:logo_height] + " is not a valid entry for logo height"
			end
			if params[:x_offset].present?
				if !(params[:x_offset].to_i.is_a? Integer)
					status = false
					error = params[:x_offset] + " is not a valid entry for x-offset"
				end
			else
				status = false
				error = params[:x_offset] + " is not a valid entry for x-offset"
			end
			if params[:y_offset].present?
				if !(params[:y_offset].to_i.is_a? Integer)
					status = false
					error = params[:y_offset] + " is not a valid entry for y-offset"
				end
			else
				status = false
				error = params[:y_offset] + " is not a valid entry for y-offset"
			end
			if status 
				logo = {}
				logo["file_url"] = params[:logo_file_url]
				logo["width"] = params[:logo_width].to_i
				logo["height"] = params[:logo_height].to_i
				logo["x_offset"] = params[:x_offset].to_i
				logo["y_offset"] = params[:y_offset].to_i
				ps.logo = logo
			end
		end
		if status && params[:video_tile]
			if params[:video_tile]=="on"
				ps.has_video_tile = true
			else
				ps.has_video_tile = false
			end
		else
			ps.has_video_tile = false
		end
		if status && params[:naming_tag]
			count = params[:con_types].count
			constructor = []
			for i in 0..count-1
				h = {}
				case params[:con_types][i]
				when "fixed"
					h["type"] = params[:con_types][i]
					h["key"] = nil
					h["value"] = params[:con_values][i]
					constructor << h
				when "variable"
					h["type"] = params[:con_types][i]
					h["key"] = params[:con_keys][i]
					h["value"] = nil
					constructor << h
				when "custom"
					h["type"] = params[:con_types][i]
					h["key"] = params[:con_keys][i]
					h["value"] = nil
					constructor << h
				end
			end
			replacer = []
			count = params[:rep_inputs].count
			for i in 0..count-1
				h = {}
				h["input"] = params[:rep_inputs][i]
				h["output"] = params[:rep_outputs][i]
				replacer << h
			end
			ps.naming_tag = {}
			ps.naming_tag["constructor"] = constructor
			ps.naming_tag["replacer"] = replacer
		end
		if status
			#ps.protocols = params[:protocols].split(",").map(&:strip)
			ps.protocols = params[:protocols]
			ps.cdn = params[:cdn].split(",").map(&:strip)
			ps.bg_id = current_bg.id
			ps.save
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
	def delete
		ps = PublishSettings.find(params[:ps_id])
		ps.destroy
		render json: {} , status: :ok
	end
	def fetch_all
		status = true
		error = ""
		ps_list = nil
		@bg = current_bg
		ps_list = PublishSettings.where(:bg_id=>@bg.id).order_by(:created_at => 'desc').page(params[:page]).per(params[:page_size])
		if status
			render json: {:list => ps_list, :total_count => ps_list.total_count, :total_pages => ps_list.total_pages} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
	def new
		@bg = current_bg
		@profile_list = []
		Profile.where(:is_system_profile => true).order_by(:created_at => 'desc').each do |profile|
			@profile_list << profile
		end
		Profile.where(:bg_id => @bg.id).order_by(:created_at => 'desc').each do |profile|
			@profile_list << profile
		end
	end
	def view_all
		@bg = current_bg
			if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
		page_size = params[:page_size] ? params[:page_size] : 25
		params[:page_size] = page_size
		@pb_settings = PublishSettings.all_of(:bg_id => @bg.id).order_by(:created_at => 'desc').page(page).per(page_size)
		@next_pb_settings = PublishSettings.all_of(:bg_id => @bg.id).order_by(:created_at => 'desc').page(next_page).per(page_size)
    	if params[:title].present?
      @pb_settings= PublishSettings.all_of(:bg_id => @bg.id).where(:name => /#{params[:title]}/i)
      # @next_pb_settings = @next_pb_settings.where(:name => /#{params[:title]}/i)
    	end
	end
end
