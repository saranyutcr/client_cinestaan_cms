class ShowsController < ApplicationController
  include ApplicationHelper

  before_filter :set_object

  def set_object
  	@show = ShowTheme.new
    @episode = Media.new
    @subcategory = Subcategory.new
  end
  	
	def new
		@bg = current_bg
    @catalog_id = params[:catalog_id]
		catalog = Catalog.find(params[:catalog_id])
		url = "http://" + get_ott_base_url + "/catalogs/" + catalog.ott_id + "?auth_token=" + @bg.ott_auth_token
		response = http_request("get",{},url,60)
		@catalog = response["body"]["data"]

    # Upload image to s3 server
      s3uploadremovspace = @bg.name+"/"+@catalog["name"]+"/shows"
      s3upload = s3uploadremovspace.gsub(" ", "_");
      @signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)

	end	

	def create
		params[:catalog_object] = eval(params[:catalog_object])
    if params[:child_flag] == "episodes"
      params[:subcategories_flag] = "no"
      params[:episodes_flag] = "yes"
    else
      params[:subcategories_flag] = "yes"
      params[:episodes_flag] = "no"
    end  
    status,obj = @show.create_at_ott(params,current_bg)
    if status
    	render json: {} , status: :ok
    else
    	render :json => {:error => obj}.to_json, :status => 500
    end	
	end	

	def view_all
	end	

	def get_all
    shows = @show.get_shows_by_bg(current_bg)   
    data = shows.as_json.map do |show|
    	tenant_names = []
        tenants = show["tenant_ids"]
        tenants.each do |t|
          name = Tenant.find(t).name
          tenant_names << name if name
        end 
        catalog_name = Catalog.find_by(:id => show["catalog_id"]).name
        show.merge!({"tenant_names" => tenant_names,"catalog_name" => catalog_name })	
    end	
    render json: {:list => data}
	end	

  #Business group can edit title,description,status,featured image,landscape image and portrait image
  def edit
    @bg = current_bg
    show = @show.get_show_by_ott_id(params[:show_id])
    if show
      url = "http://" + get_ott_base_url + "/catalogs/" + show.ott_catalog_id + "/items/" + show.ott_id + "?region=IN&auth_token=" + @bg.ott_auth_token + "&status=any"
      response = http_request("get",{},url,60)
      @show_resp = response["body"]["data"]

      caturl = "http://" + get_ott_base_url + "/catalogs/" + show.ott_catalog_id + "?auth_token=" + @bg.ott_auth_token
      response = http_request("get",{},caturl,60)
      #logger.debug "===========> Status :: #{response.inspect} <========="
      if response["status"]
        @catalog = response["body"]["data"]
        @t_ott_id = response["body"]["data"]["tenant_ids"]
        @tenant_list = []

        @t_ott_id.each do |obj|
          Tenant.where(:ott_id=>obj).each do |tenan|
            h = {}
            h["id"] = tenan.id
            h["name"] = tenan.name
            h["ott_id"] = tenan.ott_id

            h ["genres"] =[]
              response["body"]["data"]["genres"].keys.each do |g|
              h["genres"] = (response["body"]["data"]["genres"][tenan.ott_id]).map{|gen| gen["name"] } if g == h["ott_id"]
            end

            h ["languages"] =[]
            response["body"]["data"]["languages"].keys.each do |l|
              h ["languages"] = (response["body"]["data"]["languages"][tenan.ott_id]).map{|lan| lan["name"] } if l == h["ott_id"]
            end 

            # h["applications"] = []
            # App.where(:tenant_id => tenan.id).each do |tapp|
            #   if @catalog["application_ids"].include? tapp.ott_id 
            #     application = {}
            #     application["a_id"] = tapp.id
            #     application["a_name"] = tapp.name
            #     application["a_tid"] = tapp.tenant_id
            #     h['applications'] << application
            #   end
            # end
            @tenant_list << h
          end
        end
      end

      # Upload image to s3 server
      #s3upload = @bg.name+"/"+@show_resp["catalog_name"]+"/shows/images"
      s3uploadremovspace = @bg.name+"/"+@show_resp["catalog_name"]+"/shows"
      s3upload = s3uploadremovspace.gsub(" ", "_");
      @signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)

      render "edit"
    else
        
    end
  end  

  def update
    if params[:method] == "update"
      params[:show_obj] = eval(params[:show_obj])
      params[:catalog_object] = {}
      params[:catalog_object][:catalog_id] = params[:show_obj][:catalog_id]
      params[:catalog_object][:application_ids] = params[:show_obj][:app_ids]
      params[:catalog_object][:tenant_ids] = params[:show_obj][:tenant_ids]
      params[:subcategories_flag] = params[:show_obj][:subcategory_flag]
      params[:episodes_flag] = params[:show_obj][:episode_flag]
      status,obj = @show.update_at_ott(params,current_bg)
      if status
        render json: {} , status: :ok
      else
        render :json => {:error => obj}.to_json, :status => 500
      end 
    end 

    if params[:method] == "delete"
      show = @show.get_show_by_id(params[:id]) || @show.get_show_by_ott_id(params[:id])
      if show.episodes_flag == "yes"
        show_cms_id = show.id
        episodes = @episode.get_media_by_show(show_cms_id)
        if episodes.count > 0
          render :json => {:error => "Please delete epsiodes under this Show!"}.to_json, :status => 500
          return
        else
          ##Delete Show from OTT
          status,msg = show.delete_show_from_ott

          ##Delete Show from CMS
          if status
            status,msg = show.delete_show_from_cms
          end
        end
      else
        show_cms_id = show.id
        subcategories = @subcategory.get_subcategories_by_show_id(show_cms_id)
        if subcategories.count > 0  
          render :json => {:error => "Please delete Subcategories under this Show!"}.to_json, :status => 500
          return
        else
          ##Delete Show from OTT
          status,msg = show.delete_show_from_ott

          ##Delete Show from CMS
          if status
            status,msg = show.delete_show_from_cms
          end
        end  
      end

      if status
        render json: {} , status: :ok
      else
          render :json => {:error => msg}.to_json, :status => 500
      end
    end  

  end  

end
