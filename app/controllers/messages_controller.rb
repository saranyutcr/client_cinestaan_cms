class MessagesController < ApplicationController

	include ApplicationHelper
	include MessagesHelper


	def get_tenant_messages
		status = true
		error = ""
        tenantid = params[:tenant_id]
		@bg = current_bg
		tenants = Tenant.find_by(:_id => tenantid)
		catalogs = Catalog.find_by(:bg_id => current_bg.id, :theme => "message", :tenant_id => tenantid)
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?region=IN&auth_token="+tenants.ott_auth_token+"&status=published"
		response = http_request("get",{},url,60)
		messagecontrolitems = response["body"]["data"]
		status = response["status"]
		error  = response["error"]

		if status
			if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
		page_size = params[:page_size] ? params[:page_size] : 25
		params[:page_size] = page_size
         if params[:title].present?
      		data_list =  messagecontrolitems["items"].delete_if { |h| h["type"] == "config_params" }.select {|k| k if k["title"].to_s.match(/#{params[:title]}/i)}
    		render json: {:items => data_list, :next_cnt => {},:psize => params[:page_size],:pge => page,:nxt => 0}
    	 else
    	 	data_list = Kaminari.paginate_array(messagecontrolitems["items"].delete_if { |h| h["type"] == "config_params" }).page(page).per(page_size)
        	next_items = Kaminari.paginate_array(messagecontrolitems["items"].delete_if { |h| h["type"] == "config_params" }).page(next_page).per(page_size)
    	 
        # render json: {:items => data_list, :total_count => data_list.total_count, :total_pages => data_list.total_pages}
        nxt_ims = next_items.count 
        items_cnt = data_list.count
        @nxt_items = next_items.count
        render json: {:items => data_list, :next_cnt => next_items,:psize => params[:page_size],:pge => page,:nxt => nxt_ims,:icount => items_cnt}
        end
        else
        render :json => {:error => error}.to_json, :status => 500
        end
	end

	def message_edit
		status = true
		error = ""
		msid = params[:msid]
		@tid =  params[:tid]
		@bg = current_bg
		tenants = Tenant.find_by(:_id => @tid)
		catalogs = Catalog.find_by(:bg_id => current_bg.id, :theme => "message", :tenant_id => @tid)
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+msid+"?region=IN&auth_token="+tenants.ott_auth_token+"&status=published"
		response = http_request("get",{},url,60)
		@message_edit = response["body"]["data"]
		status = response["status"]
		error  = response["error"]

		render "message/edit_messages"
	end

	def message_update
		status = true
		error = ""
		@tenant = params[:tenant_id]
		tenants = Tenant.find_by(:_id => @tenant)
		@bg = current_bg
		if status
			message_items = message_items(@bg,tenants,params)
			status = message_items["status"]
			error = message_items["error"]
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end

	def config_edit
		@bg = current_bg
		@tenants = Tenant.where(:bg_id => current_bg.id).reverse

		# if @tenants.count == 1
			catalog = Catalog.find_by(:bg_id => current_bg.id, :theme => "message", :tenant_id => @tenants.last.id.to_s)
			@catalog = catalog
			if catalog
				url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/items?region=IN&auth_token="+@tenants.last.ott_auth_token+"&status=published"
				response = http_request("get",{},url,60)
				if response["code"] == 200
					config_params = response["body"]["data"]["items"].select {|h| h["platform_code"]== 5000}.last
					url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/items/"+ config_params["content_id"] +"?region=IN&auth_token="+@tenants.last.ott_auth_token+"&status=published"
				    response = http_request("get",{},url,60)
				    @config_details = response["body"]["data"]
				    @tenant = @tenants.last.as_json(:except => [:created_at,:updated_at])
				    s3uploadremovspace = @bg.name + "/logos"
    				s3upload = s3uploadremovspace.gsub(" ", "_");
	    			@signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)
				end
			end
		# end			
		render "message/config_edit"
	end

	def get_tenant_config
		tenant = Tenant.find_by(:id => params[:id])
		catalog = Catalog.find_by(:bg_id => current_bg.id.to_s, :theme => "message", :tenant_id => tenant.id.to_s)
		url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/items?region=IN&auth_token="+tenant.ott_auth_token+"&status=published"
		response = http_request("get",{},url,60)
		if response["code"] == 200
			config_params = response["body"]["data"]["items"].select {|h| h["platform_code"]== 5000}.last
			url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/items/"+ config_params["content_id"] +"?region=IN&auth_token="+tenant.ott_auth_token+"&status=published"
			response = http_request("get",{},url,60)
			@config_details = response["body"]["data"]
			@tenant = tenant.as_json(:except => [:created_at,:updated_at])
		end	
	    render partial: "/message/config_details"
		# if status
		# 	render json: {:item => @config_details} , status: :ok
		# else
		# 	render :json => {:error => error}.to_json, :status => 500
		# end

	end	

	def update_config
		params[:tenant] = eval(params[:tenant])
		params[:cms_config] = eval(params[:cms_config])
		params[:bg] = current_bg
		update_at_ott(params)
		render json: {} , status: :ok
	end	

	def view_all
		@bg = current_bg
		@all_tenants = Tenant.where(:bg_id => @bg.id).order_by(:created_at => 'desc')
		if @all_tenants.count == 1
			@all_tenants.each do |tent|
				@tid = tent.id
			end
		else
			@all_tenants
		end
        if params[:page_size].present? 
        	@page_size = params[:page_size]
		end
        

        if params[:tenant_id].present? || params[:title].present?
			status = true
		error = ""
        tenantid = params[:tenant_id]
		tenants = Tenant.find_by(:_id => tenantid)
		catalogs = Catalog.find_by(:bg_id => current_bg.id, :theme => "message", :tenant_id => tenantid)
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?region=IN&auth_token="+tenants.ott_auth_token+"&status=published"
		response = http_request("get",{},url,60)
		messagecontrolitems = response["body"]["data"]
		status = response["status"]
		error  = response["error"]

		if status
			@bg = current_bg
			if params[:page].nil?
				page = 1
				next_page = page + 1
				params[:page] = page
			else
				page = params[:page]
				next_page = page.to_i + 1
			end

		page_size = params[:page_size] ? params[:page_size] : 25
		params[:page_size] = page_size
         if params[:title].present?
	      data_list =  messagecontrolitems["items"].delete_if { |h| h["type"] == "config_params" }.select {|k| k if k["title"].to_s.match(/#{params[:title]}/i)}
  		else
  			data_list = Kaminari.paginate_array(messagecontrolitems["items"].delete_if { |h| h["type"] == "config_params" }).page(page).per(page_size)
        	next_items = Kaminari.paginate_array(messagecontrolitems["items"].delete_if { |h| h["type"] == "config_params" }).page(next_page).per(page_size)
    	 	 @nxt_items = next_items.count
    	 	 nxt_ims = next_items.empty?
    	 end
        	@page = page
			@page_size = params[:page_size]
			@title = params[:title]
        end	
end

		render "message/view_all"
	end

end
