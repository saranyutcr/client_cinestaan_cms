class Profile
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :video_bitrate, type: String
  field :audio_bitrate, type: String
  field :container, type: String
  field :video_codec, type: String
  field :audio_codec, type: String
  field :height, type: Integer
  field :width, type: Integer
  field :bg_id, type: String
  field :is_system_profile, type: Boolean
end
