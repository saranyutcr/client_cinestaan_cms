class PublishSettings
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name, type: String
  field :profiles, type: Array
  field :protocols, type: Array
  field :cdn, type: Array
  field :bg_id, type: String
  field :naming_tag_id, type: String
  field :splice, type: Hash #{:start_at_second=>0,:end_at_second=>60}
  field :has_video_tile, type: Boolean
  field :logo, type: Hash #{"file_url"=>"", "width"=>"", "height"=>"", "x_offset"=>"", "y_offset"=>""}
  field :naming_tag, type: Hash
end