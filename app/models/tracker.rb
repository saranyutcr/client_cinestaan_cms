class Tracker
  include Mongoid::Document

  field :title, type: String
  field :tracker_type, type: String
  field :description, type: String

end
