class Tenant
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :bg_id, type: String
  field :ott_id, type: String
  field :ott_auth_token, type: String

  before_destroy :delete_dependents

  def delete_dependents
  	#deletes own apps
    App.where(:tenant_id=>self.id).each do |app|
        app.destroy
    end
  end

  def get_tenants_by_bg(bg_id)
    Tenant.where(:bg_id => bg_id)
  end  

  def get_tenants_by_id(ids)
    Tenant.any_in(:id => ids)
  end  
end