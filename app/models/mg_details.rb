class MgDetails
 	include Mongoid::Document

  	field :content_owner_id, type: String
    field :mg_paid, type: Float
end
