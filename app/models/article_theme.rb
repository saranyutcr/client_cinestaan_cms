class ArticleTheme
  include Mongoid::Document

  include ArticlesHelper
  include ApplicationHelper
  include HttpRequest

  field :title, type: String
  field :ott_catalog_id, type: String
  field :catalog_id, type: String
  field :ott_status, type: String
  field :ott_id, type: String
  field :ott_publish_date,type: DateTime
  field :ott_unpublish_date, type: DateTime
  field :ott_publish_req_job_id, type: String
  field :ott_unpublish_req_job_id, type: String
  field :bg_id, type: String
  field :app_ids, type: Array
  field :tenant_ids, type: Array

  def add_article_in_publish_queue(metadata)
    self.ott_publish_req_job_id = self.delay(:queue => "#{Rails.env}_publish_cms",:run_at => self.ott_publish_date).publish_at_ott.id
  end

  def add_article_in_unpublish_queue(metadata)
    self.ott_unpublish_req_job_id = self.delay(:queue => "#{Rails.env}_unpublish_cms",:run_at => self.ott_unpublish_date).unpublish_at_ott.id
  end

  def get_by_id(cms_id)
    ArticleTheme.find_by(:id => cms_id)
  end

  def get_data_by_ott_id(ott_id)
    ArticleTheme.find_by(:ott_id => ott_id)
  end  

  def create_at_ott(params)
  	maxtime = 60
    body = {}
    status = true
    metadata = create_ott_object(params)
    body["auth_token"] = params[:bg_obj].ott_auth_token
    body["content"] = metadata
    url = "http://"+get_ott_base_url+"/catalogs/"+params[:cms_catalog_obj].ott_id+"/items"
    response = http_request("post",body,url,maxtime)
    if response["code"] == 200
    	params[:ott_response] = response["body"]["data"]
    	create_at_cms(params)
    else
      status = false
      if response["body"].has_key? "error"
        error = response["body"]["error"]["message"]
      else
        error = response["body"]["errors"]
      end 	
    end	
    return true,error
  end

  def update_at_ott(params)
    maxtime = 60
    body = {}
    status = true
    metadata = create_ott_object(params)
    body["auth_token"] = params[:bg_obj].ott_auth_token
    body["content"] = metadata
    url = "http://"+get_ott_base_url+"/catalogs/"+params[:cms_catalog_obj].ott_id+"/items/"+self.ott_id
    response = http_request("put",body,url,maxtime)
    if response["code"] == 200
      params[:ott_response] = response["body"]["data"]
      self.update_at_cms(params)
    else
      status = false
      if response["body"].has_key? "error"
        error = response["body"]["error"]["message"]
      else
        error = response["body"]["errors"]
      end   
    end 
    return true,error
  end  

  def update_at_cms(params)
    self.ott_status = params[:ott_response][:status]
    self.ott_publish_date = params[:utc_published_date]
    self.ott_unpublish_date = params[:utc_unpublished_date]
    self.app_ids = params[:apps]
    tenants = []
    params[:apps].each do |app|
      tenant = App.find(app).tenant_id
      tenants << tenant
    end
    self.tenant_ids = tenants.uniq

    if self.ott_publish_req_job_id
      Delayed::Job.where(:id => self.ott_publish_req_job_id).delete_all
      self.ott_publish_req_job_id = nil
    end 

    if self.ott_unpublish_req_job_id
      Delayed::Job.where(:id => self.ott_unpublish_req_job_id).delete_all
      self.ott_unpublish_req_job_id = nil
    end

    if !params["published_date"].blank? && params["status"] != "published"
      self.add_article_in_publish_queue(params)
    end
    if !params["unpublished_date"].blank?
      self.add_article_in_unpublish_queue(params)
    end

    self.save
  end

  def create_at_cms(params)
  	article = ArticleTheme.new
  	article.title = params[:title]
  	article.ott_catalog_id = params[:cms_catalog_obj].ott_id
  	article.catalog_id = params[:catalog_id]
  	article.ott_status = params[:ott_response][:status]
  	article.ott_id = params[:ott_response][:article_id]
  	article.ott_publish_date = params[:utc_published_date]
  	article.ott_unpublish_date = params[:utc_unpublished_date]
  	article.bg_id = params[:bg_obj].id
  	article.app_ids = params[:apps]
  	tenants = []
  	params[:apps].each do |app|
  		tenant = App.find(app).tenant_id
  		tenants << tenant
  	end
  	article.tenant_ids = tenants.uniq
  	article.save

  	if !params["published_date"].blank? && params["status"] != "published"
   		article.add_article_in_publish_queue(params)
   	end
   	if !params["unpublished_date"].blank?
   		article.add_article_in_unpublish_queue(params)
   	end
   	article.save
  end	

  def create_ott_object(params)
  	metadata = {}
  	metadata["title"] = params[:title]
		if params[:regions]
			metadata["regions"] = params[:regions].split(",").map(&:strip)
		else
			metadata["regions"] = ["IN"]
		end
		metadata["app_ids"] = []
		tenants = []
		params[:apps] = eval(params[:apps]) if params[:apps].class == String
		params[:apps].each do |a|
		  app = App.find(a)
		  metadata["app_ids"] << app.ott_id
		  tenants << app.tenant_id
		end	
		tenants = tenants.uniq
		
		metadata["tenant_ids"] = []
		tenants.each do |t|
			tenant = Tenant.find(t)
			metadata["tenant_ids"] << tenant.ott_id
		end

		metadata["business_group_id"] = params[:bg_obj].ott_id

		languages = params[:language]
		lang = languages.first
		metadata["language"] = lang

		if params["genres"].class == String
	       uniqgenres = eval(params[:genres])
	       metadata["genres"] = uniqgenres.uniq
	    else  
	       uniqgenres = params[:genres]
	       metadata["genres"] = uniqgenres.uniq
		end	
		metadata["description"] = params[:description]
		metadata["rating"] = params[:rating].to_i
		
		metadata["asset_id"] = params[:asset_id] if params[:asset_id]
		metadata["sequence_no"] = params[:sequence_no] if params[:sequence_no] 

		custom_fields = get_custom_fields(params)
		metadata["custom_fields"] = custom_fields if !custom_fields.empty?
		status = params[:status]

		if !params[:utc_published_date].blank?
		    metadata["published_date"] = params[:utc_published_date]
		    if status == "published"
		    	status = "unpublished"
		    end	
		else
			metadata["published_date"] = ""     
		end
		
		if !params[:utc_unpublished_date].blank?
			metadata["unpublished_date"] = params[:utc_unpublished_date]
		else
			metadata["unpublished_date"] = "" 
		end	

		metadata["status"] = status
		metadata["short_description"] = params[:short_description] if params[:short_description]
		metadata["keywords"] = params[:keywords] if params[:keywords]
		metadata["release_date_string"] = params[:release_date_string] if params[:release_date_string]
		if params.has_key? "people"
        obj = create_people_object(params)
       	metadata["people"] = obj if !obj.empty?
    end	

    if params.has_key? "access_control_id"
    	acobj = update_access_control(params)
    	metadata["behavior"] = acobj if !acobj.empty?
		end
		return metadata
  end

  def publish_at_ott
    status = true
    error = ""
    maxtime = 60
    body = {}
    bg = BusinessGroup.find(self.bg_id)
    catalog = catalog_object.get_catalog_by_ott_id(self.ott_catalog_id)

    url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"    
    
    response = http_request("get",{},url,maxtime)
    if response["code"] == 200
      body["auth_token"] = bg.ott_auth_token
      resp_data =  response["body"]["data"]
      body["content"] = {}
      body["content"]["status"] = "published"
      body["content"]["title"] = self.title
      body["content"]["app_ids"] = resp_data["app_ids"]
      body["content"]["tenant_ids"] = resp_data["tenant_ids"]
      body["content"]["business_group_id"] = bg.ott_id
      body["content"]["regions"] = ["IN"] # hardcoded value
      body["content"]["description"] = resp_data["description"]
      body["content"]["short_description"] = resp_data["short_description"]
      body["content"]["release_date_string"] = resp_data["release_date_string"]
      body["content"]["genres"] = resp_data["genres"]
      body["content"]["language"] = resp_data["language"]
      body["content"]["rating"] = resp_data["rating"]
      body["content"]["asset_id"] = resp_data["asset_id"]
      body["content"]["keywords"] = resp_data["keywords"]
      body["content"]["custom_fields"] = resp_data["custom_fields"]

      ott_play_url = resp_data["play_url"]   

      response = http_request("put",body,url,maxtime)
      if response["code"] != 200
      status = false
      error = response["body"]["error"]["message"]
      # error = "Could not publish at OTT!"
      else
          self.ott_status = response["body"]["data"]["status"]
          self.save
      end
    else
        status = false
        error = "Could not publish at OTT!"     
    end    

    return status,error
  end

  def unpublish_at_ott
    status = true
    error = ""
    maxtime = 60
    body = {}
    bg = BusinessGroup.find(self.bg_id)
    catalog = catalog_object.get_catalog_by_ott_id(self.ott_catalog_id)

    url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"    
    
    response = http_request("get",{},url,maxtime)
    if response["code"] == 200
      body["auth_token"] = bg.ott_auth_token
      resp_data =  response["body"]["data"]
      body["content"] = {}
      body["content"]["status"] = "unpublished"
      body["content"]["title"] = self.title
      body["content"]["app_ids"] = resp_data["app_ids"]
      body["content"]["tenant_ids"] = resp_data["tenant_ids"]
      body["content"]["business_group_id"] = bg.ott_id
      body["content"]["regions"] = ["IN"] # hardcoded value
      body["content"]["description"] = resp_data["description"]
      body["content"]["short_description"] = resp_data["short_description"]
      body["content"]["release_date_string"] = resp_data["release_date_string"]
      body["content"]["genres"] = resp_data["genres"]
      body["content"]["language"] = resp_data["language"]
      body["content"]["rating"] = resp_data["rating"]
      body["content"]["asset_id"] = resp_data["asset_id"]
      body["content"]["keywords"] = resp_data["keywords"]
      body["content"]["custom_fields"] = resp_data["custom_fields"]

      ott_play_url = resp_data["play_url"]   

      response = http_request("put",body,url,maxtime)
      if response["code"] != 200
      status = false
      error = response["body"]["error"]["message"]
      # error = "Could not publish at OTT!"
      else
          self.ott_status = response["body"]["data"]["status"]
          self.save
      end
    else
        status = false
        error = "Could not publish at OTT!"     
    end    

    return status,error
  end  

  def delete_at_ott
    status = true
    error = ""
    maxtime = 60
    body = {}
    bg = BusinessGroup.find(self.bg_id)
    body["auth_token"] = bg.ott_auth_token
    catalog = catalog_object.get_catalog_by_ott_id(self.ott_catalog_id)

    url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items/"+self.ott_id
    
    response = http_request("delete",body,url,maxtime)
    if response["code"] != 200 
        status = false
        error = "Could not delete at OTT!"
    end
    return status,error
  end  

  def catalog_object
    Catalog.new
  end  
  	
end
