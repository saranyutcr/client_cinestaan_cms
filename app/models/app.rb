class App
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :platform, type: String #android,windows,ios,web,wap
  field :bg_id, type: String
  field :tenant_id, type: String
  field :ott_id, type: String
  field :ott_auth_token, type: String

  before_destroy :delete_dependents

  def delete_dependents
  	#deletes own apps
    Catalog.where(:app_id=>self.id).each do |catalog|
        catalog.destroy
    end
  end

  def get_apps_by_bg(bg_id)
    App.where(:bg_id => bg_id)
  end  

  def get_apps_by_id(ids)
    App.any_in(:id => ids)
  end  

  def get_apps_by_ott_id(ott_ids)
    App.any_in(:ott_id => ott_ids)
  end
    
end