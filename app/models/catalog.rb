class Catalog
  include Mongoid::Document
  include Mongoid::Timestamps

  include HttpRequest
  include ApplicationHelper

  field :name, type: String
  field :app_id, type: Array
  field :theme, type: String
  field :ott_id, type: String
  field :bg_id, type: String
  field :tenant_id, type: Array
  field :layout_type, type: String
  
  def get_catalog_by_ott_id(ott_id)
  	Catalog.find_by(:ott_id => ott_id)
  end	

  def get_catalog_by_id(id)
    Catalog.find_by(:id => id)
  end  

  def get_catalogs_by_bg(params)
    Catalog.where(:bg_id => params[:bg_object].id)
  end  
 
  def delete_at_ott
    status = true
    error = ""
    ott_id = self.ott_id
    bg = BusinessGroup.find(self.bg_id)
    
    url = "http://"+get_ott_base_url+"/catalogs/"+ self.ott_id + "?auth_token=" + bg.ott_auth_token
    response = http_request("delete",{},url,60)
    if !response["status"]
      status = response["status"]
      error = response["error"]
    end
    return status,error
  end  

  def delete_at_cms
    if self.delete
      status = true
      error = ""
      CatalogList.where(:catalog_id => self.id).each do |list|
        if (list.tags.count == 1)  && (list.tags.include? "catalog")
          list.delete
        else
          list.tags = list.tags.delete_if { |e| e== catalog_id }
          list.save  
        end 
      end  
    else
      status = false
      error = "Can't delete Catalog from CMS!"
    end 
    return status,error
  end  

end
