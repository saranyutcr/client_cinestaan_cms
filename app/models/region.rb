class Region
  include Mongoid::Document
  include Mongoid::Timestamps

  validates :name, :presence => true
  validates :iso_code, :presence => true, :length => { :is => 2 }

  #Fileds
  field :name, type: String
  field :iso_code, type: String
end
