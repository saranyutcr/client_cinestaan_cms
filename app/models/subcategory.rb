class Subcategory
	include Mongoid::Document
	include Mongoid::Timestamps
	include HttpRequest
	include ApplicationHelper

	field :title, type: String
	field :ott_status, type: String
	field :ott_id, type: String
	field :app_ids, type: Array
	field :tenant_ids, type: Array
	field :bg_id, type: String
	field :catalog_id, type: String
	field :ott_catalog_id, type: String
	field :subcategories_flag, type: String
	field :episodes_flag, type: String
	field :show_id, type: String
	field :ott_show_id, type: String
	field :parent_subcategory_id, type: String
	field :ott_parent_subcategory_id, type: String

	def get_subcategories_by_parent_id(parent_id)
	    Subcategory.where(:parent_subcategory_id => parent_id)
	end

	def get_subcategory_by_ott_id(ott_id)
		Subcategory.find_by(:ott_id => ott_id)
	end	

	def get_subcategories_by_bg(bg_obj)
		Subcategory.where(:bg_id => bg_obj.id)
	end	

	def get_subcategory_by_id(sub_id)
		Subcategory.find_by(:id => sub_id)
	end	

	def get_subcategories_by_show_id(show_id)
    	Subcategory.where(:show_id => show_id).and(:parent_subcategory_id => "show")
	end	

	def get_subcategories_by_show_ott_id(show_id)
		Subcategory.where(:ott_show_id => show_id).and(:parent_subcategory_id => "show")
	end	

	def get_subcategories_by_id(sub_id)
		Subcategory.where(:parent_subcategory_id => sub_id)
	end	

	def create_at_ott(params)
		error = ""
		status = true
		subcategory_obj = create_subcategory_object(params)

		if params["type"] == "show"
		  url = "http://" + get_ott_base_url + \
		        "/catalogs/shows/" + params["ott_show_object"]["content_id"] + \
		        "/subcategories"
		else
			url = "http://" + get_ott_base_url + \
						"/catalogs/shows/subcategories/" + params["ott_subcategory_object"]["content_id"] + \
						"/subcategories"
		end	
		response = http_request("post",subcategory_obj,url,60)
		if response["code"] == 200
			params["response_object"] = response["body"]["data"]
			save_at_cms(params)
		else
		  error = response["body"]["error"]["message"]	
		  status = false
		end
		return status,error
	end	

	def update_at_ott(params)
		error = ""
		status = true
		subcategory_obj = create_subcategory_object(params)

		if params["type"] == "show"
		    url = "http://" + get_ott_base_url + \
		        "/catalogs/shows/" + params["ott_show_object"]["content_id"] + \
		        "/subcategories/" + params["sub_object"]["content_id"]
		 #    subcategory_obj["content"]["has_subcategories"] = params["sub_object"]["subcategory_flag"]
			# subcategory_obj["content"]["has_episodes"] = params["sub_object"]["episode_flag"]
		else
			url = "http://" + get_ott_base_url + \
						"/catalogs/shows/subcategories/" + params["ott_subcategory_object"]["content_id"] + \
						"/subcategories/" + params["sub_object"]["content_id"]
			# subcategory_obj["content"]["has_subcategories"] = params["sub_object"]["subcategory_flag"]
			# subcategory_obj["content"]["has_episodes"] = params["sub_object"]["episode_flag"]			
		end	
		response = http_request("put",subcategory_obj,url,60)
		if response["code"] == 200
			params["response_object"] = response["body"]["data"]
			update_at_cms(params)
		else
		  error = response["body"]["error"]["message"]	
		  status = false
		end
		return status,error
	end	

	def update_at_cms(params)
		sub = get_subcategory_by_ott_id(params["response_object"]["subcategory_id"])
		sub.title = params["name"]
		sub.ott_status = params["response_object"]["status"]
		sub.ott_id = params["response_object"]["subcategory_id"]
		sub.subcategories_flag = params["subcategories_flag"]
		sub.episodes_flag = params["episodes_flag"]
		sub.ott_show_id = params["response_object"]["show_theme_id"]
		sub.ott_parent_subcategory_id = params["response_object"]["nested_subcategory_id"]
		sub.bg_id = params["bg_object"]["_id"]

		if params["response_object"]["nested_subcategory_id"] == "show"
		  	sub.parent_subcategory_id = "show"
	  	else
	  		subcategory = get_subcategory_by_ott_id(params["response_object"]["nested_subcategory_id"])
	  		sub.parent_subcategory_id = subcategory.id
	  	end	

	    if params["type"] == "show"
			sub.catalog_id = params["cms_show_object"]["catalog_id"]
			sub.app_ids = []
			sub.app_ids = params["cms_show_object"]["app_ids"]
			sub.tenant_ids = []
			sub.tenant_ids = params["cms_show_object"]["tenant_ids"]
			sub.show_id = params["cms_show_object"]["_id"]
			sub.ott_catalog_id = params["ott_show_object"]["catalog_id"]
		else
			sub.catalog_id = params["cms_subcategory_object"]["catalog_id"]
			sub.app_ids = []
			sub.app_ids = params["cms_subcategory_object"]["app_ids"]
			sub.tenant_ids = []
			sub.tenant_ids = params["cms_subcategory_object"]["tenant_ids"]
			sub.show_id = params["cms_subcategory_object"]["show_id"]
			sub.ott_catalog_id = params["ott_subcategory_object"]["catalog_id"]
		end	
		sub.save
	end	

	def save_at_cms(params) 
		sub = Subcategory.new
		sub.title = params["name"]
		sub.ott_status = params["response_object"]["status"]
		sub.ott_id = params["response_object"]["subcategory_id"]
		sub.subcategories_flag = params["subcategories_flag"]
		sub.episodes_flag = params["episodes_flag"]
		sub.ott_show_id = params["response_object"]["show_theme_id"]
		sub.ott_parent_subcategory_id = params["response_object"]["nested_subcategory_id"]
		sub.bg_id = params["bg_object"]["_id"]

		if params["response_object"]["nested_subcategory_id"] == "show"
		  	sub.parent_subcategory_id = "show"
	  	else
	  		subcategory = get_subcategory_by_ott_id(params["response_object"]["nested_subcategory_id"])
	  		sub.parent_subcategory_id = subcategory.id
	  	end	

	  if params["type"] == "show"
			sub.catalog_id = params["cms_show_object"]["catalog_id"]
			sub.app_ids = []
			sub.app_ids = params["cms_show_object"]["app_ids"]
			sub.tenant_ids = []
			sub.tenant_ids = params["cms_show_object"]["tenant_ids"]
			sub.show_id = params["cms_show_object"]["_id"]
			sub.ott_catalog_id = params["ott_show_object"]["catalog_id"]
		else
			sub.catalog_id = params["cms_subcategory_object"]["catalog_id"]
			sub.app_ids = []
			sub.app_ids = params["cms_subcategory_object"]["app_ids"]
			sub.tenant_ids = []
			sub.tenant_ids = params["cms_subcategory_object"]["tenant_ids"]
			sub.show_id = params["cms_subcategory_object"]["show_id"]
			sub.ott_catalog_id = params["ott_subcategory_object"]["catalog_id"]
		end	
		sub.save
	end



	def create_subcategory_object(params)
    body = {}
		body["auth_token"] = params["bg_object"]["ott_auth_token"]
		body["content"] = {}
		body["content"]["title"] = params["name"]
		body["content"]["status"] = params["status"]
		body["content"]["l_listing_image"] = params["l_listing_image"]
		body["content"]["p_listing_image"] = params["p_listing_image"]
		body["content"]["featured_image"] = params["featured_image"]
		body["content"]["business_group_id"] =  params["bg_object"]["ott_id"]
		body["content"]["regions"] = ["IN"]
		body["content"]["has_subcategories"] = params["subcategories_flag"] if params["subcategories_flag"]
		body["content"]["has_episodes"] = params["episodes_flag"] if params["episodes_flag"]
		body["content"]["description"] = params["description"]
		body["content"]["sequence_no"] = params["sequence_no"]
		if params["type"] == "show"
		  body["content"]["app_ids"] = params["ott_show_object"]["app_ids"]
		  body["content"]["tenant_ids"] = params["ott_show_object"]["tenant_ids"]
		else
		  body["content"]["app_ids"] = params["ott_subcategory_object"]["app_ids"]
		  body["content"]["tenant_ids"] = params["ott_subcategory_object"]["tenant_ids"] 
	  	end
	  	body["content"]["meta_title"] = params["meta_title"] if !params["meta_title"].blank?
		body["content"]["meta_keywords"] = params["meta_keywords"] if !params["meta_keywords"].blank?
		body["content"]["meta_description"] = params["meta_description"] if !params["meta_description"].blank?
		get_episodetypes = build_episode_type_hash(params)
		body["content"]["episodetype_tags"] = get_episodetypes

		return body
	end	

	def delete_subcategory_from_ott
		ott_id = self.ott_id
		bg = BusinessGroup.find(self.bg_id)
		url =  "http://"+ get_ott_base_url + "/catalogs/shows/subcategories/" + ott_id +"/?auth_token=" + bg.ott_auth_token 
		response = http_request("delete",{},url,60)
		if response["code"] == 200
			status = true
			error = ""
		else
		  error = response["body"]["error"]["message"]	
		  status = false
		end
		return status,error
	end	

	def delete_subcategory_from_cms
		if self.delete
			status = true
			error = ""
		else
			status = false
			error = "Can't delete Subcategory from CMS!"
		end	
		return status,error
	end
	def build_episode_type_hash(params)
	  	episodetype_tag = []
		if params["episodetype_tags"]
		    params["episodetype_tags"].each do |ett|
		      	et_tags = ett.split('-')
		      	name = et_tags[0]
		      	display_title = et_tags[1]
		        h = { "name" => name , "display_title" => display_title }
		        episodetype_tag << h
		    end   
	    end
	    return episodetype_tag
	end	
end
