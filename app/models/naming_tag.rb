class NamingTag
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :bg_id, type: String
  field :constructor, type: Array
  field :replacer, type: Array #array of hashes: [{input=>"",output=>""},{input=>"",output=>""}]
end