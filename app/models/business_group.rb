class BusinessGroup
    include Mongoid::Document
    include Mongoid::Timestamps
    include CloudFrontConfig
    include HttpRequest
    include ApplicationHelper

    field :name, type: String
    field :email, type: String
    field :tx_base_url, type: String
    field :tx_auth_token, type: String
    field :ott_id, type: String
    field :ott_auth_token, type: String
    field :admin_base_url, type: String
    field :plan, type: String #ott,transcoding
    field :manual_smart_url, type: Boolean
    field :friendly_id, type: String
    field :bucket_name, type: String
    field :domain_name, type: String  #streaming domain name/distribution name
    field :cloud_front_message, type: String
    field :tag, type: String
    field :storage_domain_name, type: String
    field :distribution_id, type: String
    field :storage_tag, type: String
    field :source_tag, type: String
    field :cdn_name, type: String
    field :aws_access_key_image, type: String
    field :aws_secret_image, type: String

    field :staging_webview #will not be editable
    field :prod_webview    #editable- by default prod and preprod will point to same URL
    field :preprod_webview #will not be editable
    field :data_upload, type: Array #values allowed bulk,bridge
    field :bridge_base_url, type: String

    before_destroy :delete_dependents

    def delete_dependents
        #deletes own users
        User.where(:bg_id=>self.id).each do |user|
            user.destroy
        end
        #deletes own tenants
        Tenant.where(:bg_id=>self.id).each do |tenant|
            tenant.destroy
        end
        #media
        Media.where(:bg_id=>self.id).each do |media|
            media.destroy
        end
        #naming_tag
        NamingTag.where(:bg_id=>self.id).each do |naming_tag|
            naming_tag.destroy
        end
        #profile
        Profile.where(:bg_id=>self.id).each do |profile|
            profile.destroy
        end
        #publish_settings
        PublishSettings.where(:bg_id=>self.id).each do |ps|
            ps.destroy
        end
        #catalog
        Catalog.where(:bg_id=>self.id).each do |catalog|
            catalog.destroy
        end
    end
    def monthly_tx_stats
        stats = {}
        months = []
        created = []
        published = []
        for i in 0..5
            t = Time.now-i.months
            months << Date::MONTHNAMES[(Time.now-i.months).month]
            published << Media.all_of(:bg_id => self.id,:created_at => t.beginning_of_month..t.end_of_month,:status=>"Published").count
            created << Media.all_of(:bg_id => self.id,:created_at => t.beginning_of_month..t.end_of_month).count
        end
        stats["months"] = months.reverse
        stats["created"] = created.reverse
        stats["published"] = published.reverse
        return stats
    end

    def create_distribution
        begin
        business_group = self.name
        bucket_name = "#{business_group}-#{Time.now.to_i}".gsub(" ","-").downcase
        region = "ap-southeast-1"
        access_key = "AKIAIBQGVPLT4QQ4SFJA"
        access_secret_key = "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"
        bucket = create_bucket region, access_key, access_secret_key, bucket_name
        # puts "sleeping for 5 sec"
        # sleep(5)

        if bucket
          puts "bucket_name : #{bucket} is created"
          status = add_crossdomain_to_bucket region, access_key, access_secret_key, bucket_name
          if status 
            self.cloud_front_message = "crossdomain.xml added to bucket : #{bucket_name}"
            status = add_cors_configs region, access_key, access_secret_key, bucket_name
            if status
              self.cloud_front_message = "added cors config to bucket : #{bucket_name}"
            else
              self.cloud_front_message = "failed to add cors config to bucket : #{bucket_name}"  
            end  
          else
            self.cloud_front_message = "failed to add crossdomain.xml for bucket : #{bucket_name}"
          end
          origin_access = create_origin_access_identity region, access_key, access_secret_key, business_group
          puts "sleeping for 10 sec"
          sleep(10)
          if origin_access
            cloudfront = create_cloudfront_distribution region, access_key, access_secret_key, business_group, bucket_name, origin_access
            # puts "sleeping for 20 sec"
            sleep(10)
            #cloudfront = true
            if cloudfront
              bucket_policy = create_bucket_policy_with_origin_identity region, access_key, access_secret_key, business_group, bucket_name, origin_access
              # puts "sleeping for 10 sec"
              sleep(10)
              if bucket_policy
                puts cloudfront.inspect
                puts "domain_name : #{cloudfront.distribution.domain_name}"
                  self.domain_name = cloudfront.distribution.domain_name
                  self.distribution_id = cloudfront.distribution.id
                  self.bucket_name = bucket_name
                  # storage domain name required to create storage tag
                  self.storage_domain_name = bucket_name + ".s3.amazonaws.com"
                  
                  status,data = create_tag_for_s3_storage(self.storage_domain_name,self.bucket_name,self)
                  if status
                    self.storage_tag = data["tag"]

                    status,data = create_cdn(self)
                    if status
                      self.cdn_name = data["name"]
                      self.cloud_front_message = "success"
                    else
                      self.cloud_front_message = data["error"]    
                    end  
                  else  
                    self.cloud_front_message = data["error"]
                  end  
                # return true,cloudfront.distribution.domain_name
              else
                self.cloud_front_message = "bucket policy not created, please check exception message"
                #puts "bucket policy not created, please check exception message"
                # return false,"bucket policy not created, please check exception message"
              end
            else
                self.cloud_front_message = "cloudfront creating distribution failed, please check exception message"
              # puts "cloudfront creating distribution failed, please check exception message"
              # return false,"cloudfront creating distribution failed, please check exception message"
            end
          else
            self.cloud_front_message = "origin access identity not created, please check exception message"
            # puts "origin access identity not created, please check exception message"
            # return false,"origin access identity not created, please check exception message"
          end
        else
            self.cloud_front_message = "bucket not created, exiting from program. Please check exception message"
          # puts "bucket not created, exiting from program. Please check exception message"
          # return false,"bucket not created, exiting from program. Please check exception message"
        end
        self.save
        end

    end 
end
