module AddUserSubscriptionHelper

def usersubscriptions(params)
		status = true
        error = ""
        maxtime = 60

			body = {}
			apps = params[:app_ids]
			app = App.find_by(:ott_id => apps.first)
       	 	body["auth_token"] = app.ott_auth_token
       	 	body["us"] = "123456"
       	 	if params[:regions]
				body["region"] = params[:regions].split(",").map(&:strip)
			else
				body["region"] = "IN"
			end
        	body["payment_info"] = {}
			body["payment_info"]["amount"] = params[:pprice]
			body["payment_info"]["mode"] = "CMS"
			body["payment_info"]["price_charged"] = params[:price_charged]
			body["payment_info"]["coupon_code"] = ""

			start_date = DateTime.parse(params[:start_date]).strftime("%Y%m%d%H%M%S")

			body["payment_info"]["packs"] = []
			count = params[:sub_plans].count
        	for i in 0..count-1
			 plansdata = {}
			 plansdata["plan_id"] = params[:sub_plans][i]
			 plansdata["category"] = params[:ptype]
			 plansdata["catalog_id"] = ""
			 plansdata["content_ids"] = params[:content_id]
			 plansdata["start_date"] = start_date
			 body["payment_info"]["packs"] << plansdata
			end

			body["transaction_info"] = {}
			body["transaction_info"]["txn_id"] = "12344"
			body["transaction_info"]["txn_message"] = "CMS"
			body["transaction_info"]["txn_status"] = "success"
			body["transaction_info"]["order_id"] = ""
			body["transaction_info"]["partner"] = "CMS"

			body["user_info"] = {}
			body["user_info"]["email"] = params[:email]
			body["user_info"]["mobilenumber"] = params[:mobilenumber]
			body["user_info"]["device"] = "CMS"
			body["user_info"]["miscellaneous"] = {}

			body["user_id"] = params[:email]
			
		  url = "http://"+get_ott_base_url+"/admin/transactions?auth_token="+app.ott_auth_token  
          response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
            #error = response["body"]["error"]["message"]
    	end

        return response
	 end

end
