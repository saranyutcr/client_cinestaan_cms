module PeopleHelper  
	def create_at_ott(params)
		person = create_person_object(params)
		catalog_cms_id = params["catalog_id"].split("-")[0]
		catalog_id = (catalog_object.get_catalog_by_id(catalog_cms_id)).ott_id
		url = "http://" + get_ott_base_url + "/catalogs/" + catalog_id + "/items"
		response = http_request("post",person,url,60)
		if response["code"] == 200
			return true,response
		else	
			return false,response["body"]["error"]["message"]
		end	
	end	

	def get_people_from_ott params

		catalog_cms_id = params["catalog_id"]#.split("-")[0]
		catalog_id = (catalog_object.get_catalog_by_id(catalog_cms_id)).ott_id	
		ps = 1000
		url = "http://" + get_ott_base_url + "/catalogs/" + catalog_id + "/items?auth_token=" +  params["bg_obj"]["ott_auth_token"] + "&region=IN&page_size=" + "#{ps}"
		response = http_request("get",{},url,60)
		if response["code"] == 200
			return true,response
		else	
			return false,response["body"]["error"]["message"]
		end	


	end


	def update_people_at_ott(params)
		logger.debug "========from people helper===#{params}"
		person = create_person_object(params)
		# catalog_cms_id = params["catalog_id"]#.split("-")[0]
		# item_ott_id = params["item_id"]
		item_ott_id = params["id"]
		# catalog_id = (catalog_object.get_catalog_by_id(catalog_cms_id)).ott_id
		catalog_id = params["catalog_id"]
		url = "http://" + get_ott_base_url + "/catalogs/" + catalog_id + "/items/" + item_ott_id + "?auth_token=" + params["bg_obj"]["ott_auth_token"]
		logger.debug "url for ott ===#{url} people===#{person}"
		response = http_request("put",person,url,60)
		if response["code"] == 200
			return true,response
		else	
			return false,response["body"]["error"]["message"]
		end	
	end

	def create_person_object(params)
		body = {}
		body["auth_token"] = params["bg_obj"]["ott_auth_token"]
		body["content"] = {}
		body["content"]["status"] = params["status"]
		body["content"]["title"] = params["title"]
		body["content"]["description"] = params["description"]
		body["content"]["asset_id"] = params["asset_id"]
		body["content"]["address"] = params["address"]
		body["content"]["city"] = params["city"]
		body["content"]["state"] = params["state"]
		body["content"]["country"] = params["country"]
		body["content"]["pin_code"] = params["pin_code"]
		body["content"]["telephone_number"] = params["telephone_number"]
		body["content"]["mg_paid"] = params["mg_paid"]
		custom_fields = get_custom_fields(params)
		body["content"]["custom_fields"] = custom_fields if !custom_fields.empty?
		# body["content"]["l_listing_image"] = params["l_listing_image"] if params["l_listing_image"]
		if !params["p_listing_image"].blank?
			body["content"]["p_listing_image"] = params["p_listing_image"] 
		else
		    body["content"]["p_listing_image"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default+image.png"	
		end	
		# body["content"]["featured_image"] = params["featured_image"] if params["featured_image"]
		# apps = []
		app_objs = app_object.get_apps_by_id(params["apps"])
		ott_apps = (app_objs.as_json.map { |h| h["ott_id"] }).uniq
	    tenant_ids = (app_objs.as_json.map { |h| h["tenant_id"] }).uniq
		# tenant_objs = tenant_object.get_tenants_by_bg(params["bg_obj"]["_id"])
		ott_tenants = tenant_object.get_tenants_by_id(tenant_ids).as_json.map { |h| h["ott_id"] }.uniq
		# ott_tenants = (tenant_objs.as_json.map { |h| h["ott_id"] }).uniq
		
		body["content"]["app_ids"] = ott_apps
		body["content"]["tenant_ids"] = ott_tenants
		body["content"]["business_group_id"] = params["bg_obj"]["ott_id"]
		body["content"]["regions"] = ["IN"]
		return body
	end	

	

	def delete_at_ott(params)
		url = "http://" + get_ott_base_url + "/catalogs/"+ params[:catalog_id] + "/items/" + params[:id] + "?auth_token=" + params["bg_obj"].ott_auth_token
		response = http_request("delete",{},url,60)
		if response["code"] == 200
			return true,""
		else	
			return false,response["body"]["error"]["message"]
		end
	end

	def app_object
		App.new
	end	

	def tenant_object
		Tenant.new
	end	

	def catalog_object
		Catalog.new
	end
	def get_custom_fields(params)
  obj = {}
  if params[:tags]
    tags = params[:tags]
    tags = eval(params[:tags]) if params[:tags].kind_of? String
    tags.each do |hash|
      data = hash
      # data = JSON.parse(hash) if hash.kind_of? String  
      datatype = data["type"]
      case datatype
      when "String" 
        obj[data["tag"]] = !params[data["tag"]].blank? ? params[data["tag"]] : "" 
      when "Array"
        obj[data["tag"]] = !params[data["tag"]].blank? ? params[data["tag"]].split(',') : []
      when "Integer"
        obj[data["tag"]] = !params[data["tag"]].blank? ? check_valid_data(params[data["tag"]]) : -1
      when "DateTime" 
        obj[data["tag"]] = !params[data["tag"]].blank? ? params[data["tag"]] : "" 
      end      
    end 
  end 
  return obj
  end
end
