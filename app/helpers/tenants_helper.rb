module TenantsHelper
	def create_tenant(bg,name)
		status = true
		error = ""
		response = nil
		if status
			body = {}
			body["tenant_name"] = name
			body["auth_token"] = bg.ott_auth_token
			url = "http://"+get_ott_base_url+"/tenant"
			response = http_request("post",body,url,60)
			if !response["status"]
				status = false
				error = response["error"]
			end
		end
		if status
			tenant = Tenant.new
			tenant.name = name
			tenant.ott_id = response["body"]["data"]["tenant_id"]
			tenant.ott_auth_token = response["body"]["data"]["authentication_token"]
			tenant.bg_id = bg.id
			tenant.save
			return tenant
		else
			Rails.logger.debug error
			return nil
		end
	end
end
