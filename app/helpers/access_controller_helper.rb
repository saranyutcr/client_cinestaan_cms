module AccessControllerHelper

def create_access_controller(bg,catalogid,params)
	status = true

	metadata = {}
			metadata["title"] = params[:title].strip
			metadata["description"] = params[:description]
			metadata["is_default"] = false

			if params[:method] == "update"
				metadata["tenant_ids"] = []
				tenant_id = Tenant.find_by(:id => params[:tenants])
				metadata["tenant_ids"] << tenant_id.ott_id
			else
			metadata["tenant_ids"] = []
			params[:tenants].each do |tenant_ids|
				tenant_id = Tenant.find(tenant_ids)
				metadata["tenant_ids"] << tenant_id.ott_id
			end
			end
			
			if params[:isfree] == "yes"
				metadata["is_free"] = true
			else
				metadata["is_free"] = false
			end

			if params[:is_tvod] == "yes"
				metadata["is_tvod"] = true
			else
				metadata["is_tvod"] = false
			end
			if params[:adsavailable] == "yes"
				metadata["ads_available"] = true
			else
				metadata["ads_available"] = false
		    end
			if params[:loginrequired] == "yes"
				metadata["login_required"] = true
			else
				metadata["login_required"] = false
			end
			start_date = DateTime.parse(params[:start_date]).strftime("%Y%m%d%H%M%S")
			end_date = DateTime.parse(params[:end_date]).strftime("%Y%m%d%H%M%S")
			metadata["start_date"]=  start_date
			metadata["end_date"]= end_date
			
			 presetting = {}
			 presetting["mobile_ads_url"] = params[:premobileadd]
			 presetting["ads_url"] = params[:preadd]
			 if params[:preactive] == "yes"
			 	presetting["active"] = true
			 else
			 	presetting["active"] = false
			 end
			 presetting["type"] = params[:pretype]
			metadata['pre_role_settings'] = presetting

			 psetting = {}
			 psetting["mobile_ads_url"] = params[:postmobileadd]
			 psetting["ads_url"] = params[:postadd]
			 if params[:postactive] == "yes"
			 	psetting["active"] = true
			 else
			 	psetting["active"] = false
			 end
			 psetting["type"] = params[:posttype]
			metadata['post_role_settings'] = psetting

			 msetting = {}
			 msetting["mobile_ads_url"] = params[:midmobileadd]
			 msetting["ads_url"] = params[:midadd]
			 msetting["mid_role_frequency"] = params[:midrolefreq].to_i		 
			 if params[:midactive] == "yes"
			 	msetting["active"] = true
			 else
			 	msetting["active"] = false
			 end
			 msetting["type"] = params[:midtype]
			metadata['mid_role_settings'] = msetting

			 osetting = {}
			 osetting["mobile_ads_url"] = params[:overlaymobileadd]
			 osetting["ads_url"] = params[:overlayadd]
			 osetting["overlay_frequency"] = params[:overlayfreq].to_i
			 if params[:overlayactive] == "yes"
			 	osetting["active"] = true
			 else
			 	osetting["active"] = false
			 end
			metadata['overlay_settings'] = osetting

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end
					
			metadata["business_group_id"] = bg.ott_id
		    metadata["status"] = params[:status] ? params[:status] : "edit"
		
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        if params[:method] == "update"
        content_id = params[:content_id]
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogid+"/items/"+content_id+"?auth_token="+@bg.ott_auth_token
		response = http_request("put",body,url,maxtime)
		if response["code"] == 200
        else
            status = response["status"]
            #error = response["body"]["error"]["message"]
        end
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogid+"/items/?auth_token="+@bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
            #error = response["body"]["error"]["message"]
        end
    	end

        return response

	end



	def create_access_control(bg,tenant)
		status = true
		error = ""
		response = nil
		if status
			body = {}
			body["auth_token"] = bg.ott_auth_token
			body["catalog_data"] = {}
			body["catalog_data"]["name"] = "Access Control"
			body["catalog_data"]["theme"] = "access_control"
			body["catalog_data"]["title"] = "Access Control"
			body["catalog_data"]["is_default"] = true
			body["catalog_data"]["apps"] = []
			tenant_ids = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				body["catalog_data"]["apps"] << app.ott_id
				tenant_ids << app.tenant_id
			end
			url = "http://"+get_ott_base_url+"/catalogs"
		    response = http_request("post",body,url,60)
			if !response["status"]
				status = false
				error = response["error"]
			end
		end
		if status
			catalog = Catalog.new
			catalog.name = "Access Control"
			catalog.theme = "access_control"
			catalog.app_id = []
			tenant_id = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				catalog.app_id << app.id
				tenant_id << app.tenant_id
			end
			catalog.tenant_id = tenant_id.uniq
			catalog.bg_id = bg.id
			catalog.ott_id = response["body"]["data"]["catalog_id"]
			catalog.save
		end
		return catalog
	end

	def default_create_access_control_items(bg,catalogid,tenant)
		status = true
     	metadata = {}
			metadata["description"] = "Insta OTT default description"
			metadata["is_free"] = true
			metadata["is_tvod"] = false
			metadata["ads_available"] = true
			metadata["login_required"] = true
			metadata["is_default"] = true
			metadata["start_date"]= "20151201"
			metadata["end_date"]= "20151225"
			metadata["tenant_ids"] = []
			Tenant.where(:id => tenant.id).each do |t_id|
				metadata["tenant_ids"] << t_id.ott_id
				metadata["title"] = "Default Access Control"
			end
			
			 presetting = {}
			 presetting["mobile_ads_url"] = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x360&iu=/6062/iab_vast_samples/skippable&ciu_szs=300x250,728x90&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=[referrer_url]&correlator=[timestamp]"
			 presetting["ads_url"] = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x360&iu=/6062/iab_vast_samples/skippable&ciu_szs=300x250,728x90&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=[referrer_url]&correlator=[timestamp]"
			 presetting["active"] = true
			 presetting["type"] = "VAST"
			metadata['pre_role_settings'] = presetting

			 psetting = {}
			 psetting["mobile_ads_url"] = "http://adex.adchakra.net/KIRTI/openx-2.8.9/www/delivery/fc.php?script=bannerTypeHtml:vastInlineBannerTypeHtml:vastInlineHtml&zones=pre-roll:0.0-0=1&nz=1&source=&r=1234&block=1&format=vast&charset=UTF-8&version=3.0"
			 psetting["ads_url"] = "http://adex.adchakra.net/KIRTI/openx-2.8.9/www/delivery/fc.php?script=bannerTypeHtml:vastInlineBannerTypeHtml:vastInlineHtml&zones=pre-roll:0.0-0=1&nz=1&source=&r=1234&block=1&format=vast&charset=UTF-8&version=3.0"
			 psetting["active"] = true
			 psetting["type"] = "VAST"
			metadata['post_role_settings'] = psetting

			 msetting = {}
			 msetting["mobile_ads_url"] = "http://adex.adchakra.net/KIRTI/openx-2.8.9/www/delivery/fc.php?script=bannerTypeHtml:vastInlineBannerTypeHtml:vastInlineHtml&zones=pre-roll:0.0-0=1&nz=1&source=&r=1234&block=1&format=vast&charset=UTF-8&version=3.0"
			 msetting["ads_url"] = "http://adex.adchakra.net/KIRTI/openx-2.8.9/www/delivery/fc.php?script=bannerTypeHtml:vastInlineBannerTypeHtml:vastInlineHtml&zones=pre-roll:0.0-0=1&nz=1&source=&r=1234&block=1&format=vast&charset=UTF-8&version=3.0"
			 msetting["mid_role_frequency"] = 5		 
			 msetting["active"] = true
			 msetting["type"] = "VAST"
			metadata['mid_role_settings'] = msetting

			 osetting = {}
			 osetting["mobile_ads_url"] = "http://pubads.g.doubleclick.net/gampad/ads?sz=400x300&iu=%2F6062%2Fiab_vast_samples&ciu_szs=300x250%2C728x90&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=[referrer_url]&correlator=[timestamp]&cust_params=iab_vast_samples%3Dimageoverlay"
			 osetting["ads_url"] = "http://pubads.g.doubleclick.net/gampad/ads?sz=400x300&iu=%2F6062%2Fiab_vast_samples&ciu_szs=300x250%2C728x90&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=[referrer_url]&correlator=[timestamp]&cust_params=iab_vast_samples%3Dimageoverlay"
			 osetting["overlay_frequency"] = 5
			 osetting["active"] = true
			metadata['overlay_settings'] = osetting
			metadata["regions"] = ["IN"]
			metadata["business_group_id"] = bg.ott_id
		    metadata["status"] = "edit"

		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogid+"/items/?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
            #error = response["body"]["error"]["message"]
        end
        return response
	end

end
