module TvchannelsHelper
	
	def livechannels(bg,params,catalogottid)
		status = true

	       metadata = {}
			metadata["title"] = params[:title]
			metadata["channel_name"] = params[:channel_name]
			metadata["channel_id"] = params[:channel_id]
			metadata["logo"] = params[:logo]
			metadata["featured_image"] = params[:featured_image]
			metadata["l_listing_image"] = params[:l_listing_image]
			metadata["p_listing_image"] = params[:p_listing_image]
			metadata["smarturl"] = params[:smarturl]
			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end
			tenant_ids = []
			metadata["app_ids"] = []
			params[:app_ids].each do |a|
			  app = App.find(a)
			  metadata["app_ids"] << app.ott_id	
			  tenant_ids << app.tenant_id
			end	
			tenant_ids = tenant_ids.uniq
			metadata["tenants"] = []
			Tenant.where(:id => tenant_ids[0]).each do |t_id|
				metadata["tenants"] << t_id.ott_id
			end
			metadata["business_group_id"] = bg.ott_id
			metadata["status"] = params[:status] ? params[:status] : "edit"
			uniqgenres = params[:genre]
			metadata["genre"] = uniqgenres.uniq
			languages = params[:language]
			metadata["language"] = languages
			metadata["description"] = params[:description]

			metadata["region_order"] = []
			# count = params[:region].count
   #      	for i in 0..count-1
			#  regionorderdata = {}
			#  regionorderdata["region"] = params[:region][i]
			#  regionorderdata["order"] = params[:order][i]
			#  regionorderdata["publish_status"] = params[:publish_status][i]
			#  regionorderdata["epg_available"] = params[:epg_available][i]
			#  tenant_ids = []
			#  regionorderdata["applications"] = []
			#  params[:app_ids].each do |a|
			#   app = App.find(a)
			#   regionorderdata["applications"] << app.ott_id	
			#   tenant_ids << app.tenant_id
			# end	
			# tenant_ids = tenant_ids.uniq
			# regionorderdata["tenants"] = []
			# Tenant.where(:id => tenant_ids[0]).each do |t_id|
			# 	regionorderdata["tenants"] << t_id.ott_id
			# end
			#  metadata["region_order"] << regionorderdata
			# end

		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogottid+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response
	end

end




