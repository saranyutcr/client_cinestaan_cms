module CatalogListsHelper
	def validate_list_params(params)

		if params[:catalog_list] == "no"

			if !params.has_key? "apps"
				return false,"Please select applications!"
			end

			if params.has_key? "tags"
				if params[:tags].blank?
					return false,"Please provide valid tags!"
				end	
			end
		end	
		return true,""
	end	

	def get_s3upload_url_for_episode(mediaid,catalog,bg)
		subcategory_data = Subcategory.find_by(:id => mediaid.subcategory_id)
    	if !subcategory_data.nil?
    		@subcatgeory = "yes"
    		subcategory_data = Subcategory.find_by(:id => mediaid.subcategory_id)
    		shows = ShowTheme.find_by(:id => subcategory_data.show_id);
    		show_name = shows.title

    	  	if subcategory_data.parent_subcategory_id == "show"
		  		s3uploadremovspace = bg.name+"/"+catalog.name+"/shows/"+show_name+"/subcategories/"+subcategory_data.title+"/episodes/"+mediaid.title+"/images/banners"
		  		s3upload = s3uploadremovspace.gsub(" ", "_");
	      	else
		        parent_subcategory = Subcategory.find_by(:id => subcategory_data.parent_subcategory_id)
		        if parent_subcategory.parent_subcategory_id != "show"
		    		parent_s_subcategory = Subcategory.find_by(:id => parent_subcategory.parent_subcategory_id)
		    		s3uploadremovspace = bg.name+"/"+catalog.name+"/shows/"+show_name+"/subcategories/"+parent_s_subcategory.title+"/subcategories/"+parent_subcategory.title+"/subcategories/"+subcategory_data.title+"/episodes/"+mediaid.title+"/images/banners"
		    		s3upload = s3uploadremovspace.gsub(" ", "_");
		        else
		    		s3uploadremovspace = bg.name+"/"+catalog.name+"/shows/"+show_name+"/subcategories/"+parent_subcategory.title+"/subcategories/"+subcategory_data.title+"/episodes/"+mediaid.title+"/images/banners"
		    		s3upload = s3uploadremovspace.gsub(" ", "_");
		  	    end
	    	end
    	else
    		@show = "yes"
    		show_data = ShowTheme.find_by(:id => mediaid.show_id) || ShowTheme.find_by(:ott_id => mediaid.show_id)
    		#s3upload = bg.name+"/"+catalog.name+"/"+show_data.title+"/episodes/"+mediaid.title+"/images"
    		s3uploadremovspace = bg.name+"/"+catalog.name+"/shows/"+show_data.title+"/episodes/"+mediaid.title+"/images/banners"
    		s3upload = s3uploadremovspace.gsub(" ", "_");
    		puts "======s3upload show======"
    		p s3upload
    	end
    	return s3upload
	end

	def get_s3upload_url_for_subcategory(item,catalog,bg)
		if item.parent_subcategory_id == "show"
			show_data = ShowTheme.find_by(:id => item.show_id)
			s3uploadremovspace = bg.name+"/"+catalog.name+"/shows/"+show_data.title+"/subcategories/"+item.title+"/images/banners"
		  	s3upload = s3uploadremovspace.gsub(" ", "_");
		else
			parent_subcategory = Subcategory.find_by(:id => item.parent_subcategory_id)
	        if parent_subcategory.parent_subcategory_id != "show"
	    		parent_s_subcategory = Subcategory.find_by(:id => parent_subcategory.parent_subcategory_id)
	    		s3uploadremovspace = bg.name+"/"+catalog.name+"/shows/"+show_name+"/subcategories/"+parent_s_subcategory.title+"/subcategories/"+parent_subcategory.title+"/subcategories/"+item.title+"/images/banners"
	    		s3upload = s3uploadremovspace.gsub(" ", "_");
	        else
	    		s3uploadremovspace = bg.name+"/"+catalog.name+"/shows/"+show_name+"/subcategories/"+parent_subcategory.title+"/subcategories/"+item.title+"/images/banners"
	    		s3upload = s3uploadremovspace.gsub(" ", "_");
	  	    end
		end	
		return s3upload
	end

	def get_s3upload_url_for_show(item,catalog,bg)
		s3uploadremovspace = bg.name+"/"+catalog.name+"/shows/"+item.title+"/images/banners"
		s3upload = s3uploadremovspace.gsub(" ", "_");
	end	
end
