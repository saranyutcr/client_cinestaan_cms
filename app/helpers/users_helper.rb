module UsersHelper
	def generate_password
    	return SecureRandom.hex(4)
  	end

  	class SendSignupMail < Struct.new(:user,:password)
  		def perform
  			Emailer.send_signup_email(user,password).deliver
  		end
  	end

  	class SendPublishedMail < Struct.new(:user,:media)
  		def perform
  			Emailer.send_published_email(user,media).deliver
  		end
  	end

    class RegisterMail < Struct.new(:user)
      def perform
        Emailer.send_register_email(user).deliver
      end
    end

    class ConfirmMail < Struct.new(:user)
      def perform
        Emailer.send_confirm_email(user).deliver
      end
    end

    class ForgotMail < Struct.new(:user)
      def perform
        Emailer.send_forgot_email(user).deliver
      end
    end

    class UserRegister < Struct.new(:user)
      def perform
        Emailer.send_user_register(user).deliver
      end
    end

end
