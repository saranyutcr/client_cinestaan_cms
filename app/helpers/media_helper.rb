module MediaHelper
include PeopleHelper
	def extract_metadata_from_params(params)
		metadata = {}
		status = true
		error = ""
		catalog = Catalog.find(params[:catalog_id])
		case catalog.theme
		when "movie"
			metadata["title"] = params[:title]
			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end
			metadata["app_ids"] = []
			tenants = []
			params[:apps] = eval(params[:apps]) if params[:apps].class == String
			params[:apps].each do |a|
			  app = App.find(a)
			  metadata["app_ids"] << app.ott_id
			  tenants << app.tenant_id
			end	
			tenants = tenants.uniq
			
			metadata["tenant_ids"] = []
			tenants.each do |t|
				tenant = Tenant.find(t)
				metadata["tenant_ids"] << tenant.ott_id
			end
		
			metadata["business_group_id"] = BusinessGroup.find(catalog.bg_id).ott_id

			languages = params[:language]
			lang = languages.first
			metadata["language"] = lang


			if params["genres"].class == String
               uniqgenres = eval(params[:genres])
               metadata["genres"] = uniqgenres.uniq
            else  
               uniqgenres = params[:genres]
               metadata["genres"] = uniqgenres.uniq
			end	
			metadata["description"] = params[:description]
			metadata["duration"] = 60
			metadata["rating"] = params[:rating].to_i
			# metadata["smart_url"] = params[:smart_url]
			
			metadata["asset_id"] = params[:asset_id] if params[:asset_id]
			metadata["sequence_no"] = params[:sequence_no] if params[:sequence_no] 

			custom_fields = get_custom_fields(params)
			metadata["custom_fields"] = custom_fields if !custom_fields.empty?
		when "video"
			metadata["title"] = params[:title]
			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["app_ids"] = []
			tenants = []
			params[:apps] = eval(params[:apps]) if params[:apps].class == String
			params[:apps].each do |a|
			  app = App.find(a)
			  metadata["app_ids"] << app.ott_id	
			  tenants << app.tenant_id
			end	
			tenants = tenants.uniq
			
			metadata["tenant_ids"] = []
			tenants.each do |t|
				tenant = Tenant.find(t)
				metadata["tenant_ids"] << tenant.ott_id
			end
			
			metadata["business_group_id"] = BusinessGroup.find(catalog.bg_id).ott_id
			languages = params[:language]
			lang = languages.first
			metadata["language"] = lang
			
			if params["genres"].class == String
              uniqgenres = eval(params[:genres])
               metadata["genres"] = uniqgenres.uniq
            else  
               uniqgenres = params[:genres]
               metadata["genres"] = uniqgenres.uniq
			end				
			metadata["description"] = params[:description]
			metadata["duration"] = 60
			metadata["rating"] = params[:rating].to_i
			# metadata["smart_url"] = params[:smart_url]
			
			metadata["asset_id"] = params[:asset_id] if params[:asset_id]
			metadata["sequence_no"] = params[:sequence_no] if params[:sequence_no]

			custom_fields = get_custom_fields(params)
			metadata["custom_fields"] = custom_fields if !custom_fields.empty?
		when "show"
		    metadata["title"] = params[:title]
		    metadata["show_id"] = params[:show_id]
		    metadata["subcategory_id"] = params[:subcategory_id]
			
			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["app_ids"] = []
			tenants = []
			params[:apps] = eval(params[:apps]) if params[:apps].class == String
			params[:apps].each do |a|
			  app = App.find(a)
			  metadata["app_ids"] << app.ott_id	
			  tenants << app.tenant_id
			end	
			tenants = tenants.uniq
			
			metadata["tenant_ids"] = []
			tenants.each do |t|
				tenant = Tenant.find(t)
				metadata["tenant_ids"] << tenant.ott_id
			end
			
			metadata["business_group_id"] = BusinessGroup.find(catalog.bg_id).ott_id
			languages = params[:language]
			lang = languages.first
			metadata["language"] = lang
			if params["genres"].class == String
              uniqgenres = eval(params[:genres])
               metadata["genres"] = uniqgenres.uniq
            else  
               uniqgenres = params[:genres]
               metadata["genres"] = uniqgenres.uniq
			end	
			
			metadata["description"] = params[:description]
			metadata["duration"] = 60
			metadata["rating"] = params[:rating].to_i
			# metadata["smart_url"] = params[:smart_url]
			
			metadata["asset_id"] = params[:asset_id] if params[:asset_id]
			metadata["sequence_no"] = params[:sequence_no] if params[:sequence_no]	
			custom_fields = get_custom_fields(params)
			metadata["custom_fields"] = custom_fields if !custom_fields.empty?
		end
		status = params[:status]

		if !params[:utc_published_date].blank?
		    metadata["published_date"] = params[:utc_published_date]
		    if status == "published"
		    	status = "unpublished"
		    end	
		else
			metadata["published_date"] = ""     
		end
		
		if !params[:utc_unpublished_date].blank?
			metadata["unpublished_date"] = params[:utc_unpublished_date]
		else
			metadata["unpublished_date"] = "" 
		end	

		metadata["status"] = status

		metadata["featured_image"] = params[:featured_image] if !params[:featured_image].blank?
		metadata["l_listing_image"] = params[:l_listing_image] if !params[:l_listing_image].blank?
		metadata["p_listing_image"] = params[:p_listing_image] if !params[:p_listing_image].blank?
		# metadata["status"] = params[:status] ? params[:status] : "edit"
		# New fields values for content owner starts
		metadata["start_time"] = params[:start_time] if params[:start_time]
		metadata["end_time"] = params[:end_time] if params[:end_time]
		metadata["censor_rating"] = params[:censor_rating] if params[:censor_rating]
		metadata["currency"] = params[:currency] if params[:currency]
		metadata["cost_per_hour"] = params[:cost_per_hour] if params[:cost_per_hour]
		metadata["content_owner_id"] = params[:content_owner] if params[:content_owner]
		# New fields values for content owner ends
		metadata["short_description"] = params[:short_description] if params[:short_description]
		metadata["keywords"] = params[:keywords] if params[:keywords]
		metadata["release_date_string"] = params[:release_date_string] if params[:release_date_string]
		metadata["duration_string"] = params[:duration_string] if params[:duration_string]
		# metadata["published_date"] = params[:published_date] if params[:published_date]
		if params["source_url"].blank?
			metadata["play_url_type"] = params[:default_url_type]
			metadata["play_url"] = build_play_url_hash(params)
	    end

	    if params.has_key? "people"
	        obj = create_people_object(params)
	       	metadata["people"] = obj if !obj.empty?
	    end	

	    if params.has_key? "access_control_id"
	    	acobj = update_access_control(params)
	    	metadata["behavior"] = acobj if !acobj.empty?
		end
		metadata["episode_type"] = params[:episodetype_tag] if params[:episodetype_tag]

		return metadata
	end

	def extract_metadata_from_row(media_row)
		metadata = {}
		return metadata
	end
	def validate_media_params(media_params)
		Rails.logger.debug "Validating Media Params #{media_params}"
		bg = BusinessGroup.find(media_params["bg_id"])
		if bg.plan == "transcoding"
			required_params = ["title","source_type","source_url","user_id","user_role","publish_settings_id","bg_id","priority","status","is_removed","bulk_upload_id","custom_keys"]
		else
			required_params = []
		end
		if required_params.all? {|s| media_params.key? s}
			return true
		else
			return false
		end
	end
	def validate_media_row_for_key(media_row,key,row_number)
		status = true
		error = ""
		# if (!media_row.has_key? key) || (media_row[key]==nil) || (media_row[key].strip=="")
		if (!media_row.has_key? key) || (media_row[key].blank?)
			status = false
			error = "The key \"#{key}\" is mandatory! Verify row #{row_number}!"
			Rails.logger.debug error
		end
		return status,error
	end
	def update_media(media,metadata)
		media.title=metadata["title"]
		if media.save
			return media
		else
			return nil
		end

	    
	end


	def create_media(media_params)
		media = Media.new
		media.title = media_params["title"]
		media.source_type = media_params["source_type"]
		media.source_url = fix_multiple_slashes(media_params["source_url"]) if media_params["source_url"]
		#media.source_url = media_params["source_url"] if media_params["source_url"]
		media.notification_url = media_params["notification_url"]
		media.user_id = media_params["user_id"]
		media.user_role = media_params["user_role"]
		media.publish_settings_id = media_params["publish_settings_id"]
		media.bg_id = media_params["bg_id"]
		media.priority = media_params["priority"]
		media.status = media_params["status"]
		media.ott_status = media_params["ott_status"]
		media.is_removed = media_params["is_removed"]
		media.bulk_upload_id = media_params["bulk_upload_id"]
		media.custom_keys = media_params["custom_keys"]
		media.cut_intervals = media_params["cut_intervals"]
		media.banner = media_params["banner"]
		media.smart_url = media_params["smart_url"]
		media.show_id = media_params["show_id"]
		media.subcategory_id = media_params["subcategory_id"]
		media.asset_id = media_params["asset_id"]
		media.ott_publish_date = media_params["utc_published_date"] if !media_params["utc_published_date"].blank?
		media.ott_unpublish_date = media_params["utc_unpublished_date"] if !media_params["utc_unpublished_date"].blank?
		media.image_extraction_flag = media_params["imgoptradio"]
		media.parent_media_id = media_params["parent_media_id"]
		media.content_owner_id = media_params["content_owner_id"]
		media.currency = media_params["currency"]
		media.cost_per_hour = media_params["cost_per_hour"]
		media.censor_rating = media_params["censor_rating"]
		media.duration_string = media_params["duration_string"]
		if  media_params["videoflag"] == "yes"
			media.videolist = true
		else
			media.videolist = false
		end

		if media.save
			return media
		else
			return nil
		end
	end
	def fix_multiple_slashes(url)
		protocol = nil
		if url.split(":\/\/").count > 0
			#finding input protocol from prefix
			protocol = url.split(":\/\/")[0]
			#removing protocol temporarily
			url = url.sub("#{protocol}:\/\/","")
			#loop through and reduce more than one adjacent slash to single slash in remaining URL
			while url.include?"//"
				url = url.gsub!("\/\/","\/")
			end
			if protocol!=nil
				url = protocol+"://"+url
			else
				url = nil
			end
		else
			url = nil
		end
		return url
	end
	def extra_keys_required_for_storage(cdn_name)
		status = true
  		error = ""
  		extra_keys = nil
		#make api call
		# ip = get_cms_base_url.partition(':').first
		# url = "http://"+ip+":4567/extra_keys_required_for_storage"
		url = "http://#{get_aux_module_base_url}/extra_keys_required_for_storage"
		body = {}
  		maxtime = 60
  		body["cdn_name"] = cdn_name
		begin
			Timeout::timeout(maxtime) do
				Fiber.new{
					ret = Typhoeus::Request.post(url,:body => body.to_json,:headers => {"content-type" => "application/json","accept" =>"application/json"})
					if ret.options[:response_body]
						extra_keys = JSON.parse(ret.options[:response_body])["extra_keys"]
					else
						extra_keys = []
					end
				}.resume
			end
			if extra_keys == nil
				extra_keys = []
			end
		rescue Timeout::Error
			extra_keys = []
		rescue 	Exception => e
			Rails.logger.debug e.message
			Rails.logger.debug e.backtrace.inspect
			extra_keys = []
		end
		return extra_keys
	end
=begin
  	class DeleteMedia < Struct.new(:media)
  		def perform
  			body = {}
			body["platform_media_id"] = media.platform_media_id
	  		maxtime = 60
	  		bg = BusinessGroup.find(media.bg_id)
			begin
				Timeout::timeout(maxtime) do
					Fiber.new{
						url = "http://#{bg.tx_base_url}/delete_media"
						ret = Typhoeus::Request.post(url,:body => body.to_json,:headers => {"content-type" => "application/json","accept" =>"application/json"})
					}.resume
				end
			rescue Timeout::Error
			rescue 	Exception => e
				Rails.logger.debug e.message
				Rails.logger.debug e.backtrace.inspect
			end
  		end
  	end
=end
  	def sign_smart_url(smart_url_key,head_url)
	  	signature = Digest::MD5.hexdigest("#{smart_url_key}#{head_url}")
	  	full_url =  "#{head_url}#{signature}"
	  	full_url
	end
	def shorten_string(anystring,start_length,end_length)
		start_string = anystring[0..start_length]
		end_string = anystring[anystring.length-end_length,anystring.length-1]
		return start_string+"..."+end_string
	end
	def is_valid_string?(x)
		if x.nil? || x.empty? || !(x.is_a? String)
			return false
		else
			return true
		end
	end
	def is_valid_integer?(x)
		if x.nil? || !(x.is_a? Integer)
			return false
		else
			return true
		end
	end
	def is_valid_float?(x)
		if x.nil? || !(x.is_a? Float)
			return false
		else
			return true
		end
	end
	def remove_trailing(x)
	  	Float(x)
	  	i, f = x.to_i, x.to_f
	  	i == f ? i : f
	rescue ArgumentError
	  	x
	end
	def excel_to_hash(path)
		excel = Roo::Excelx.new(path)
		excel.default_sheet = excel.sheets.first
		headers = excel.row(1)
		new_headers = []
		headers.each do |element|
		if element.class == Float
			e = element.to_i.to_s 
		else
			e = element	
		end
		new_headers << e
		end	
		media_list = []
	    (2..excel.last_row).each do |i|
	      next unless excel.row(i)[0]
	      row = Hash[[new_headers, excel.row(i)].transpose]
	      row.each_key{|x| row[x] = remove_trailing(row[x].to_s.strip) if row[x]}
	      media_list << row
	    end
	    return media_list
	end

	def check_images(catalog,params,bg)
		status = true
		msg = ""

		if params["featured_image"].blank? || params["l_listing_image"].blank? || params["p_listing_image"]
			url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id + "?auth_token="+ bg.ott_auth_token
			response = http_request("get",{},url,60)
			if response["code"] == 200
				catalog_data = response["body"]["data"]
			end
	    end
	    image_hash = get_catalog_image(catalog_data)
	    large = image_hash["l_large"]
	    medium = image_hash["l_medium"]
	    small = image_hash["l_small"]

	    params["featured_image"] = params["featured_image"].blank? ? large : params["featured_image"]
	    params["l_listing_image"] = params["l_listing_image"].blank? ? medium : params["l_listing_image"]
	    params["p_listing_image"] = params["p_listing_image"].blank? ? small : params["p_listing_image"]

		# theme = catalog["theme"]
		# case theme
		# when "video"
		# 	image = params[:l_listing_image]
		# 	if image.empty?
		# 		status = false
		# 		msg = "Please Provide Landscape Image"
		# 	end	
		# when "movie"	
		# 	image = params[:p_listing_image]
		# 	if image.empty?
		# 		status = false
		# 		msg = "Please Provide Portrait Image"
		# 	end	
		# end	
		return status,params
	end

	def get_catalog_image(catalog)
		image_hash = default_images(catalog["theme"])
		if !catalog["item_images"].blank?
			
			if catalog["item_images"].has_key? "l_large"
				if catalog["item_images"]["l_large"].has_key? "url"
					if !catalog["item_images"]["l_large"]["url"].blank? 
						image_hash["l_large"] = catalog["item_images"]["l_large"]["url"]
					end	
				end	
			end		

			if catalog["item_images"].has_key? "l_medium"
				if catalog["item_images"]["l_medium"].has_key? "url"
					if !catalog["item_images"]["l_medium"]["url"].blank? 
						image_hash["l_medium"] = catalog["item_images"]["l_medium"]["url"]
					end	
				end	
			end

			if catalog["item_images"].has_key? "l_small"
				if catalog["item_images"]["l_small"].has_key? "url"
					if !catalog["item_images"]["l_small"]["url"].blank? 
						image_hash["l_small"] = catalog["item_images"]["l_small"]["url"]
					end	
				end	
			end	
		end	

		return image_hash
	end	

    def build_play_url_hash(params)
		play_url = {}
		if params[:url_types]
			count = params[:url_types].count
		else
			count = 0
		end
		#count = params[:url_types].count
		for i in 0..count-1
			key = params[:url_types][i]
			h = {}
			h["url"] = params[:url_names][i]
			# h["width"] = !params[:widths][i].empty? ? params[:widths][i].to_f : -1.0
			# h["height"] = !params[:heights][i].empty? ? params[:heights][i].to_f : -1.0
			play_url[key] = h
		end	
		return play_url
	end

	def get_custom_fields(params)
		obj = {}
		if params[:tags]
			tags = params[:tags]
			tags = eval(params[:tags]) if params[:tags].kind_of? String
			tags.each do |hash|
				data = hash
				# data = JSON.parse(hash) if hash.kind_of? String  
				datatype = data["type"]
				case datatype
				when "String"	
				  obj[data["tag"]] = !params[data["tag"]].blank? ? params[data["tag"]] : "" 
				when "Array"
				  obj[data["tag"]] = !params[data["tag"]].blank? ? params[data["tag"]].split(',') : []
				when "Integer"
				  obj[data["tag"]] = !params[data["tag"]].blank? ? check_valid_data(params[data["tag"]]) : -1
				end  	  
			end	
		end	
		return obj
	end	

	def check_valid_data(value)
		if value.numeric?
			return value.to_i
		else
		    return -1	
		end	
	end	


	def update_access_control(params)
        accesscontrol = []
        count = params[:region].count
        for i in 0..count-1
	    behavior = {}
		behavior["region"] = params[:region][i]
        behavior["access_control_id"] = params[:access_control_id][i]
        if params["tvod_pack"].present?
			subscription_catalogs = Catalog.where(:bg_id => current_bg.id, :theme => "subscription")
			# @all_tvod_featured_plans = []
			@tvod_featured_items = []
			if subscription_catalogs.present?
			subscription_catalogs.each do |subscription_catalog|
				tvod_url = "http://"+get_ott_base_url+"/catalogs/"+subscription_catalog.ott_id+"/items?region=any&auth_token="+ current_bg.ott_auth_token 
	            @tvod_response = http_request("get",{},tvod_url,60)
	            # @tvod_featured_items << @tvod_response["body"]["data"]["plans"]
			end
			end
          # tvod_url = "http://54.165.33.205:10000/catalogs/590fffda39ac6e6ef10005d9/items/5a5459ede859403c5a000003?auth_token=dz25UevHcFq72awidzMx&region=US"
          #                 byebug
          # @tvod_plans = @tvod_response["body"]["data"]["plans"]
          # a = @tvod_plans.select{|r| r["id"] == params["tvod_pack"]}
         # tvod_plans = update_tvod_plans(params)
                # metadata["behavior"] = acobj if !acobj.empty?
    	  if params["tvod_regions"].present? 
    		regions_array = []
    		regions_array << params["tvod_regions"].collect{|r| r.split(" -").first}
    		behavior["tvod_region"] = regions_array.flatten
    	  end
          behavior["tvod_plan_id"] = params['tvod_pack']
          behavior["tvod_subscription_id"] = params['tvod_plan']#@tvod_response["body"]["data"]["catalog_id"] if !@tvod_response.nil?
          behavior["tvod_catalog_id"] = @tvod_response["body"]["data"]["catalog_id"] if !@tvod_response.nil?

          # behavior["tvod_price"] = a.first["region"].first["price"]
          # behavior["tvod_currency"] = a.first["region"].first["currency"]
          # behavior["tvod_duration"] = a.first["duration"]
          # behavior["tvod_period"] = a.first["period"]
          # behavior["svod_id"] = []
          # params[:svodid].each do |sd|
         # behavior["svod_id"] << sd
        end
		accesscontrol << behavior
	    end

		return accesscontrol
	end


    def extract_metadata_from_videolistparams(params)
		metadata = {}
		#obj = {}

		catalog = Catalog.find(params[:catalog_id])
		case catalog.theme
		when "movie"
			metadata["catalog"] = "movie"
		when "video"
			metadata["catalog"] = "video"
	    when "show"
	    	metadata["catalog"] = "show"
	    end
			metadata["title"] = params[:title]
			metadata["featured_image"] = params[:featured_image] if !params[:featured_image].blank?
			metadata["l_listing_image"] = params[:l_listing_image] if !params[:l_listing_image].blank?
			metadata["p_listing_image"] = params[:p_listing_image] if !params[:p_listing_image].blank?

			metadata["category"]=params[:videotags]
			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end
					
			metadata["business_group_id"] = BusinessGroup.find(catalog.bg_id).ott_id
			metadata["description"] = params[:description]
			metadata["duration"] = params[:duration_string]
			metadata["asset_id"] = params[:asset_id]
			metadata["rating"] = params[:rating].to_i
			metadata["smart_url"] = "smart_url"
		    metadata["status"] = params[:status] ? params[:status] : "edit"
		
		if params["source_url"].blank?
			logger.debug "=================> Params[:default_url_type] is:: #{params[:default_url_type]}<============="
			metadata["play_url_type"] = params[:default_url_type]
			metadata["play_url"] = build_play_url_hash(params)
	    end

	    if params.has_key? "people"
	        obj = create_people_object(params)
	       	metadata["people"] = obj if !obj.empty?
	    end

	    #obj[:video_list] = metadata
		return metadata
	end

	# def validate_media_row_for_ott_saas(media_row,index)
	# 	status = true
	# 	error = ""
	# 	mandatory_keys = ["Catalog Id","Title","Regions","Applications"] #{}"Source URL","Image Large","Image Medium","Image Small","Genres","Language"]
	#     # non_mandatory_keys = ["Source URL","Smart URL","Smart URL Type"]
	#     mandatory_keys.each do |key|
	#         status,error = validate_media_row_for_key(media_row,key,index)
	#         if !status
	#         	break
	#         end	
	#     end 
	#     return status,error
	# end	

	def validate_only_mandatory_params(media_row,index)
		status = true
		error = ""
		mandatory_keys = ["Catalog Id","Title","Regions","Applications"]
		mandatory_keys.each do |key|
			status,error = validate_media_row_for_key(media_row,key,index)
	        if !status
	        	break
	        end	
		end	
		status,error = check_url_type(media_row,index)
		if !status
			return status,error
		end	
		return status,error
	end	

	def validate_only_mandatory_params_for_update(media_row,index)
		status = true
		error = ""
		mandatory_keys = ["Asset Id","Catalog Id","Title","Regions","Applications"]
		mandatory_keys.each do |key|
			status,error = validate_media_row_for_key(media_row,key,index)
	        if !status
	        	break
	        end	
		end	
		# status,error = check_url_type(media_row,index)
		# if !status
		# 	return status,error
		# end	
		return status,error
	end	

	def check_url_type(media_row,index)
		if (!media_row.key? "Source URL") && (!media_row.key? "Smart URL")
			return false, "Please provide either Source URL / Smart URL key at row number #{index}"
		end	

		if (media_row["Source URL"]).blank? && (media_row["Smart URL"]).blank?
			return false, "Please provide either Source URL / Smart URL value at row number #{index}"
		end	

		if media_row.key? "Source URL"
			if !media_row["Source URL"].blank?
				if !media_row["Source URL"].start_with?('ftp://','http://')
					return false, "Error in Source URL Format at row number #{index}!"
				end
			end	
		else
		    if media_row.key? "Smart URL"
		    	if media_row["Smart URL"].blank?
		    		return false,"Please provide key Smart URL at row number #{index}!"
		    	end	

		   	 	if !media_row.key? "Smart URL Type"
		   	 		return false, "Please provide key Smart URL Type at row number #{index}!"
		   	 	end	
		    end	
		end	
		return true,""
	end	

	def check_for_identical_catalog(media_list)
		return (media_list.map { |h| h["Catalog Id"] }).uniq.length == 1
	end	
	def check_for_identical_movie_catalog(media_list)
		
		return (media_list.map { |h| h["Movie Catalog Id"] }).uniq.length == 1
			
	end	
	def check_for_identical_person_catalog(media_list)
		
		return (media_list.map { |h| h["Person Catalog Id"] }).uniq.length == 1
			
	end	
	def extract_videodata_from_row_for_ott(media_row,bulkupload,params)
		
		metadata = {}
		metadata[:title] = media_row["Title"]
		metadata[:regions] = ["IN"]
		metadata[:catalog_id] = media_row["Catalog Id"]
		metadata[:genres] = media_row["Genres"].to_s.to_a if !media_row["Genres"].blank?
		metadata[:language] = media_row["Language"] if !media_row["Language"].blank?
		metadata[:description] = media_row["Description"] if !media_row["Description"].blank?
		metadata[:duration] = media_row["Duration"] if media_row["Duration"]
		metadata[:release_date_string] = media_row["Release Date"] if media_row["Release Date"]
		metadata[:rating] = media_row["Rating"].to_i if media_row["Rating"]
		metadata[:asset_id] = media_row["Video Asset Id"] if media_row["Video Asset Id"]
		metadata[:category] = media_row["Category"] if media_row["Category"]
		metadata[:smart_url] = "smart_url"		
		metadata[:featured_image] = media_row["Image Large"] if !media_row["Image Large"].blank?
                metadata[:l_listing_image] = media_row["Image Medium"] if !media_row["Image Medium"].blank?
                metadata[:p_listing_image] = media_row["Image Small"] if !media_row["Image Small"].blank?
		#metadata[:smart_url_type] = media_row["Smart URL Type"] if media_row["Smart URL Type"]
		if media_row["Smart Url"].blank?
			metadata["play_url"] = {} 
			metadata["play_url"][media_row["Source Url Type"]] = {}
			metadata["play_url"][media_row["Source Url Type"]]["url"] = media_row["Source Url"]
			metadata["play_url_type"] = media_row["Source Url Type"]
		else
			metadata["play_url"] = {} 
			metadata["play_url"][media_row["Source Url Type"]] = {}
			metadata["play_url"][media_row["Source Url Type"]]["url"] = media_row["Smart Url"]
			metadata["play_url_type"] = media_row["Source Url Type"]

		end
		return metadata

	end



	def extract_metadata_from_row_for_ott(media_row,bulkupload,params)
		metadata = {}
		metadata[:title] = media_row["Title"]
		metadata[:regions] = media_row["Regions"]
		metadata[:catalog_id] = media_row["Catalog Id"]
		if media_row["Applications"].include? "|"
			metadata[:apps] = media_row["Applications"].split("|")
		else	
			metadata[:apps] = media_row["Applications"].to_s.to_a
		end	
		metadata[:featured_image] = media_row["Image Large"] if !media_row["Image Large"].blank?
		metadata[:l_listing_image] = media_row["Image Medium"] if !media_row["Image Medium"].blank?
		metadata[:p_listing_image] = media_row["Image Small"] if !media_row["Image Small"].blank?
		metadata[:genres] = media_row["Genres"] if !media_row["Genres"].blank?
		metadata[:language] = media_row["Language"] if !media_row["Language"].blank?
		metadata[:description] = media_row["Long Description"] if !media_row["Long Description"].blank?
		metadata[:short_description] = media_row["Short Description"] if !media_row["Short Description"].blank?
		metadata[:duration_string] = media_row["Duration"] if media_row["Duration"]
		metadata[:release_date_string] = media_row["Release Date"] if media_row["Release Date"]
		metadata[:rating] = media_row["Rating"].to_i if media_row["Rating"]
		metadata[:asset_id] = media_row["Asset Id"] if media_row["Asset Id"]
		metadata[:sequence_no] = media_row["Sequence Number"].to_i if media_row["Sequence Number"]
		metadata[:keywords] = media_row["Keywords"] if media_row["Keywords"]
#		if params["type"] == "bulk_upload"
			metadata[:smart_url] = media_row["Smart URL"] if media_row["Smart URL"] && media_row["Source URL"].blank?
			metadata[:smart_url_type] = media_row["Smart URL Type"] if media_row["Smart URL Type"]
#		end
		metadata[:show_id] = media_row["Show Id"] if media_row["Show Id"]
		metadata[:subcategory_id] = media_row["Subcategory Id"] if media_row["Subcategory Id"]
		if params[:catalog_ott_obj]
			custom_tags = params[:catalog_ott_obj][:custom_tags]
			if !custom_tags.blank?
				# data = {}
				media_row[:tags] = custom_tags
				custom_fields = get_custom_fields(media_row)
				metadata[:custom_fields] = custom_fields if !custom_fields.empty?
			end	
		end	
		# metadata[:source_url] = media_row["Source URL"]
		metadata[:bulk_upload_id] = bulkupload.id
		metadata[:validate_data] = params["validate"]
		return metadata
	end	

	def validate_data_at_cms(media_list,type)
		status = true
		error = ""
		i = 1
		media_list.each do |media_row|
			catalog_id  = media_row["Catalog Id"]
			app_ids = media_row["Applications"]
			catalog = Catalog.new.get_catalog_by_id(catalog_id)
			if type == "bulk_update"
				asset_id = media_row["Asset Id"]
				media = Media.find_by(:ott_catalog_id => catalog.ott_id,:asset_id => asset_id,:is_removed => false)
				if !media
					status = false
					error = "Media is not present for Asset Id '#{asset_id}'!"
					break
				end	
			end	
			
			if catalog
				if catalog.theme == "livetv" || catalog.theme == "person"
					status = false
				    error = "You can not bulk upload data for theme '#{catalog.theme}'!"
				    break
				else	
					@flag = 0
					catalog_apps = catalog.app_id
					app_ids = app_ids.to_s
					if app_ids.include? "|"
						apps = app_ids.split("|")
					else	
						apps = app_ids.to_s.to_a
					end 
					apps.each do |app|
						if !catalog_apps.include? app
							status = false
							error = "Invalid Appication #{app} at row number #{i}!"
							@flag = 1
						end	
						if @flag == 1
						    break
						end    	
					end	
					if @flag == 1
						break
					end	
				end	
			else
			    status = false
			    error = "Invalid Catalog Id at row number #{i}!"
			    break	
			end	

			if catalog.theme == "show"
				##if show id and subcategory id are blank
				if media_row["Show Id"].blank? && media_row["Subcategory Id"].blank?
					status = false
					error = "Please provide either Show Id or Subcategory Id at row number #{i}!"
					break
				end

				if !media_row["Show Id"].blank? && !media_row["Subcategory Id"].blank?
				##if show id and subcategory id are present	
				    status = false
					error = "Please provide either Show Id or Subcategory Id at row number #{i}!"
					break	
				end	

				#check if show exists or not
				if media_row["Show Id"]
					show = ShowTheme.find(media_row["Show Id"])
					if !show
						status = false
						error = "Invalid Show Id at row number #{i}!"
						break
					end	
				end	

				if media_row["Subcategory Id"]
					subcategory = Subcategory.find(media_row["Subcategory Id"])
					if !subcategory
						status = false
						error = "Invalid Subcategory Id at row number #{i}!"
						break
					end	
				end

			end
			i = i + 1
		end
		return status,error	
	end	

	def extract_metadata_from_params_for_bulk_upload(params,bg)
		metadata = {}
		metadata["title"] = params[:title]
		
		if params[:regions]
			metadata["regions"] = params[:regions].split(",").map(&:strip)
		else
			metadata["regions"] = ["IN"]
		end
		metadata["app_ids"] = []
		tenants = []

		params[:apps].each do |a|
		  app = App.find(a)
		  metadata["app_ids"] << app.ott_id	
		  tenants << app.tenant_id
		end	
		tenants = tenants.uniq
		
		metadata["tenant_ids"] = []
		tenants.each do |t|
			tenant = Tenant.find(t)
			metadata["tenant_ids"] << tenant.ott_id
		end
	
		metadata["business_group_id"] = bg.ott_id
		
		metadata["language"] = params[:language] if params[:language]
		if params[:genres] 
			if !params[:genres].blank?
				if params[:genres].include? "|"
					metadata["genres"] = params[:genres].split("|")
				else
				    metadata["genres"] = params[:genres].to_s.to_a	
				end	
			end	
		end	
		metadata["featured_image"] = params[:featured_image] if params[:featured_image]
		metadata["l_listing_image"] = params[:l_listing_image] if params[:l_listing_image]
		metadata["p_listing_image"] = params[:p_listing_image] if params[:p_listing_image]

		metadata["description"] = params[:description] if params[:description]
		metadata["short_description"] = params[:short_description] if params[:short_description]
		metadata["release_date_string"] = params[:release_date_string] if params[:release_date_string]
		metadata["duration_string"] = params[:duration_string] if params[:duration_string]
		metadata["rating"] = params[:rating] if params[:rating]
		metadata["asset_id"] = params[:asset_id] if params[:asset_id]
		metadata["sequence_no"] = params[:sequence_no] if params[:sequence_no]
		metadata["keywords"] = params[:keywords] if params[:keywords]

		metadata["duration"] = 60
		metadata["rating"] = params[:rating].to_i if params[:rating]
		if params[:smart_url]
			metadata["play_url"] = {} 
			metadata["play_url"][params[:smart_url_type]] = {}
			metadata["play_url"][params[:smart_url_type]]["url"] = params[:smart_url]
			metadata["play_url_type"] = params[:smart_url_type]
		end
		metadata["status"] = params[:status] ? params[:status] : "edit"
		metadata["bulk_upload_id"] = params[:bulk_upload_id]
		metadata["show_id"] = params[:show_id] if params[:show_id]
		metadata["subcategory_id"] = params[:subcategory_id] if params[:subcategory_id]
		metadata["custom_fields"] = params[:custom_fields] if params[:custom_fields]
		# metadata["validate_data"] = params[:validate_data]
		return metadata
	end	

	def remove_data_from_ott(bulkupload,catalog,bg)
		url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/items/bulk_delete/"+ bulkupload.id + "?auth_token=" + bg["ott_auth_token"]	
		response = http_request("delete",{},url,60)		
	end	

	def remove_data_from_cms(bulkupload)
		data = Media.where(:bulk_upload_id => bulkupload.id)
		data.delete_all
		# data.each do |media|
		# 	media.is_removed = true
		# 	media.save
		# end	
	end	
	def get_soundtracks(asset_id,bg)
		base_url = bg.bridge_base_url
		
		url = base_url + "/getExtrasVideoList/1/" + asset_id
		logger.debug "soundtrack url ==#{url}"
		response = Typhoeus::Request.get(url,headers: {"authorization" => "Basic MTowY2U5OTk0NTY2OTNkZmMxNzc3OTFjOWQ1ZGZlNjE0MQ=="},followlocation: true)
		

		if response.response_code == 200

		  data = JSON.parse(response.body)	
		  logger.debug "cinestaan data for videolist===#{data}=========="

		  if data["Success"] == "1"
		  	return true,data["Result"]
		  else
			return false,"Error! Id doesn't exists. in external database"
		  end
		else
		  return false,"Can not pull soundtrack data from bridge!"	
		end	
	end
	def construct_data_for_sound_track data,asset_id,bg,catalog
		#logger.debug "constructing sound track details===#{data}"

		soundtrack = []
		list = data["list"]
		# large =   "http://d36k7zwutv9wuw.cloudfront.net/image-bank/1500-1500/"
		# medium =  "http://d36k7zwutv9wuw.cloudfront.net/image-bank/640-360/"
		# small =   "http://d36k7zwutv9wuw.cloudfront.net/image-bank/120-160/"

		logger.debug "list ====#{list}"
		
		list.each do |l|
			sound_data = {}
			sound_data["asset_id"] = l["id"]
			if l["title"].blank?
				sound_data["title"] = l["id"] 
			else
				sound_data["title"] = l["title"]
			end
			sound_data["duration"] = l["song_duration"]
			sound_data["release_date"] = l["song_release_date"]
			sound_data["status"] = "published"
			sound_data["description"] = ""
			sound_data["regions"]=["IN"]
			sound_data["rating"] = ""
			#image = l["image"]
			
		#	if !image.nil?
		#	 sound_data["featured_image"] = image 
		#	 sound_data["l_listing_image"] = image 
		#	 sound_data["p_listing_image"] = image 
 		#	end
 			sound_data["smart_url"] = "smart_url"
 			sound_data["play_url"] = {}
 			#sound_data["play_url"][l["embed_source"]] = {}

 			if l.has_key?("embedcode")
 				l["embed_source"] = "daily_motion" if l["embed_source"] == "dailymotion"
 				sound_data["play_url"][l["embed_source"]] = {}
 				sound_data["play_url"][l["embed_source"]]["url"] = l["embedcode"]
 			else
 				sound_data["play_url"]["saranyu"] = {}
 				sound_data["play_url"]["saranyu"]["url"] = "http://svu.1.apisaranyu.in/smart_urls/57f1fbd17669610aca660000"
 			end
 			sound_data ["category"] = l["type"]


				soundtrack << sound_data

		end
		
		return soundtrack


	end
def construct_ppl_sound asset_id,data,bg
	#status = false
	
	#data = data["song_list"]
	logger.debug "dta--===#{data}"
	people_data = []
	
		if data.has_key?"singers"
			
			singers = data["singers"]
			singers.each do |s|
				person ={}
				person["person_id"] = s["person_hash_id"]
				person["role_name"] = "singers"
				person["person_name"] = s["person_name"]
				person["person_info_info"] = ""
				# status,person_details =get_person person["person_id"],asset_id,bg
				# if status
				# logger.debug "personinfo===#{person_details["info"]}"
				# info = person_details["info"]       			
    #        		info.each do |i|
    #        			value=i["person_info_info"]
    #        			person["person_info_info"]=value
    #        		end
    #        		end
           		people_data << person
				#logger.debug "personinfo===#{det["person_info_info"]}"
				
			end
		end
		if data.has_key?"music"
			
			music = data["music"]
			music.each do |m|
				person ={}
				person["person_id"] = m["person_hash_id"]
				person["role_name"] = "music"
				person["person_name"] = m["person_name"]
				person["person_info_info"] = ""
				# status,person_details =get_person person["person_id"],asset_id,bg
				# if status
				# info = person_details["info"]       			
    #        		info.each do |i|
    #        			value=i["person_info_info"]
    #        			person["person_info_info"]=value
    #        		end
    #        		end


				people_data << person
			end
		end
		if data.has_key?"lyricist"
			
			lyricist = data["lyricist"]
			lyricist.each do |l|
				person ={}
				person["person_id"] = l["person_hash_id"]
				person["role_name"] = "lyricist"
				person["person_name"] = l["person_name"]
				person["person_info_info"] = ""
				# status,person_details =get_person person["person_id"],asset_id,bg
				# if status
				# info = person_details["info"]       			
    #        		info.each do |i|
    #        			value=i["person_info_info"]
    #        			person["person_info_info"]=value
    #        		end
    #        		end
				people_data << person		
			end
		end
		return true,people_data
	
end

	def create_videolist params,bg,catalog
		logger.debug "from videl list ============"

		details = params[:bridge_data][:details].last
		asset_id = details[:movie_hash_id]
		sound_data ={}
		status,data = get_soundtracks asset_id,bg
		#logger.debug "data from cinestan===#{data}"

		if status

			if data["list"].present?
				
				sound_data = construct_data_for_sound_track data,asset_id,bg,catalog
				

			 	return true,sound_data
			end 

		return false, "sound track data not found"
			
		else
			return false, "sound track data not found"
		end

		#status,data = create_videolist sound_data,params
	end

	def get_data_from_external_db(params,media_row,bg)
		base_url = bg.bridge_base_url
		
		theme = params["movie_catalog_ott_obj"]["theme"]
		if theme == "movie" || theme == "video"
			url = base_url + "/getMediaDetails/1/" + media_row["Asset Id"]
		end
		response = Typhoeus::Request.get(url,headers: {"authorization" => "Basic MTowY2U5OTk0NTY2OTNkZmMxNzc3OTFjOWQ1ZGZlNjE0MQ=="},followlocation: true)
		# response = http_request("get",{},url,60)	
		if response.response_code == 200
		  data = JSON.parse(response.body)	
		  #logger.debug "#{data}=========="

		  if data["Success"] == "1"
		  	return true,data["Result"]
		  else
			return false,"Error! Id doesn't exists. in external database"
		  end
		else
		  return false,"Can not pull movie data from bridge!"	
		end	
	end

	def get_person person_id,asset_id,bg
			
		base_url = bg.bridge_base_url
	       logger.debug "movie_id==#{asset_id}===person_id#{person_id}"	
		url = base_url + "/getPersonDetails/1/" + asset_id +"/"+ person_id
		
		response = Typhoeus::Request.get(url,headers: {"authorization" => "Basic MTowY2U5OTk0NTY2OTNkZmMxNzc3OTFjOWQ1ZGZlNjE0MQ=="},followlocation: true)
		
		if response.response_code == 200

		  data = JSON.parse(response.body)	
		  #logger.debug "#{data}=========="
		  if data["Success"] == "1"
		  	return true,data["Result"]
		  else
			return false,"Error! Id doesn't exists. in external database"
		  end
		else
		  return false,"Can not pull  people data from bridge!"	
		end	
	end

def get_ott_data data, catalog, bg
	people_data = []
	page_size = 0
	people = {}
	bg_obj = {}
	ppl_obj = {}
	bg_obj["name"]=bg.name
	bg_obj["ott_auth_token"] = bg.ott_auth_token
	bg_obj["ott_id"] = bg.ott_id

	cat_obj = {}
	cat_obj["catalog_id"] = catalog._id
	cat_obj["apps"] = catalog.app_id
	cat_obj["tenant_id"]=catalog.tenant_id
	cat_obj["business_group_id"] = catalog.bg_id
	cat_obj["ott_id"] = catalog.ott_id
	
	ppl_param = {}
	get_data = {}
	ppl_param["catalog_id"] = cat_obj["catalog_id"]
	ppl_param["bg_obj"] = bg_obj
		

	
    data.each do |d|
    	people["bg_obj"] = bg_obj
		people["title"]=d["person_name"]
		people["description"] = d["person_info_info"]
		people["regions"] = ["IN"]
		people["catalog_id"] = cat_obj["catalog_id"]
		people["business_group_id"] = cat_obj["buiness_group_id"]
		people["apps"] = cat_obj["apps"]
		people["tenants"] = cat_obj["tenant_id"]
		people["status"] = "published"
		people["featured_image"] = d["featured_image"]
		people["l_listing_image"] = d["l_listing_image"]
		people["p_listing_image"] = d["p_listing_image"]
		people["asset_id"] = d["asset_id"]
		status,get_data = get_people_from_ott ppl_param
			resp_body = get_data["body"]
			items = resp_body["data"]["items"]
			if items.count == 0
				status,val=create_at_ott(people)
				page_size = page_size +1

				val=val["body"]
					if status
		 				ppl_obj = {}
		 				role = []
         				
         				ppl_obj["person_id"] = val["data"]["item_id"]
         				ppl_obj["catalog_id"] = cat_obj["ott_id"]
         				role << d["role_name"]
         			    ppl_obj["roles"] = role
         				ppl_obj["description"] = d["person_info_info"]
         				ppl_obj["character_name"] = d["movie_character_name"]
         				ppl_obj["person_order"] = d["movie_person_order"]
         				people_data << ppl_obj
					end
				
			
			else
				
				person = items.select { |a1| a1["title"] == people["title"] }.empty?#? nil : it.select { |a1| a1[:title] == "#{people["title"]}" }.last
				
			 if person
			 	
			 	status,val=create_at_ott(people)
			 	page_size = page_size +1
         			val=val["body"]
         			logger.debug "status #{status}==val = #{val}"
					if status
		 				ppl_obj = {}
		 				role = []
		 				
         				
         				ppl_obj["person_id"] = val["data"]["item_id"]
         				ppl_obj["catalog_id"] = cat_obj["ott_id"]
         				role << d ["role_name"]
         				ppl_obj["roles"] = role
         				ppl_obj["description"] = d["person_info_info"]
         				ppl_obj["character_name"] = d["movie_character_name"]
         				ppl_obj["person_order"] = d["movie_person_order"]
         				
         				people_data << ppl_obj
					end
					
			 
			 else
			 	
			 	ppl_obj = {}
			 	items.each do |i|
			 		role = []
			 		
			 		if i["title"] == people["title"]
			 	ppl_obj["person_id"] = i["content_id"]
         			ppl_obj["catalog_id"] = cat_obj["ott_id"]
         			role << d["role_name"]
         			ppl_obj["roles"] = role
         			ppl_obj["description"] = d["person_info_info"]
         			ppl_obj["character_name"] = d["movie_character_name"]
         			ppl_obj["person_order"] = d["movie_person_order"]
         			people_data << ppl_obj

         		end
         	end
			 
			 end
			end

			
	end



	if status
		return true,people_data
	else
		return false,"people data not found"
	end


end
def update_ott_people data,catalog,bg
	people_data = []
	people = {}
	bg_obj = {}
	ppl_obj = {}
	bg_obj["name"]=bg.name
	bg_obj["ott_auth_token"] = bg.ott_auth_token
	bg_obj["ott_id"] = bg.ott_id

	cat_obj = {}
	cat_obj["catalog_id"] = catalog._id
	cat_obj["apps"] = catalog.app_id
	cat_obj["tenant_id"]=catalog.tenant_id
	cat_obj["business_group_id"] = catalog.bg_id
	cat_obj["ott_id"] = catalog.ott_id
	
	ppl_param = {}
	get_data = {}
	ppl_param["catalog_id"] = cat_obj["catalog_id"]
	ppl_param["bg_obj"] = bg_obj
		
    data.each do |d|
		#logger.debug "#{d}======each people-------"
    	people["bg_obj"] = bg_obj
		people["title"]=d["person_name"]
		people["description"] = d["person_info_info"]
		people["regions"] = ["IN"]
		people["catalog_id"] = cat_obj["catalog_id"]
		people["business_group_id"] = cat_obj["buiness_group_id"]
		people["apps"] = cat_obj["apps"]
		people["tenants"] = cat_obj["tenant_id"]
		people["status"] = "published"
		people["featured_image"] = d["featured_image"]
		people["l_listing_image"] = d["l_listing_image"]
		people["p_listing_image"] = d["p_listing_image"]
		people["asset_id"] = d["asset_id"]
		

		status,get_data = get_people_from_ott ppl_param
		
			resp_body = get_data["body"]
			
			items = resp_body["data"]["items"]
			person = items.select { |a1| a1["title"] == people["title"] }.empty?#? nil : it.select { |a1| a1[:title] == "#{people["title"]}" }.last
			

			if person
				logger.debug "==================from  if person updates"
				status,val=create_at_ott(people)
         			val=val["body"]
         			
					if status
		 				ppl_obj = {}
		 				role = []
		 				#p "#{val["data"]["item_id"]}=======00"
         				
         				ppl_obj["person_id"] = val["data"]["item_id"]
         				ppl_obj["catalog_id"] = cat_obj["ott_id"]
         				role << d ["role_name"]
         				ppl_obj["roles"] = role
         				ppl_obj["description"] = d["person_info_info"]
         				ppl_obj["character_name"] = d["movie_character_name"]
         				ppl_obj["person_order"] = d["movie_person_order"]
         				#logger.debug "#{ppl_obj}=-------------movie obj"
         				people_data << ppl_obj
					end

			else
				logger.debug "===========from else"
				items.each do |i|
			 		role = []
			 		
			 		if i["title"] == people["title"]
			 	
			 		people["item_id"] = i["content_id"]

			 		status,ott_data = update_people_at_ott people
			 		var = ott_data["body"]
			 		
			 		ppl_obj = {}
		 				role = []         				
         				ppl_obj["person_id"] = var["data"]["item_id"]
         				ppl_obj["catalog_id"] = cat_obj["ott_id"]
         				role << d ["role_name"]
         				ppl_obj["roles"] = role
         				ppl_obj["description"] = d["person_info_info"]
         				ppl_obj["character_name"] = d["movie_character_name"]
         				ppl_obj["person_order"] = d["movie_person_order"]
         				
         				people_data << ppl_obj
         				
			 		end
			 	end
			
			end
		
		#status,get_data = get_people_from_ott ppl_param
	end

	if status 
		return true,people_data
	else
		return false,"error in people data"
		
	end

	
end

	def construct_data_people params,bg

		people_data=[]
		count = 0
		catalog_obj = {}
		large =   "http://d36k7zwutv9wuw.cloudfront.net/image-bank/1500-1500/"
		medium =  "http://d36k7zwutv9wuw.cloudfront.net/image-bank/640-360/"
		small =   "http://d36k7zwutv9wuw.cloudfront.net/image-bank/120-160/"
		details = params[:bridge_data][:details].last
		asset_id = details[:movie_hash_id]
		
		
		if params[:bridge_data].has_key? "cast_list"
			details = params[:bridge_data][:cast_list]
			image = " "
			#count = count+ details.count
			details.each do |v|
                        if v[:person_hash_id].blank?
			  next
			end
			logger.debug "somappa ================== #{v[:person_hash_id]}"	
			var={}
           		var["asset_id"] = v[:person_hash_id] 
          		var["person_name"] = v[:person_name] 
           		var["role_name"] = v[:role_name]
           		#image = v[:person_image]
           		var["movie_character_name"]= v[:movie_person_character_name]
           		var["movie_person_order"] = v[:movie_person_order]
           		image = v[:person_image]
           		
           		if !image.nil? 
           			var["featured_image"] = medium + image
           			var["l_listing_image"] = medium + image
           			var["p_listing_image"] = medium + image

           		end
           		
           		# status,data = get_person var["asset_id"],asset_id,bg
           		# if status
           			
           		# 	info=data["info"]
           			
           		# 	info.each do |i|
           		# 		value=i["person_info_info"]
           		# 	var["person_info_info"]=value
           			
           		# 	end
           		# else
           		# 	return false,"person details not found"
           		# end
           		people_data << var
           		

 			end
			
		end
		if params[:bridge_data].has_key? "crew_list"
			details = params[:bridge_data][:crew_list]
			
			details.each do |v|
			if v[:person_hash_id].blank?
                          next
                        end

			var={}
           		var["asset_id"] = v[:person_hash_id]
          		var["person_name"] = v[:person_name]
           		var["role_name"] = v[:role_name]
           		image = v[:person_image]
           		var["movie_character_name"]= v[:movie_person_character_name]
           		var["movie_person_order"] = v[:movie_person_order]
           		var["person_info_info"]= ""
           		# status,data = get_person var["asset_id"],asset_id,bg
           		# if status
           			
           		# 	info=data["info"]
           			
           		# 	info.each do |i|
           		# 	value=i["person_info_info"]
           		# 	var["person_info_info"]=value
           		# 	end
           		# end
           		
           		if !image.nil? 
           			var["featured_image"] = medium + image
           			var["l_listing_image"] = medium + image
           			var["p_listing_image"] = medium + image
           		end

           		
           		people_data << var
           		

 			end
		end
		if status 
			return true,people_data
		else
			return false,"error in people data"
		
		end
	end 

	def build_data_from_bridge(params)
	
		metadata = {}
		large =   "http://d36k7zwutv9wuw.cloudfront.net/image-bank/1500-1500/"
		medium =  "http://d36k7zwutv9wuw.cloudfront.net/image-bank/640-360/"
		small =   "http://d36k7zwutv9wuw.cloudfront.net/image-bank/120-160/"
		if params[:bridge_data].has_key? "details"
			details = params[:bridge_data][:details].last
			metadata["title"] = details[:movie_title]
			metadata["asset_id"] = details[:movie_hash_id]
			image_name = details[:movie_mugshot_image]
			if !image_name.nil?
			metadata["featured_image"] = large + image_name
			metadata["l_listing_image"] = medium + image_name
			metadata["p_listing_image"] = small + image_name
			end
			metadata["rating"] = details[:movie_rating]
			metadata["year"] = details[:movie_release_date].split("-")[0].to_i
			metadata["release_date_string"] = details[:movie_release_date]
			metadata["duration_string"] = details[:movie_runtime]
			metadata["duration"] = details[:movie_runtime].to_i
		end

		if params[:bridge_data].has_key? "info"
			info = params[:bridge_data][:info].last
			metadata["description"] = info[:movie_info_info]
			metadata["short_description"] = ""
			metadata["keywords"] = ""
			metadata["sequence_no"] = 1
		end
		metadata["status"] = "edit"
		metadata["bulk_upload_id"] = params[:media_params][:bulk_upload_id]
		metadata["show_id"] = params[:media_params][:show_id] if params[:media_params][:show_id]
		metadata["subcategory_id"] = params[:media_params][:subcategory_id] if params[:media_params][:subcategory_id]

		if params[:media_row][:regions]
			metadata["regions"] = params[:media_row][:regions].split(",").map(&:strip)
		else
			metadata["regions"] = ["IN"]
		end
		metadata["business_group_id"] = params[:bg_object].ott_id
		metadata["language"] = "tbd"
		metadata["genres"] = ["tbd"]


		metadata["app_ids"] = []
		tenants = []
		params[:media_row]["Applications"].split("|").each do |a|
		  app = App.find(a)
		  metadata["app_ids"] << app.ott_id	
		  tenants << app.tenant_id
		end	
		tenants = tenants.uniq
		
		metadata["tenant_ids"] = []
		tenants.each do |t|
			tenant = Tenant.find(t)
			metadata["tenant_ids"] << tenant.ott_id
		end
		
		if !params[:media_row]["Smart URL"].blank?
			metadata["play_url"] = {} 
			metadata["play_url"][params[:media_row]["Source URL Type"]] = {}
			metadata["play_url"][params[:media_row]["Source URL Type"]]["url"] = params[:media_row]["Smart URL"]
			metadata["play_url_type"] = params[:media_row]["Source URL Type"]
		end
		return metadata
	end	
	
end

