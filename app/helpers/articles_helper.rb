module ArticlesHelper
	def validate_article_params(params)
		if !params.has_key? "catalog_id"
      return false,"Please select Catalog!"
    end
  
    if params[:catalog_id].blank?
      return false,"Please select Catalog!"
    end
    
  	if @videoflag == "yes"
    else
	    if !params.has_key? "apps"
	  		return false,"Please Select Applications!"
	  	end	
    end

    if !params.has_key? "genres"
      return false,"Please Select Genres!"
    end 

    if params["language"].count == 1 && params["language"].first.empty?
  		return false,"Please Select Language!"
  	end	

  	

    if !params[:utc_published_date].blank?
      if params[:utc_published_date] < DateTime.now.utc
        return false,"Published date can not be less than the current time!"
      end  
    end  

    if !params[:utc_published_date].blank? && !params[:utc_unpublished_date].blank?
      if params[:utc_unpublished_date] < params[:utc_published_date]
        return false,"Published date can not be greater than unpublished date!"
      end 

      if params[:utc_unpublished_date] == params[:utc_published_date]
        return false,"Published and unpublished date can not be equal!"
      end 
    end  

    return true,""
	end	

  def get_custom_fields(params)
  obj = {}
  if params[:tags]
    tags = params[:tags]
    tags = eval(params[:tags]) if params[:tags].kind_of? String
    tags.each do |hash|
      data = hash
      # data = JSON.parse(hash) if hash.kind_of? String  
      datatype = data["type"]
      case datatype
      when "String" 
        obj[data["tag"]] = !params[data["tag"]].blank? ? params[data["tag"]] : "" 
      when "Array"
        obj[data["tag"]] = !params[data["tag"]].blank? ? params[data["tag"]].split(',') : []
      when "Integer"
        obj[data["tag"]] = !params[data["tag"]].blank? ? check_valid_data(params[data["tag"]]) : -1
      end     
    end 
  end 
  return obj
  end

  def check_valid_data(value)
    if value.numeric?
      return value.to_i
    else
        return -1 
    end 
  end 

  def update_access_control(params)
    accesscontrol = []
    count = params[:region].count
    for i in 0..count-1
    behavior = {}
    behavior["region"] = params[:region][i]
        behavior["access_control_id"] = params[:access_control_id][i]
   #    behavior["tvod_id"] = params[:tvodid][i]
   #    behavior["tvod_price"] = params[:tvodprice][i]
   #    behavior["tvod_currency"] = params[:tvodcurrency][i]
   #    behavior["tvod_duration"] = params[:tvodduration][i]
   #    behavior["tvod_period"] = params[:tvodperiod][i]
   #    behavior["svod_id"] = []
   #    params[:svodid].each do |sd|
   #    behavior["svod_id"] << sd
   #  end
    accesscontrol << behavior
    end

    return accesscontrol
  end

  def create_people_object(params)
    current_bg = params[:bg_obj]
    people_catalog_array = params["people"]
    roles = params["roles"]
    descriptions = params["descriptions"]
    # images = params["images"]
    url = "http://" + get_ott_base_url + "/people?auth_token=" + current_bg.ott_auth_token
    response = http_request("get",{},url,60)
    if response["code"] == 200
      @people = response["body"]["data"]["items"]
    end
    p_array = []
    i = 0
    people_catalog_array.each do |pc|
      person = {}
      person_id = pc.split("-")[0]
      catalog_id = pc.split("-")[1]
      details = @people.select{|h| h["catalog_id"]==catalog_id && h["person_id"]==person_id }
      if !details.blank?
        person_details = details.first 
        person["person_id"] = person_details["person_id"]
        person["catalog_id"] = person_details["catalog_id"]
        if roles[i].include? ","
          person["roles"] = roles[i].split(",") 
        else
          person["roles"] = [roles[i]]
        end
        person["description"] = descriptions[i]
        # person["display_image"] = images[i]
        p_array << person
      end
      i = i + 1
    end
    return p_array
  end

end
