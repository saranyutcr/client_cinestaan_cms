module ApplicationHelper
	def bootstrap_class_for flash_type
    	{ success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type] || flash_type.to_s
  	end 
	def flash_messages(opts = {})
	    flash.each do |msg_type, message|
	        concat(content_tag(:div, message, id: "flash_", class: "alert #{bootstrap_class_for(msg_type)} fade in") do 
	            concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
	            concat message 
	        end)
	    end
	    nil
	end
	def user_is_boss?
		if !(current_user.roles.include? "cms_manager") && !(current_user.roles.include? "admin")
			return false
		else
			return true
		end
	end

	def get_cms_base_url
	  	case Rails.env
	  	when "development"
	  		return SystemSettings.where(:environment=>"development").first.cms_base_url
	  	when "preproduction"
	  		return SystemSettings.where(:environment=>"preproduction").first.cms_base_url
	  	when "production"
	  		return SystemSettings.where(:environment=>"production").first.cms_base_url
	  	end
	end
	def get_cms_public_url
		case Rails.env
	  	when "development"
	  		return SystemSettings.where(:environment=>"development").first.cms_public_url
	  	when "preproduction"
	  		return SystemSettings.where(:environment=>"preproduction").first.cms_public_url
	  	when "production"
	  		return SystemSettings.where(:environment=>"production").first.cms_public_url
	  	end
	end
	def get_tx_service_base_url
		case Rails.env
	  	when "development"
	  		return SystemSettings.where(:environment=>"development").first.tx_service_base_url
	  	when "preproduction"
	  		return SystemSettings.where(:environment=>"preproduction").first.tx_service_base_url
	  	when "production"
	  		return SystemSettings.where(:environment=>"production").first.tx_service_base_url
	  	end
	end
	def get_ott_base_url
		case Rails.env
	  	when "development"
	  		return SystemSettings.where(:environment=>"development").first.ott_base_url
	  	when "preproduction"
	  		return SystemSettings.where(:environment=>"preproduction").first.ott_base_url
	  	when "production"
	  		return SystemSettings.where(:environment=>"production").first.ott_base_url
	  	end
	end
	def get_ott_auth_token
		case Rails.env
	  	when "development"
	  		return SystemSettings.where(:environment=>"development").first.ott_auth_token
	  	when "preproduction"
	  		return SystemSettings.where(:environment=>"preproduction").first.ott_auth_token
	  	when "production"
	  		return SystemSettings.where(:environment=>"production").first.ott_auth_token
	  	end
	end

	def get_aux_module_base_url
		case Rails.env
	  	when "development"
	  		return SystemSettings.where(:environment=>"development").first.aux_module_base_url
	  	when "preproduction"
	  		return SystemSettings.where(:environment=>"preproduction").first.aux_module_base_url
	  	when "production"
	  		return SystemSettings.where(:environment=>"production").first.aux_module_base_url
	  	end
	end	

	def get_web_view_base_url
		case Rails.env
	  	when "development"
	  		return SystemSettings.where(:environment=>"development").first.web_view_base_url
	  	when "preproduction"
	  		return SystemSettings.where(:environment=>"preproduction").first.web_view_base_url
	  	when "production"
	  		return SystemSettings.where(:environment=>"production").first.web_view_base_url
	  	end
	end	

	def get_s3_upload_key(folder_key,bucket_name,domain_name)
      #bucket = "tcr-cf-test"
      bucket = bucket_name.blank? ? "tcr-cf-test" : bucket_name
      if domain_name.blank?
      	acl = 'public-read'
      else
      	acl = 'private'
      end
     if @bg["aws_access_key_image"].blank?
       access_key = "AKIAIBQGVPLT4QQ4SFJA"
     else
       access_key = @bg["aws_access_key_image"]
     end

     if @bg["aws_secret_image"].blank?
       secret = "4U0B0N0S07KEpMh19X71vXG95jDTLej+4UCEnLVz"
     else
       secret = @bg["aws_secret_image"]
     end

     # access_key = "AKIAIBQGVPLT4QQ4SFJA"
     # secret = "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"
      key = "#{folder_key}/"
      expiration = 60.minutes.from_now.utc.strftime('%Y-%m-%dT%H:%M:%S.000Z')
      max_filesize = 2.megabytes
      sas = '201' # Tells amazon to redirect after success instead of returning xml 
      policy = Base64.encode64(
        "{'expiration': '#{expiration}',
          'conditions': [
            {'bucket': '#{bucket}'},
            {'acl': '#{acl}'},
            
            {'Content-Type': 'image/'},
            ['starts-with', '$key', '#{key}'],
          ]}
        ").gsub(/\n|\r/, '')
      signature = Base64.encode64(OpenSSL::HMAC.digest(OpenSSL::Digest::Digest.new('sha1'), secret, policy)).gsub(/\n| |\r/, '')
      {:access_key => access_key, :key => key, :policy => policy, :signature => signature, :sas => sas, :bucket => bucket, :acl => acl, :expiration => expiration, :domain_name => domain_name}
	end
	
end
