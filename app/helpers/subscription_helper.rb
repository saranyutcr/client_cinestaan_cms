module SubscriptionHelper


# Subscription Catalog Create Method

def create_subscription(bg,tenant)
	status = true
		error = ""
		response = nil
		if status
			body = {}
			body["auth_token"] = bg.ott_auth_token
			body["catalog_data"] = {}
			body["catalog_data"]["name"] = "subscription"
			body["catalog_data"]["theme"] = "subscription"
			body["catalog_data"]["title"] = "subscription"
			body["catalog_data"]["apps"] = []
			tenant_ids = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				body["catalog_data"]["apps"] << app.ott_id
				tenant_ids << app.tenant_id
			end
			tenant_ids = tenant_ids.uniq

			if params[:edit] == "update"
				catalog = Catalog.find_by(:bg_id => bg.id, :theme => "subscription", :tenant_id => tenant.id.to_s)
				url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id
				response = http_request("put",body,url,60)
			else
				url = "http://"+get_ott_base_url+"/catalogs"
		    	response = http_request("post",body,url,60)
				catalog = Catalog.new
			end
			
			if !response["status"]
				status = false
				error = response["error"]
			end
		end
		if status
			catalog.name = "subscription"
			catalog.theme = "subscription"
			catalog.app_id = []
			catalog.tenant_id = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				catalog.app_id << app.id
				catalog.tenant_id << app.tenant_id
			end
			catalog.tenant_id = catalog.tenant_id.uniq
			catalog.bg_id = bg.id
			catalog.ott_id = response["body"]["data"]["catalog_id"]
			catalog.save
		end
		return catalog
	end


	def subscription_items(bg,tenantid,appid)
		status = true
	       metadata = {}
			
			metadata["status"] = "edit"
			
			metadata["tenant_ids"] = []
			#tenantid.to_s.each do |tid|
				tenantotid = Tenant.find_by(:id => tenantid)
				metadata["tenant_ids"] << tenantotid.ott_id
			#end

			metadata["app_ids"] = []
			#appid.each do |a|
			  app = App.find_by(:id => appid)
			  metadata["app_ids"] << app.ott_id	
			  metadata["title"] = "Plans For "+app.name
			  metadata["app_name"] = app.name
			#end	
			metadata["business_group_id"] = bg.ott_id
			
			metadata["plans"] = []
			# count = params[:ptitle].count
   #      	for i in 0..count-1
			#  plansdata = {}
			#  plansdata["title"] = params[:ptitle][i]
			#  plansdata["description"] = params[:pdescription][i]
			#  plansdata["duration"] = params[:pduration][i]
			#  plansdata["plan_id"] = params[:plan_id][i]
			#  plansdata["plan_type"] = params[:plan_type][i]
			#  plansdata["period"] = params[:pperiod][i]
			#  plansdata["region"] = params[:pregion][i]
			#  plansdata["price"] = params[:pprice][i]
			#  plansdata["currency"] = params[:pcurrency][i]
			#  metadata["plans"] << plansdata
			# end

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "subscription", :tenant_id => tenantid)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
            #error = response["body"]["error"]["message"]
        end
    	end

        return response
	end

	def create_sub_plan(bg, params, tenant_id)
	 	status = true

	        content = {}
	        metadata = {}
 		    metadata["title"] = params[:ptitle]
			metadata["status"] = params[:status] ? params[:status] : "edit"
			metadata["business_group_id"] = bg.ott_id
			# metadata["item_id"] = itemid
			# metadata["plans"] = []
 		#     content["title"] = params[:ptitle]
			# content["status"] = params[:status] ? params[:status] : "edit"
			# content["business_group_id"] = bg.ott_id
			# # content["item_id"] = itemid
			# content["plans"] = []
			# count = params[:ptitle].count if params[:ptitle]
			# count = 0
			plansdata = {}
			 # if params[:ptitle] == "app_plans"
			 # 	# add free pack
			 # plansdata["title"] = params[:ptitle] if params[:ptitle]
			 # plansdata["plan_id"] = params[:plan_id] 
			 # plansdata["description"] = params[:pdescription]
			 # plansdata["region"] = params[:pregion].to_a if params[:pregion]
			 # plansdata["plan_type"] = params[:plan_type]
			 # plansdata["start_date"] = params[:start_date]
			 # plansdata["end_date"] = params[:end_date]
			 # plansdata["app_plan_status"] = params[:app_plan_status]
			 # plansdata["app_action"] = params[:app_action]
			 # plansdata["app_ids"] = []
				#  if params[:apps].present?
				#  params[:apps].each do |app_id|
				# 	app = App.find(app_id)
				# 	plansdata["app_ids"] << app.ott_id
				#   end
				# end
			 # else
 			 
	 			 # content["plan_type"] = params[:plan_type] if params[:plan_type]
				 metadata["title"] = params[:ptitle] if params[:ptitle]
				 metadata["description"] = params[:pdescription]
				 metadata["duration"] = params[:pduration] if params[:pduration]
				 metadata["plan_type"] = params[:plan_type] 
				 metadata["plan_id"] = params[:plan_id] 
				 metadata["period"] = params[:pperiod] if params[:pperiod]
				 metadata["region"] = params[:pregion].to_a if params[:pregion]
				 metadata["price"] = params[:pprice]   if params[:pprice]
				 metadata["currency"] = params[:pcurrency] if params[:pcurrency]
				 plansregiondata = {}
				 plansregiondata["region"] = params[:pregion].to_a if params[:pregion]
				 plansregiondata["price"] = params[:pprice] if params[:pprice]
				 plansregiondata["currency"] = params[:pcurrency] if params[:pcurrency]
				 plansregiondata["plan_type"] = params[:plan_type] if params[:pcurrency]
	 			 metadata["regions"] = [plansregiondata]
	  			 metadata["app_ids"] = []
	 			 metadata['plans'] = []
				 if params[:apps].present?
				 params[:apps].each do |app_id|
					app = App.find(app_id)
					metadata["app_ids"] << app.ott_id
				  end
				end
			# end
			# if params[:regions]
			# 	content["regions"] = params[:regions].split(",").map(&:strip)
			# else
			# 	content["regions"] = ["IN"]
			# end
			# content = {}

			# metadata = content
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
	    # metadata["palns"] = []
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "subscription", :tenant_id => tenant_id)

        if params[:method] == "update"
        url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+itemid+"/plans/"+params[:planid]+"?auth_token="+bg.ott_auth_token  
        response = http_request("put",body,url,maxtime)
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token
		# url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+itemid+"/plans?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
    	end
        if response["code"] == 200
        else
            status = response["status"]
            #error = response["body"]["error"]["message"]
        end

        return response		
		
	end

	 def subscription_plans(bg,params,itemid,tenant_id)
	 	status = true

	        content = {}
	        metadata = {}
 		    metadata["title"] = params[:title]
			metadata["status"] = params[:status] ? params[:status] : "edit"
			metadata["business_group_id"] = bg.ott_id
			metadata["item_id"] = itemid
			# metadata["plans"] = []
 		    content["title"] = params[:title]
			content["status"] = params[:status] ? params[:status] : "edit"
			content["business_group_id"] = bg.ott_id
			content["item_id"] = itemid
			content["plans"] = []
			count = params[:ptitle].count
        	for i in 0..count-1
			 plansdata = {}

			 if params[:title] == "app_plans"
			 	# add free pack
			 plansdata["title"] = params[:ptitle][i] if params[:ptitle]
			 plansdata["plan_id"] = params[:plan_id][i] 
			 plansdata["description"] = params[:pdescription][i]
			 plansdata["region"] = params[:pregion][i].to_a if params[:pregion]
			 plansdata["plan_type"] = params[:plan_type][i]
			 plansdata["start_date"] = params[:start_date]
			 plansdata["end_date"] = params[:end_date]
			 plansdata["app_plan_status"] = params[:app_plan_status]
			 plansdata["app_action"] = params[:app_action]
			 plansdata["app_ids"] = []
				 if params[:apps].present?
				 params[:apps].each do |app_id|
					app = App.find(app_id)
					plansdata["app_ids"] << app.ott_id
				  end
				end
			 else
 			 content["plan_type"] = params[:plan_type][i] if params[:plan_type][i]
			 plansdata["title"] = params[:ptitle][i] if params[:ptitle][i]
			 plansdata["description"] = params[:pdescription][i]
			 plansdata["duration"] = params[:pduration][i] if params[:pduration][i]
			 plansdata["plan_type"] = params[:plan_type][i] 
			 plansdata["plan_id"] = params[:plan_id][i] 
			 plansdata["period"] = params[:pperiod][i] if params[:pperiod]
			 plansdata["region"] = params[:pregion][i].to_a if params[:pregion]
			 plansdata["price"] = params[:pprice][i]   if params[:pprice]
			 plansdata["currency"] = params[:pcurrency][i] if params[:pcurrency]
			end
			plansdata["regions"] = []
		    if params["pregion"]
		      count = params["pregion"].count
		      for i in 0..count-1
		        plan_meatdata = {}
		        plan_meatdata["region"] = params[:pregion][i].to_a if params[:pregion][i]
		        plan_meatdata["price"] = params[:pprice][i] if params[:pprice][i]
		        plan_meatdata["currency"] = params[:pcurrency][i] if params[:pcurrency][i]
		        plan_meatdata["plan_type"] = params[:plan_type][i] if params[:plan_type][i]
		        plansdata["regions"] << plan_meatdata
		      end
		    end
			 content["plans"] << plansdata
			end
			if params[:regions]
				content["regions"] = params[:regions].split(",").map(&:strip)
			else
				content["regions"] = ["IN"]
			end
			# content = {}
			metadata = content
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        metadata['plan_type'] = params['plan_type']

        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "subscription", :tenant_id => tenant_id)

        if params[:method] == "update"
        url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+itemid+"/plans/"+params[:planid]+"?auth_token="+bg.ott_auth_token  
        response = http_request("put",body,url,maxtime)
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+itemid+"?auth_token="+bg.ott_auth_token
		# url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+itemid+"/plans?auth_token="+bg.ott_auth_token  
        response = http_request("put",body,url,maxtime)
    	end
        if response["code"] == 200
        else
            status = response["status"]
            #error = response["body"]["error"]["message"]
        end

        return response
	 end


	 #Subcription Free pack

	 def subscription_free_pack(bg,tenant)
		status = true
	       metadata = {}
			
			metadata["status"] = "edit"
			
			metadata["tenant_ids"] = []
			tenantotid = Tenant.find_by(:id => tenant.id)
			metadata["tenant_ids"] << tenantotid.ott_id

			metadata["app_ids"] = []
			# App.where(:tenant_id => tenant.id).each do |app_id|
			# 	app = App.find(app_id)
			# 	metadata["app_ids"] << app.ott_id
			# end

			metadata["title"] = "app_plans"
			metadata["app_name"] = "app_plans"
			metadata["business_group_id"] = bg.ott_id
			
			metadata["plans"] = []

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "subscription", :tenant_id => tenant.id.to_s)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
            #error = response["body"]["error"]["message"]
        end
    	end

        return response
	end

end
