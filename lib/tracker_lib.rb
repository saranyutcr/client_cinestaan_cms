module TrackerLib
	 
	#Funtion to track count
	def track_entity_count(params)
		entity = params[:entity]
		status,message = check_bg_plan_status(params)
		if status
			bg_plan = params["bg_plan"]
			case entity
				when "catalog"
				  status,message = check_plan_trackers(params)
				when "media"	
					status,message = check_plan_trackers(params)
			end	
		else
		  return status,message
		end
	end

	def check_bg_plan_status(params)
		bg_plan = params["bg_plan"]
		if bg_plan

			#1.check plan expiry
			today = Time.now
			plan_expiry_date = bg_plan.start_date + bg_plan.days
#			if plan_expiry_date <= today.to_date
#				return false,"Your pack has been expired!"
#			end

		else
		  return false,"You have not selected any pack yet!"	
		end	
		return true,"Valid Pack"
	end	

	def check_plan_trackers(params)
		plan_details = params["bg_plan"].plan_details
		trackers = plan_details["trackers"]
		trackers.each do |tracker|
			title = tracker["title"]
			case title
			when "catalog_limit"
				if params["entity"] == "catalog"
					catalog_tracker_count = tracker["count"]
					break if catalog_tracker_count == -1
					catalog_count = get_catalog_count(params)
					if catalog_tracker_count <= catalog_count
						return false,"You have reached maximum limit for catalogs!"
					end	
				end
			when "media_limit"
				if params["entity"] == "media"
					media_tracker_count = tracker["count"]
					break if media_tracker_count == -1
					media_count = get_media_count(params)
					if false #media_tracker_count <= media_count
						return false,"You have reached maximum limit for videos!"
					end

					#Bulk upload media check
					if ["is_bulk_upload"] == "yes"
						media_can_be_uploaded = media_tracker_count - media_count
						if params["bulk_upload_count"] > media_can_be_uploaded
							return false,"You can upload only #{media_can_be_uploaded} videos!"
						end	
					end	

				end		
			end
		end
		return true,"All Trackers are valid"
	end	

	def get_catalog_count(params)
		catalogs = catalog_object.get_catalogs_by_bg(params)
		data = catalogs.as_json.reject { |h| h["theme"]=="subscription" || h["theme"]=="access_control" || h["theme"]=="message" || h["theme"]=="coupon"}
		return data.count
	end	

	def get_media_count(params)
		media = media_object.get_media_by_bg(params)
		return media.count
	end	

	def catalog_object
		Catalog.new
	end	

	def media_object
		Media.new
	end
		
end
