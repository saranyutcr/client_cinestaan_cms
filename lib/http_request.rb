require 'typhoeus'
require 'json'

module HttpRequest
	def http_request(method,body,url,timeout_at)
		response_status = true
		response_error = ""
		response_body = nil
		response_code = nil
		ret = nil
		begin
			Timeout::timeout(timeout_at) do
				case method
				when "post"
					ret = Typhoeus::Request.post(url,:body => body.to_json,:headers => {"content-type" => "application/json","accept" =>"application/json"})
				when "put"
					ret = Typhoeus::Request.put(url,:body => body.to_json,:headers => {"content-type" => "application/json","accept" =>"application/json"})
				when "get"
					ret = Typhoeus::Request.get(url,:headers => {"content-type" => "application/json","accept" =>"application/json"})
				when "delete"
					ret = Typhoeus::Request.delete(url,:body => body.to_json,:headers => {"content-type" => "application/json","accept" =>"application/json"})
				end
				Rails.logger.debug ret.inspect
			end
		rescue Timeout::Error
			response_status = false
			response_error = "Request Timed Out"
			response_code = 408
		rescue Exception => e
			response_status = false
			response_error = "Exception Raised for HTTP Request"
			response_code = 500
		end
		if ret.options[:response_body] && ret.options[:response_body]!=""
			response_body = JSON.parse(ret.options[:response_body])
		else
			response_body = ""
		end
		if ret.options[:response_code]
			response_code = ret.options[:response_code]
=begin
			if response_code != 200
				response_status = false
				response_error = "Request Failed"
			end
=end
		end
		response = {}
		response["status"] = response_status
		response["error"] = response_error
		response["body"] = response_body
		response["code"] = response_code
		return response
	end
end