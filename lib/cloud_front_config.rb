module CloudFrontConfig
	
  include ApplicationHelper
  
	def create_cloudfront_distribution region, access_key, access_secret_key, bg, bucket_name, origin_identity
  begin
    cloudfront = Aws::CloudFront::Client.new(region: region,credentials: Aws::Credentials.new(access_key, access_secret_key))
    resp = cloudfront.create_distribution({
    distribution_config: { # required
      caller_reference: "#{bucket_name}_test", # required
  #    aliases: {
  #      quantity: 1, # required
  #      items: ["string"],
  #    },
  #   default_root_object: "string",
      origins: { # required
        quantity: 1, # required
        items: [
          {
            id: "#{bucket_name}_test", # required
            domain_name: "#{bucket_name}.s3.amazonaws.com", # required
            origin_path: "",
  #          custom_headers: {
  #            quantity: 1, # required
  #            items: [
  #              {
  #                header_name: "string", # required
  #               header_value: "string", # required
  #              },
  #            ],
  #         },
            s3_origin_config: {
              origin_access_identity: "origin-access-identity/cloudfront/#{origin_identity.cloud_front_origin_access_identity.id}", # required
              #origin_access_identity: "origin-access-identity/cloudfront/E3L75OYJ4YLKJ0", # required
            },
  #          custom_origin_config: {
  #            http_port: 1, # required
  #            https_port: 1, # required
  #            origin_protocol_policy: "http-only", # required, accepts http-only, match-viewer, https-only
  #            origin_ssl_protocols: {
  #              quantity: 1, # required
  #              items: ["SSLv3"], # required, accepts SSLv3, TLSv1, TLSv1.1, TLSv1.2
  #            },
  #          },
          },
        ],
      },
      default_cache_behavior: { # required
        target_origin_id: "#{bucket_name}_test", # required
        forwarded_values: { # required
          query_string: false, # required
          cookies: { # required
            forward: "none", # required, accepts none, whitelist, all
  #          whitelisted_names: {
  #            quantity: 1, # required
  #            items: ["string"],
  #          },
          },
   #       headers: {
    #        quantity: 1, # required
     #       items: ["string"],
      #    },
        },
        trusted_signers: { # required
          enabled: false, # required
          quantity: 0, # required
#          items: ["self"],
        },
        viewer_protocol_policy: "allow-all", # required, accepts allow-all, https-only, redirect-to-https
        min_ttl: 1, # required
        allowed_methods: {
          quantity: 2, # required
          items: ["HEAD","GET"], # required, accepts GET, HEAD, POST, PUT, PATCH, OPTIONS, DELETE
          cached_methods: {
            quantity: 2, # required
            items: ["HEAD","GET"], # required, accepts GET, HEAD, POST, PUT, PATCH, OPTIONS, DELETE
          },
        },
        smooth_streaming: true,
        default_ttl: 1,
        max_ttl: 1,
        compress: false,
      },
=begin
      cache_behaviors: {
        quantity: 1, # required
        items: [
          {
            path_pattern: "string", # required
            target_origin_id: "string", # required
            forwarded_values: { # required
              query_string: false, # required
              cookies: { # required
                forward: "none", # required, accepts none, whitelist, all
                whitelisted_names: {
                  quantity: 1, # required
                  items: ["string"],
                },
              },
              headers: {
                quantity: 1, # required
                items: ["string"],
              },
            },
            trusted_signers: { # required
              enabled: false, # required
              quantity: 1, # required
              items: ["string"],
            },
            viewer_protocol_policy: "allow-all", # required, accepts allow-all, https-only, redirect-to-https
            min_ttl: 1, # required
            allowed_methods: {
              quantity: 1, # required
              items: ["GET"], # required, accepts GET, HEAD, POST, PUT, PATCH, OPTIONS, DELETE
              cached_methods: {
                quantity: 1, # required
                items: ["GET"], # required, accepts GET, HEAD, POST, PUT, PATCH, OPTIONS, DELETE
              },
            },
            smooth_streaming: false,
            default_ttl: 1,
            max_ttl: 1,
            compress: false,
          },
        ],
      },

      custom_error_responses: {
        quantity: 1, # required
        items: [
          {
            error_code: 1, # required
            response_page_path: "string",
            response_code: "string",
            error_caching_min_ttl: 1,
          },
        ],
      },
=end
      comment: "#{bucket_name}", # required
  #    logging: {
  #      enabled: false, # required
  #      include_cookies: false, # required
  #      bucket: "string", # required
   #     prefix: "string", # required
  #    },
      price_class: "PriceClass_All", # accepts PriceClass_100, PriceClass_200, PriceClass_All
      enabled: true, # required
      viewer_certificate: {
        cloud_front_default_certificate: true,
  #      iam_certificate_id: "string",
  #      acm_certificate_arn: "string",
  #      ssl_support_method: "sni-only", # accepts sni-only, vip
  #      minimum_protocol_version: "SSLv3", # accepts SSLv3, TLSv1
  #      certificate: "string",
  #      certificate_source: "cloudfront", # accepts cloudfront, iam, acm
      },
  #    restrictions: {
  #      geo_restriction: { # required
  #        restriction_type: "blacklist", # required, accepts blacklist, whitelist, none
  #        quantity: 1, # required
  #        items: ["string"],
  #      },
  #    },
  #    web_acl_id: "string",
    },
  })
  return resp
  rescue Exception => e
    puts "Failed to create distribution, Got Exception : #{e}"
    return false
  end
end

def create_bucket region, access_key, access_secret_key, bucket_name
  begin   
    flag = false
    Aws.config.update(region: region,credentials: Aws::Credentials.new(access_key, access_secret_key))
    s3_client = Aws::S3::Client.new()
    #s3 = AWS::S3.new(region: region,access_key_id: 'AKIAIBQGVPLT4QQ4SFJA',secret_access_key: '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')
    resp = s3_client.list_buckets
    resp.buckets.each do |bucket|
      if bucket.name == bucket_name
        flag = true
        break
      end 
    end

    if flag
      puts "bucket already exist, bucket_name : #{bucket_name}"
      return bucket_name
    else
      bucket = s3_client.create_bucket(bucket: bucket_name)
    end
    return bucket_name
  rescue Exception => e
   puts "Failed to create bucket, Got Exception : #{e}"
  end
end

	def create_origin_access_identity region, access_key, access_secret_key, bg
	  begin
	    cloudfront = Aws::CloudFront::Client.new(region: region,credentials: Aws::Credentials.new(access_key, access_secret_key))
	    resp = cloudfront.create_cloud_front_origin_access_identity({
	      cloud_front_origin_access_identity_config: { # required
	      caller_reference: bg, # required
	      comment: bg, # required
	    },
	    })
	    return resp
	  rescue Exception => e
	    puts "Failed to create origin access identity, Got Exception : #{e}"
	    return false
	  end
	end

	def create_bucket_policy_with_origin_identity region, access_key, access_secret_key, bg, bucket_name, origin_identity
	  begin
	    Aws.config.update(region: region,credentials: Aws::Credentials.new(access_key, access_secret_key))
	    s3_client = Aws::S3::Client.new()
	    policy = {
	      "Version"=>"2012-10-17",
	      "Id"=>"PolicyForCloudFrontPrivateContent",
	      "Statement"=>[
	        {
	          "Sid"=> "#{bg} Ad permission",
	          "Effect"=>"Allow",
	          "Principal"=>{"CanonicalUser"=> "#{origin_identity.cloud_front_origin_access_identity.s3_canonical_user_id}"},
	          #{}"Principal": {"CanonicalUser": "2101cab3fcede38fe8fe408ae1052b68c59057448a85e2a1568699846387a229d58da39e24968692824c0c093bb4ea39"},
	          "Action"=>"s3:GetObject",
	          "Resource"=>["arn:aws:s3:::#{bucket_name}/*"]
	        }
	      ]
	    }
	    puts policy
	    s3_client.put_bucket_policy({
	      bucket: bucket_name,
	      policy: policy.to_json
	    })
	    resp = s3_client.get_bucket_policy({
	      bucket: bucket_name,
	    })

	    puts resp.policy.read
	    return true
	  rescue Exception => e
	    puts "Failed to create bucket policy, Got exception : #{e}"
	    return false
	  end
	end

  def add_crossdomain_to_bucket region, access_key, access_secret_key, bucket_name
    begin
      Aws.config.update(region: region,credentials: Aws::Credentials.new(access_key, access_secret_key))
      s3 = Aws::S3::Resource.new()
      obj = s3.bucket(bucket_name).object('crossdomain.xml')
      xml_content = '<?xml version="1.0"?>
  <!DOCTYPE cross-domain-policy SYSTEM "http://www.adobe.com/xml/dtds/cross-domain-policy.dtd">
  <cross-domain-policy>
  <allow-access-from domain="*" />
  <site-control permitted-cross-domain-policies="all" />
  </cross-domain-policy>'
      ret = obj.put(body: xml_content)
      return true
    rescue Exception => e
      puts "Got exception : #{e}"
      return false
    end
  end

  def create_tag_for_s3_storage(streaming_domain_name,bucket_name,bg)
    tag_name = bucket_name + "_instaott"
    body = {}
    body[:auth_token] = bg.tx_auth_token
    body[:source_url] = {}
    body[:source_url][:type] = "s3"
    body[:source_url][:domain_name] = streaming_domain_name
    body[:source_url][:tag] = tag_name
    body[:source_url][:credentials] = {}
    body[:source_url][:credentials][:access_key_id] = "AKIAIBQGVPLT4QQ4SFJA"
    body[:source_url][:credentials][:secret_access_key] = "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"
    
    url = "http://"+bg.tx_base_url+"/source_url_crd"
    response = http_request("post",body,url,60)
    if response["code"] == 200
      return true,response["body"]
    end  
    return false,response["body"]
  end  

  def create_cdn(bg)

    body = {}
    body[:auth_token] = bg.tx_auth_token
    body[:cdn_info] = {}
    body[:cdn_info][:name] = bg.bucket_name + "_cloudfront"
    body[:cdn_info][:internal_cdn_name] = "origin"
    body[:cdn_info][:adaptive_file_type] = "m3u8"
    body[:cdn_info][:protocol_for_path] = "http_pd"
    body[:cdn_info][:domain_name] = bg.domain_name
    body[:cdn_info][:s3_write] = "yes"

    url = "http://"+bg.tx_base_url+"/create_cdn"
    response = http_request("post",body,url,60)

    if response["code"] == 200
      return true,response["body"]
    end  
    return false,response["body"]
  end  

  #Delete bucket objects(images)
  def delete_bucket_object bucket_name, object_key
    begin
      Aws.config.update(region: "ap-southeast-1",access_key_id: 'AKIAIBQGVPLT4QQ4SFJA',secret_access_key: '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')
      s3 = Aws::S3::Resource.new()
      obj = s3.bucket(bucket_name).object(object_key)
      if obj.exists?
        ret = obj.delete
        return true
      else
        return false
      end
    rescue Exception => e
      puts "failed to delete, Got exception : #{e}"
    end
  end

  def add_cors_configs region, access_key, access_secret_key, bucket_name
    begin
      Aws.config.update(region: region,credentials: Aws::Credentials.new(access_key, access_secret_key))
      s3_client = Aws::S3::Client.new
      p = s3_client.put_bucket_cors({
        bucket: bucket_name, # required
        cors_configuration: { # required
        cors_rules: [ # required
        {
          allowed_headers: ["*"],
          allowed_methods: ["GET"], # required
          allowed_origins: ["*"], # required
          max_age_seconds: 3600,
        },{
          allowed_headers: ["*"],
          allowed_methods: ["POST"], # required
          allowed_origins: ["*"], # required
          max_age_seconds: 3600,
        },{
          allowed_headers: ["*"],
          allowed_methods: ["PUT"], # required
          allowed_origins: ["*"], # required
          max_age_seconds: 3600,
        },
      ],
    },
  })
  #    bucket = Aws::S3::Bucket.new(bucket_name)
  #    puts bucket.exists?
  #    puts bucket.cors
  #    bucket.cors.add(:allowed_methods => %w(GET POST PUT),:allowed_origins => %w(*),:max_age_seconds => 3600,:allowed_headers => %w(*))
    return true
    rescue Exception => e
      puts "Got exception : #{e}"
      return false
    end 
  end

end